import DataAdjustHeight from './global/DataAdjustHeight';
import DataCursor from './global/DataCursor';
import DataFullHeight from './global/DataFullHeight';
import DataHash from './global/DataHash';
import DataHideScroll from './global/DataHideScroll';
import DataMobileMenu from './global/DataMobileMenu';
import DataNumber from './global/DataNumber';
import DataScroll from './global/DataScroll';
import DataScrollId from './global/DataScrollId';
import DataSelect from './global/DataSelect';
import DataSticky from './global/DataSticky';
import DataTextWrap from './global/DataTextWrap';
import DataToggle from './global/DataToggle';
import DataVideo from './global/DataVideo';
import ScrollDisabler from './global/ScrollDisabler';

import Footer from './site/Footer';
import Header from './site/Header';

import DataLoader from './sections/DataLoader';
import IconsDrop from './sections/IconsDrop';
import InstructionSection from './sections/InstructionSection';
import MapTabs from './sections/MapTabs';
import NumberSlider from './sections/NumberSlider';
import PeopleList from './sections/PeopleList';
import TabsContent from './sections/TabsContent';
import TabsLine from './sections/TabsLine';
import TabsStats from './sections/TabsStats';

class Core
{
  /* ------------------------------------------------------------------------------
  | Custom event (added to window) | Arg      | Source                            |
  |--------------------------------|----------|-----------------------------------|
  | refreshWindowHeight            | -        | DataScroll.js / DataHideScroll.js |
  | afterCustomScroll              | integer  | DataScroll.js                     |
  | setManualScroll                | integer  | DataScroll.js                     |
  | afterManualScroll              | integer  | DataScroll.js                     |
  | lockScroll                     | boolean  | DataScroll.js / ScrollDisabler.js |
  | scrollToDataId                 | string   | DataScrollId.js                   |
  | mobileMenuStatus               | boolean  | DataMobileMenu.js                 |
  | setMobileMenuStatus            | boolean  | DataMobileMenu.js                 |
  | googleMapsLoaded               | -        | DataMap.js                        |
  | refreshTextWrap                | -        | DataTextWrap.js                   |
  ------------------------------------------------------------------------------- */

  constructor()
  {
    new DataAdjustHeight();
    new DataCursor();
    new DataFullHeight();
    new DataHash();
    new DataHideScroll();
    new DataMobileMenu();
    new DataNumber();
    new DataScroll();
    new DataScrollId();
    new DataSelect();
    new DataSticky();
    new DataTextWrap();
    new DataToggle();
    new DataVideo();
    new ScrollDisabler();

    new Footer();
    new Header();

    new DataLoader();
    new IconsDrop();
    new InstructionSection();
    new MapTabs();
    new NumberSlider();
    new PeopleList();
    new TabsContent();
    new TabsLine();
    new TabsStats();
  }
}

new Core();
