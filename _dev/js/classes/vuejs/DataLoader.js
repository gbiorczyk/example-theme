Vue.component('data-loader', {
  props: [
    'apiUrl',
    'defaultForm',
    'defaultValues',
    'defaultItems',
    'defaultPagination',
    'loaderConfig',
    'urlPattern',
  ],
  data()
  {
    return {
      form: Object.assign({
        page: this.defaultPagination.page,
      }, this.defaultForm, this.defaultValues),
      pagination: {
        min: this.defaultPagination.page,
        pages: this.defaultPagination.pages,
      },
      items: [],
      loader: {
        timeInit: null,
        timeoutSend: null,
        timeoutWait: null,
      },
      status: {
        updated: false,
        loading: false,
        empty: (!this.defaultItems || (JSON.parse(atob(this.defaultItems)).length === 0)),
      },
      cache: {},
    };
  },
  mounted()
  {
    this.initWatch();
    this.refreshPageEvents();
    this.setLoaderPosition();
  },
  methods:
  {
    /* ---
      Detect changes in filters
    --- */
    initWatch()
    {
      Object.keys(this.defaultForm).forEach((key) => {
        this.$watch(`form.${key}`, () => {
          this.form.page   = 1;
          const isDelayKey = (this.loaderConfig.delayKeys.indexOf(key) > -1);
          this.loadResults(true, isDelayKey, true);
        });
      });
    },

    /* ---
      Navigation
    --- */
    loadPrev(e)
    {
      e.preventDefault();
      if (this.pagination.min === 1) return;
      this.pagination.min--;
      this.form.page = this.pagination.min;
      this.loadResults(true, false, true);
    },
    loadNext(e)
    {
      e.preventDefault();
      if (this.form.page >= this.pagination.pages) return;
      this.form.page++;
      this.pagination.min = this.form.page;
      this.loadResults(true, false, true);
    },
    loadMore(e)
    {
      e.preventDefault();
      if (this.status.loading) return;
      this.form.page++;
      this.loadResults(false);
    },

    /* ---
      Reset form
    --- */
    resetFilters(e)
    {
      e.preventDefault();
      this.form.page = 1;
      this.resetInputs(Object.keys(this.defaultForm));
    },
    resetInputs(inputs)
    {
      const { length } = inputs;
      for (let i = 0; i < length; i++) {
        this.form[inputs[i]] = this.defaultForm[inputs[i]];
        if (this.$refs[`form.${inputs[i]}`] === undefined) continue;
        this.resetInputValue(this.$refs[`form.${inputs[i]}`], this.defaultForm[inputs[i]]);
      }
    },
    resetInputValue(input, value)
    {
      input.value = value;
      const event = document.createEvent('HTMLEvents');
      event.initEvent('change', true, true);
      input.dispatchEvent(event);
      event.initEvent('input', true, true);
      input.dispatchEvent(event);
    },

    /* ---
      API connection
    --- */
    loadResults(clearBefore, sendDelay = false, scrollTop = false)
    {
      this.status.loading  = true;
      this.loader.timeInit = Date.now();

      setTimeout(() => {
        this.setLoaderPosition();
        this.refreshPageEvents();
      }, 0);

      const cacheKey = JSON.stringify(this.form);
      const response = (this.cache[cacheKey] !== undefined) ? this.cache[cacheKey] : null;
      if (response) this.showResults(response, clearBefore, scrollTop);
      else this.loadResultsByApi(clearBefore, sendDelay, scrollTop, cacheKey);
    },
    setLoaderPosition()
    {
      const { loader, wrapper } = this.$refs;
      loader.style.transform = '';

      const posLoader  = loader.getBoundingClientRect();
      const posWrapper = wrapper.getBoundingClientRect();
      const offsetMax  = posWrapper.height - posLoader.height - this.loaderConfig.spacing;

      let offset = (window.innerHeight / 2 - posLoader.top) - (posLoader.height / 2);
      offset = Math.min(offsetMax, offset);
      offset = Math.max(this.loaderConfig.spacing, offset);
      loader.style.transform = `translateY(${offset}px)`;
    },
    loadResultsByApi(clearBefore, sendDelay, scrollTop, cacheKey)
    {
      clearTimeout(this.loader.timeoutSend);
      clearTimeout(this.loader.timeoutWait);
      this.loader.timeoutSend = setTimeout(() => {
        this.connectToApi(clearBefore, scrollTop, cacheKey);
      }, sendDelay ? this.loaderConfig.delay : 0);
    },
    connectToApi(clearBefore, scrollTop, cacheKey)
    {
      axios.get(
        this.apiUrl,
        { params: this.form }
      ).then((response) => {
        if (response.status !== 200) return; // eslint-disable-line
        this.showResults(response.data, clearBefore, scrollTop, cacheKey);
      });
    },
    showResults(response, clearBefore, scrollTop, cacheKey = null)
    {
      if (cacheKey) this.cache[cacheKey] = response;

      const diffTime = Date.now() - this.loader.timeInit;
      const endTime  = Math.max((this.loaderConfig.time - diffTime), 0);

      clearTimeout(this.loader.timeoutWait);
      this.loader.timeoutWait = setTimeout(() => {
        this.printResults(response, clearBefore, scrollTop);
      }, (cacheKey !== null) ? endTime : this.loaderConfig.timeCache);
    },

    /* ---
      Display API response
    --- */
    printResults(response, clearBefore, scrollTop)
    {
      this.loadDefaultItems();
      if (clearBefore) this.items = [];

      this.items  = this.items.concat(response.data);
      this.pagination.pages = response.pages;

      this.finishLoading(response.data, scrollTop);
      if (response.data) this.setPageUrl();
    },
    loadDefaultItems()
    {
      if (this.status.updated) return;
      this.items = JSON.parse(atob(this.defaultItems));
    },
    finishLoading(response, scrollTop)
    {
      this.status.updated = true;
      this.status.loading = false;
      this.status.empty   = (response.length === 0);

      this.$nextTick(() => {
        this.refreshPageEvents();
        if (scrollTop) this.scrollToTop();
      });
    },
    refreshPageEvents()
    {
      window.triggerEvent('refreshWindowHeight');
      window.triggerEvent('refreshTextWrap');
    },
    scrollToTop()
    {
      const position     = this.$refs.wrapper.getBoundingClientRect();
      const windowScroll = document.body.scrollTop || document.documentElement.scrollTop;
      const scrollAdd    = position.top - this.loaderConfig.scrollOffset;

      if (scrollAdd > 0) return;
      window.triggerEvent('setManualScroll', (windowScroll + scrollAdd));
    },

    /* ---
      URL generation
    --- */
    setPageUrl()
    {
      let url = decodeURI(this.urlPattern);
      const matches = url.match(/![a-z-_|/=#]+!/gi);
      if (!matches) return;

      const { length } = matches;
      for (let i = 0; i < length; i++) {
        const data  = matches[i].match(/!([^|]+)\|(.*)!/);
        const value = this.getValueOfUrlPage(data);
        url = url.replace(matches[i], value);
      }
      url = url.replace(/([^:])([/]{2,})/g, '$1/');
      url = url.replace(/\?[&]+/, '?');
      url = url.replace(/[&]+/, '&');
      url = url.replace(/(\?|&)$/, '');

      window.history.replaceState({}, document.title, url);
    },
    getValueOfUrlPage(data)
    {
      let value = '';
      if (data && (this.form[data[1]] !== undefined)) value = this.form[data[1]];
      if (Array.isArray(value)) value = value.join(',');
      if ((data[1] === 'page') && (value <= 1)) value = '';

      if (value === '') return value;
      else return data[2].replace('#', value);
    },
  },
});
