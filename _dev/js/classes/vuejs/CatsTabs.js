import Swiper from 'swiper/js/swiper.min';

Vue.component('cats-tabs', {
  props: [
    'defaultTab',
    'defaultCategory',
  ],
  data() {
    return {
      config: {
        tabSelector: '.tabsStats__buttonsWrapper .tabsStats__buttonLink',
        scrollOffset: 70,
      },
      status: {
        tab: 0,
        category: 0,
      },
      slider: null,
      tabs: [],
    };
  },
  watch: {
    tab: () => {
      this.setDefaultItem();
    },
  },
  mounted() {
    this.status.tab      = this.defaultTab;
    this.status.category = this.defaultCategory;
    this.initCarousel();
    this.afterSlideChange();
  },
  updated() {
    window.triggerEvent('refreshWindowHeight');
  },
  methods: {
    setTab(e, index, categoryIndex)
    {
      e.preventDefault();
      this.status.tab      = index;
      this.status.category = categoryIndex;
    },
    setCategory(e, index)
    {
      e.preventDefault();
      this.status.category = index;
      this.scrollToItems();
    },
    initCarousel()
    {
      this.tabs   = this.$refs.tabs.querySelectorAll(this.config.tabSelector);
      this.slider = new Swiper(this.$refs.slider, {
        slidesPerView: 1,
        loop: true,
        navigation: {
          nextEl: this.$refs.sliderNext,
          prevEl: this.$refs.sliderPrev,
        },
      });
      this.slider.on('slideChange', this.afterSlideChange);
    },
    afterSlideChange()
    {
      const current = this.slider.realIndex;
      const percent = (current + 1) / this.tabs.length * 100;

      this.$refs.sliderProgress.style.width = `${percent}%`;
      this.tabs[current].click();
    },
    scrollToItems()
    {
      const position = this.$refs.items.getBoundingClientRect();
      if (position.top <= (window.innerHeight - this.config.scrollOffset)) return;

      const scrollAdd    = position.top - this.config.scrollOffset;
      const windowScroll = document.body.scrollTop || document.documentElement.scrollTop;
      window.triggerEvent('setManualScroll', (windowScroll + scrollAdd));
    },
  },
});
