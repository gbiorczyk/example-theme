import Swiper from 'swiper/js/swiper.min';

class TabsContentCore
{
  constructor(wrapper)
  {
    this.section = wrapper;
    if (!this.setVars()) return;

    this.initCarousel();
    this.afterSlideChange(0);
  }

  setVars()
  {
    this.buttons        = this.section.querySelectorAll('[data-toggle-button]');
    this.sliderWrapper  = this.section.querySelector('.sliderArrows__slider');
    this.sliderPrev     = this.section.querySelector('.sliderArrows__nav--prev');
    this.sliderNext     = this.section.querySelector('.sliderArrows__nav--next');
    this.sliderProgress = this.section.querySelector('.sliderArrows__progress');

    return true;
  }

  initCarousel()
  {
    this.slider = new Swiper(this.sliderWrapper, {
      slidesPerView: 1,
      loop: true,
      navigation: {
        nextEl: this.sliderNext,
        prevEl: this.sliderPrev,
      },
    });
    this.slider.on('slideChange', this.afterSlideChange.bind(this));
  }

  afterSlideChange(index = null)
  {
    const current = (index === null) ? this.slider.realIndex : index;
    const percent = (current + 1) / this.buttons.length * 100;
    this.sliderProgress.style.width = `${percent}%`;
    this.buttons[current].click();
  }
}

export default class TabsContent
{
  constructor()
  {
    this.sections = document.querySelectorAll('.tabsContent');
    if (!this.sections.length) return;

    this.sections.forEach((section) => {
      new TabsContentCore(section);
    });
  }
}
