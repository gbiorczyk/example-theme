import { TweenLite, CSSPlugin } from 'gsap/all';
// eslint-disable-next-line
const plugins = [CSSPlugin]; // correct importing on production

class IconsDropCore
{
  constructor(wrapper)
  {
    this.section = wrapper;
    if (!this.setVars()) return;

    this.setEvents();
  }

  setVars()
  {
    this.items   = this.section.querySelectorAll('.iconsDrop__item');
    this.drops   = this.section.querySelectorAll('.iconsDrop__itemDropInner');
    this.headers = this.section.querySelectorAll('.iconsDrop__itemHeader');
    this.buttons = this.section.querySelectorAll('.iconsDrop__itemButton');

    this.classes = {
      itemActive: 'iconsDrop__item--active',
    };
    this.settings = {
      animationTime: 1,
    };
    this.status = {
      current: -1,
    };

    return true;
  }

  setEvents()
  {
    const { length } = this.items;
    for (let i = 0; i < length; i++) {
      this.headers[i].addEventListener('click', (e) => {
        e.preventDefault();
        this.setItem(i);
      });
      this.buttons[i].addEventListener('click', (e) => {
        e.preventDefault();
        this.setItem(i);
      });
    }
  }

  setItem(index)
  {
    const isClosing = (index === this.status.current);

    this.items.removeClass(this.classes.itemActive);
    if (!isClosing) this.items[index].addClass(this.classes.itemActive);

    const { length } = this.drops;
    for (let i = 0; i < length; i++) {
      if (!isClosing && (i === index)) {
        TweenLite.set(this.drops[i], {
          height: 'auto',
        });
        TweenLite.from(this.drops[i], this.settings.animationTime, {
          height: 0,
          onUpdate: () => { window.triggerEvent('refreshWindowHeight'); },
        });
      } else {
        TweenLite.to(this.drops[i], this.settings.animationTime, {
          height: 0,
          onUpdate: () => { window.triggerEvent('refreshWindowHeight'); },
        });
      }
    }

    if (!isClosing) this.status.current = index;
    else this.status.current = -1;
  }
}

export default class IconsDrop
{
  constructor()
  {
    this.sections = document.querySelectorAll('.iconsDrop');
    if (!this.sections.length) return;

    this.sections.forEach((section) => {
      new IconsDropCore(section);
    });
  }
}
