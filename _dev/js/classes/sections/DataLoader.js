import DataLoaderComponent from './../vuejs/DataLoader'; // eslint-disable-line

class DataLoaderCore
{
  constructor(wrapper)
  {
    this.section = wrapper;
    if (!this.setVars()) return;

    this.initVue();
  }

  setVars()
  {
    this.compontent = this.section.querySelector('data-loader');
    if (!this.compontent) return;

    return true;
  }

  initVue()
  {
    new Vue({ el: this.compontent });
  }
}

export default class DataLoader
{
  constructor()
  {
    this.sections = document.querySelectorAll('.dataLoader');
    if (!this.sections.length) return;

    this.sections.forEach((section) => {
      new DataLoaderCore(section);
    });
  }
}
