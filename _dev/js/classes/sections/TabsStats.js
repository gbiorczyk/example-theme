import CatsTabs from './../vuejs/CatsTabs'; // eslint-disable-line

class TabsStatsCore
{
  constructor(wrapper)
  {
    this.section = wrapper;
    if (!this.setVars()) return;

    this.initVue();
    this.setEvents();
  }

  setVars()
  {
    this.compontent  = this.section.querySelector('cats-tabs');
    this.statToggles = this.section.querySelectorAll('.tabsStats__statToggle');

    return true;
  }

  initVue()
  {
    new Vue({ el: this.compontent });
  }

  setEvents()
  {
    for (let i = 0; i < this.statToggles.length; i++) {
      this.statToggles[i].addEventListener('click', (e) => {
        e.preventDefault();
      });
    }
  }
}

export default class TabsStats
{
  constructor()
  {
    this.sections = document.querySelectorAll('.tabsStats');
    if (!this.sections.length) return;

    this.sections.forEach((section) => {
      new TabsStatsCore(section);
    });
  }
}
