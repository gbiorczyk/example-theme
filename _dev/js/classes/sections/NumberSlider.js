import Swiper from 'swiper/js/swiper.min';

class NumberSliderCore
{
  constructor(wrapper)
  {
    this.section = wrapper;
    if (!this.setVars()) return;

    this.appendPopups();
    this.setClosePopupEvents();
    this.initCarousel();
    this.afterSlideChange(0);
    this.setOpenPopupEvents();
  }

  setVars()
  {
    this.wrapper = this.section.querySelector('.numberSlider__main');
    if (!this.wrapper) return;

    this.items        = this.wrapper.querySelectorAll('.numberSlider__item');
    this.popups       = this.section.querySelectorAll('.numberSlider__popup');
    this.closeButtons = this.section.querySelectorAll('.contentPopup__close');
    this.current      = this.section.querySelector('.numberSlider__navNumber--current');
    this.line         = this.section.querySelector('.numberSlider__navLineInner');

    this.settings = {
      count: this.items.length,
    };
    this.classes = {
      itemLink: 'numberSlider__itemLink',
      popupActive: 'contentPopup--active',
    };

    return true;
  }

  appendPopups()
  {
    const { length } = this.popups;
    for (let i = 0; i < length; i++) {
      document.body.appendChild(this.popups[i]);
    }
  }

  setClosePopupEvents()
  {
    const { length } = this.closeButtons;
    for (let i = 0; i < length; i++) {
      this.closeButtons[i].addEventListener('click', (e) => {
        e.preventDefault();
        this.closePopup();
      });
    }
  }

  closePopup()
  {
    this.popups.removeClass(this.classes.popupActive);
    window.triggerEvent('lockScroll', false);
  }

  setOpenPopupEvents()
  {
    const buttons    = this.section.querySelectorAll(`.${this.classes.itemLink}`);
    const { length } = buttons;
    for (let i = 0; i < length; i++) {
      buttons[i].addEventListener('click', (e) => {
        this.openPopup(e, (i % this.items.length));
      });
    }
  }

  openPopup(e, index)
  {
    if (this.popups[index] === undefined) return;
    e.preventDefault();

    this.closePopup();
    this.popups[index].addClass(this.classes.popupActive);
    window.triggerEvent('lockScroll', true);
  }

  initCarousel()
  {
    this.slider = new Swiper(this.wrapper, {
      slidesPerView: 'auto',
      loop: true,
      spaceBetween: 30,
    });
    this.slider.on('slideChange', this.afterSlideChange.bind(this));
  }

  afterSlideChange(index = null)
  {
    const current = (index === null) ? (this.slider.realIndex + 1) : (index + 1);
    this.current.innerHTML = (current < 10) ? `0${current}` : current; // eslint-disable-line
    this.updateLine(current);
  }

  updateLine(current)
  {
    const percent = current / this.settings.count * 100;
    this.line.style.width = `${percent}%`;
  }
}

export default class NumberSlider
{
  constructor()
  {
    this.sections = document.querySelectorAll('.numberSlider');
    if (!this.sections.length) return;

    this.sections.forEach((section) => {
      new NumberSliderCore(section);
    });
  }
}
