window.GoogleMapsInit = () => { // eslint-disable-line
  window.triggerEvent('googleMapsLoaded', null);
};

export default class MapTabs
{
  constructor()
  {
    if (!this.setVars()) return;

    this.loadApi();
    this.setEvents();
  }

  setVars()
  {
    this.section = document.querySelector('.mapTabs');
    if (!this.section) return;

    this.wrapper = this.section.querySelector('.mapTabs__wrapper');
    this.buttons = this.section.querySelectorAll('.tabsLine__itemLink');
    this.tabs    = this.section.querySelectorAll('.mapTabs__item');

    this.atts = {
      key: 'data-map-key',
      pin: 'data-map-pins',
      zoom: 'data-map-zoom',
      positions: 'data-map-positions',
    };
    this.classes = {
      buttonActive: 'tabsLine__itemLink--active',
      tabActive: 'mapTabs__item--active',
      location: 'mapTabs__itemLocation',
      locationActive: 'mapTabs__itemLocation--active',
      map: 'mapTabs__itemMap',
    };
    this.settings = {
      apiPath: 'https://maps.googleapis.com/maps/api/js?key=%s&callback=GoogleMapsInit',
      apiKey: this.wrapper.getAttribute(this.atts.key),
      pinPath: this.wrapper.getAttribute(this.atts.pin),
      pinFiles: {
        default: (window.orientation === undefined) ? 'map-pin.png' : 'map-pin-mobile.png',
        hover: (window.orientation === undefined) ? 'map-pin-hover.png' : 'map-pin-mobile-hover.png', // eslint-disable-line
      },
      // eslint-disable-next-line
      styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}],
    };
    this.status = {
      loadedMaps: [],
    };

    return true;
  }

  loadApi()
  {
    const path     = this.settings.apiPath.replace('%s', this.settings.apiKey);
    const tag      = document.createElement('script');
    tag.src        = path;
    const firstTag = document.getElementsByTagName('script')[0];
    firstTag.parentNode.insertBefore(tag, firstTag);
  }

  setEvents()
  {
    const { length } = this.buttons;
    for (let i = 0; i < length; i++) {
      this.buttons[i].addEventListener('click', (e) => {
        e.preventDefault();
        this.setTab(i);
      });
    }

    window.addEvent('googleMapsLoaded', this.initMaps.bind(this));
  }

  setTab(index)
  {
    this.buttons.removeClass(this.classes.buttonActive);
    this.buttons[index].addClass(this.classes.buttonActive);
    this.tabs.removeClass(this.classes.tabActive);
    this.tabs[index].addClass(this.classes.tabActive);

    this.initMap(index);
  }

  initMaps()
  {
    if (this.tabs.length === 0) return;
    this.initMap(0);
  }

  initMap(index)
  {
    if (this.status.loadedMaps[index] !== undefined) return;
    this.status.loadedMaps[index] = true;

    const container = this.tabs[index];
    this.loadMap(container);
  }

  loadMap(container)
  {
    const wrapper   = container.querySelector(`.${this.classes.map}`);
    const locations = container.querySelectorAll(`.${this.classes.location}`);
    const positions = JSON.parse(container.getAttribute(this.atts.positions));
    const center    = MapTabs.getCenterPosition(positions);

    const map = new google.maps.Map(wrapper, {
      center,
      styles: this.settings.styles,
      mapTypeControl: false,
      streetViewControl: false,
      scrollwheel: false,
      zoomControl: true,
      fullscreenControl: true,
    });
    const markers = this.addMarkers(map, positions, locations);
    MapTabs.centerMap(markers, map);
  }

  static getCenterPosition(positions)
  {
    const center = {
      lat: 0,
      lng: 0,
    };
    const { length } = positions;
    for (let i = 0; i < length; i++) {
      center.lat += parseFloat(positions[i].lat) / length;
      center.lng += parseFloat(positions[i].lng) / length;
    }
    return center;
  }

  addMarkers(map, positions, locations, index = 0)
  {
    const { settings } = this;
    const markers = [];

    const { length } = positions;
    for (let i = 0; i < length; i++) {
      markers.push(new google.maps.Marker({
        position: {
          lat: parseFloat(positions[i].lat),
          lng: parseFloat(positions[i].lng),
        },
        map,
        icon: settings.pinPath + settings.pinFiles[(i === index) ? 'hover' : 'default'],
      }));
      markers[i].addListener('click', () => {
        this.setLocation(map, positions, locations, i, markers);
      });
    }
    return markers;
  }

  setLocation(map, positions, locations, index, markers)
  {
    locations.removeClass(this.classes.locationActive);
    locations[index].addClass(this.classes.locationActive);

    const { length } = markers;
    for (let i = 0; i < length; i++) {
      markers[i].setMap(null);
    }
    this.addMarkers(map, positions, locations, index);
  }

  static centerMap(markers, map)
  {
    const bounds     = new google.maps.LatLngBounds();
    const maxZoom    = 15;
    const { length } = markers;
    for (let i = 0; i < length; i++) {
      bounds.extend(markers[i].getPosition());
    }

    map.setCenter(bounds.getCenter());
    map.fitBounds(bounds);
    map.setZoom(map.getZoom() - 1);
    if (map.getZoom() > maxZoom) map.setZoom(maxZoom);
  }
}
