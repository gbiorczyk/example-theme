class InstructionSectionCore
{
  constructor(wrapper)
  {
    this.section = wrapper;
    if (!this.setVars()) return;

    this.setEventsForButtons();
    this.setEventsForLinks();
  }

  setVars()
  {
    this.buttons      = this.section.querySelectorAll('.instructionSection__groupTitle');
    this.contents     = this.section.querySelectorAll('.instructionSection__groupContent');
    this.links        = this.getItemsByGroups('instructionSection__group', 'instructionSection__groupItemLink'); // eslint-disable-line
    this.imagesGroups = this.section.querySelectorAll('.instructionSection__imagesGroup');
    this.images       = this.getItemsByGroups('instructionSection__imagesGroup', 'instructionSection__image'); // eslint-disable-line
    this.imagesMobile = this.getItemsByGroups('instructionSection__group', 'instructionSection__image'); // eslint-disable-line
    this.media        = this.section.querySelector('.instructionSection__media');

    this.classes = {
      buttonActive: 'instructionSection__groupTitle--active',
      contentActive: 'instructionSection__groupContent--active',
      linkActive: 'instructionSection__groupItemLink--active',
      imageGroupActive: 'instructionSection__imagesGroup--active',
      imageActive: 'instructionSection__image--active',
      mediaHidden: 'instructionSection__media--hidden',
    };
    this.config = {
      scrollOffset: 80,
    };

    return true;
  }

  getItemsByGroups(parentClass, itemClass)
  {
    const parents = this.section.querySelectorAll(`.${parentClass}`);
    const list    = [];

    const { length } = parents;
    for (let i = 0; i < length; i++) {
      const items = parents[i].querySelectorAll(`.${itemClass}`);
      list.push(items);
    }
    return list;
  }

  setEventsForButtons()
  {
    const { length } = this.buttons;
    for (let i = 0; i < length; i++) {
      this.buttons[i].addEventListener('click', (e) => {
        e.preventDefault();
        this.setContent(i);
      });
    }
  }

  setEventsForLinks()
  {
    const groups = this.links.length;
    for (let i = 0; i < groups; i++) {
      const { length } = this.links[i];
      for (let j = 0; j < length; j++) {
        this.links[i][j].addEventListener('click', (e) => {
          e.preventDefault();
          this.setImage(i, j);
        });
      }
    }
  }

  setContent(index)
  {
    const isActive = this.buttons[index].hasClass(this.classes.buttonActive);
    this.toggleMedia(isActive);

    this.buttons.removeClass(this.classes.buttonActive);
    if (!isActive) this.buttons[index].addClass(this.classes.buttonActive);
    this.contents.removeClass(this.classes.contentActive);
    if (!isActive) this.contents[index].addClass(this.classes.contentActive);
    this.imagesGroups.removeClass(this.classes.imageGroupActive);
    if (!isActive) this.imagesGroups[index].addClass(this.classes.imageGroupActive);

    this.scrollToElement(this.buttons[index]);
    window.triggerEvent('refreshWindowHeight');
  }

  toggleMedia(isActive)
  {
    if (isActive) this.media.removeClass(this.classes.mediaHidden);
    else this.media.addClass(this.classes.mediaHidden);

    if (!isActive) window.triggerEvent('pauseVideo');
  }

  setImage(groupIndex, itemIndex)
  {
    this.links[groupIndex].removeClass(this.classes.linkActive);
    this.links[groupIndex][itemIndex].addClass(this.classes.linkActive);
    this.images[groupIndex].removeClass(this.classes.imageActive);
    this.images[groupIndex][itemIndex].addClass(this.classes.imageActive);
    this.imagesMobile[groupIndex].removeClass(this.classes.imageActive);
    this.imagesMobile[groupIndex][itemIndex].addClass(this.classes.imageActive);

    this.scrollToElement(this.links[groupIndex][itemIndex]);
    window.triggerEvent('refreshWindowHeight');
  }

  scrollToElement(container)
  {
    const position     = container.getBoundingClientRect();
    const windowScroll = document.body.scrollTop || document.documentElement.scrollTop;
    const scrollAdd    = position.top - this.config.scrollOffset;

    if (scrollAdd > 0) return;
    window.triggerEvent('setManualScroll', (windowScroll + scrollAdd));
  }
}

export default class InstructionSection
{
  constructor()
  {
    this.sections = document.querySelectorAll('.instructionSection');
    if (!this.sections.length) return;

    this.sections.forEach((section) => {
      new InstructionSectionCore(section);
    });
  }
}
