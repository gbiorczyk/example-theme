import Masonry from 'masonry-layout';

class PeopleListCore
{
  constructor(wrapper)
  {
    this.section = wrapper;
    if (!this.setVars()) return;

    this.setEvents();
    this.initMasonry();
  }

  setVars()
  {
    this.wrapper = this.section.querySelector('.peopleList__items');
    if (!this.wrapper) return;

    this.items  = this.wrapper.querySelectorAll('.peopleList__item--hidden');
    this.button = this.section.querySelector('.peopleList__moreButton');

    this.settings = {
      isRtl: (document.querySelectorAll('html[dir="rtl"]').length > 0),
      count: 0,
      perPage: 2,
    };
    this.classes = {
      itemHidden: 'peopleList__item--hidden',
      buttonHidden: 'peopleList__moreButton--hidden',
    };

    return true;
  }

  setEvents()
  {
    window.addEventListener('resize', this.refreshMasonry.bind(this));

    if (this.button) {
      this.button.addEventListener('click', (e) => {
        e.preventDefault();
        this.loadMore();
      });
    }
  }

  initMasonry()
  {
    this.masonry = new Masonry(this.wrapper, {
      originLeft: this.settings.isRtl,
    });
  }

  refreshMasonry()
  {
    if (!this.masonry) return;
    this.masonry.layout();
    window.triggerEvent('refreshWindowHeight');
  }

  loadMore()
  {
    const count = this.settings.count + this.settings.perPage;
    for (let i = this.settings.count; i < count; i++) {
      if (this.items[i] === undefined) continue;
      this.items[i].removeClass(this.classes.itemHidden);
    }

    this.settings.count = count;
    if (count === this.items.length) this.button.addClass(this.classes.buttonHidden);
    this.refreshMasonry();
  }
}

export default class PeopleList
{
  constructor()
  {
    this.sections = document.querySelectorAll('.peopleList');
    if (!this.sections.length) return;

    this.sections.forEach((section) => {
      new PeopleListCore(section);
    });
  }
}
