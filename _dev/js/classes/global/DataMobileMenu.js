export default class DataMobileMenu
{
  constructor()
  {
    if (!this.setVars()) return;

    this.setEvents();
  }

  setVars()
  {
    this.atts = {
      menuToggle: 'data-mobile-toggle', // add to menu toggle button, value is class for active menu toggle
      menuWrapper: 'data-mobile-wrapper', // add to menu wrapper container, value is class for active menus wrapper
      menuClass: 'data-mobile-wrapper-active', // add to menu wrapper container, value is class for active single menu
      menu: 'data-mobile-menu', // add to menu wrapper container, value is menu index
      item: 'data-mobile-item', // add to menu item, value is target menu index
      link: 'data-mobile-link', // add to menu link, value is empty
    };

    this.menuWrapper = document.querySelector(`[${this.atts.menuWrapper}]`);
    this.menuToggle  = document.querySelector(`[${this.atts.menuToggle}]`);
    if (!this.menuWrapper || !this.menuToggle) return;

    this.classes = {
      wrapperActive: this.menuWrapper.getAttribute(this.atts.menuWrapper),
      menuActive: this.menuWrapper.getAttribute(this.atts.menuClass),
      toggleActive: this.menuToggle.getAttribute(this.atts.menuToggle),
    };

    this.menus = this.menuWrapper.querySelectorAll(`[${this.atts.menu}]`);
    this.items = this.menuWrapper.querySelectorAll(`[${this.atts.item}]`);
    this.links = this.menuWrapper.querySelectorAll(`[${this.atts.link}]`);

    return true;
  }

  setEvents()
  {
    this.menuToggle.addEventListener('click', (e) => {
      e.preventDefault();
      this.toggleMenu();
    });
    window.addEvent('setMobileMenuStatus', (status) => {
      if (!status) this.closeMenu();
    });

    const { length } = this.links;
    for (let i = 0; i < length; i++) {
      this.links[i].addEventListener('click', (e) => {
        e.preventDefault();
        this.setMenu(i);
      });
    }
  }

  toggleMenu()
  {
    this.menuToggle.toggleClass(this.classes.toggleActive);
    this.menuWrapper.toggleClass(this.classes.wrapperActive);

    const isOpen = this.menuWrapper.hasClass(this.classes.wrapperActive);
    window.triggerEvent('lockScroll', isOpen);
    window.triggerEvent('mobileMenuStatus', isOpen);
  }

  closeMenu()
  {
    this.menuToggle.removeClass(this.classes.toggleActive);
    this.menuWrapper.removeClass(this.classes.wrapperActive);

    window.triggerEvent('lockScroll', false);
  }

  setMenu(index)
  {
    const submenuIndex = this.items[index].getAttribute(this.atts.item);
    const currentMenu  = this.getSubmenu(submenuIndex);
    if (!currentMenu) return;

    this.menus.removeClass(this.classes.menuActive);
    currentMenu.addClass(this.classes.menuActive);
  }

  getSubmenu(index)
  {
    const { length } = this.menus;
    for (let i = 0; i < length; i++) {
      const menuIndex = this.menus[i].getAttribute(this.atts.menu);
      if (menuIndex === index) return this.menus[i];
    }
    return null;
  }
}
