export default class DataFullHeight
{
  constructor()
  {
    if (!this.setVars()) return;

    this.setEvents();
  }

  setVars()
  {
    this.atts = {
      wrapper: 'data-full-height', // add to section container, value is empty
    };

    this.sections = document.querySelectorAll(`[${this.atts.wrapper}]`);
    if (!this.sections.length) return;

    this.status = {
      windowWidth: window.innerWidth,
    };

    return true;
  }

  setEvents()
  {
    window.addEventListener('load', this.setSectionsHeight.bind(this));
    window.addEventListener('resize', this.refreshHeights.bind(this));
  }

  setSectionsHeight()
  {
    const { length } = this.sections;
    for (let i = 0; i < length; i++) {
      DataFullHeight.setSectionHeight(this.sections[i]);
    }
    this.status.windowWidth = window.innerWidth;
  }

  static setSectionHeight(section)
  {
    const value = window.innerHeight;
    section.style.height = `${value}px`;
  }

  refreshHeights()
  {
    if (this.status.windowWidth === window.innerWidth) return;
    this.setSectionsHeight();
  }
}
