export default class DataScrollId
{
  constructor()
  {
    if (!this.setVars()) return;

    this.setEvents();
  }

  setVars()
  {
    this.atts = {
      item: 'data-scroll-id', // add to DOM element
    };

    this.sections = document.querySelectorAll(`[${this.atts.item}]`);
    this.buttons  = document.querySelectorAll('a[href*="#"]');
    if (!this.sections.length || !this.buttons.length) return;

    this.config = {
      offsetTop: 90,
    };

    return true;
  }

  setEvents()
  {
    window.addEventListener('load', this.findSection.bind(this));

    this.buttons.forEach((item) => {
      item.addEventListener('click', (e) => {
        const href = item.getAttribute('href');
        if (this.scrollToId(href)) {
          e.preventDefault();
        }
      });
    });

    window.addEvent('scrollToDataId', (section) => {
      this.scrollToId(`#${section}`);
    });
  }

  scrollToId(link)
  {
    const parts = link.split('#');
    const hash  = (parts.length > 1) ? parts[1] : '';
    if (!hash) return;

    const sections = document.querySelectorAll(`[${this.atts.item}="${hash}"]`);
    if (!sections.length) {
      return false;
    } else {
      history.replaceState(null, document.title, parts[0]); // eslint-disable-line
      this.scrollToSection(sections);
      return true;
    }
  }

  scrollToSection(sections, index = 0)
  {
    const position = sections[index].getBoundingClientRect();
    if ((position.top === 0) && (sections[index + 1] !== undefined)) {
      return this.scrollToSection(sections, (index + 1));
    }

    const windowScroll = document.body.scrollTop || document.documentElement.scrollTop;
    const scroll       = position.top + (windowScroll - this.config.offsetTop);
    window.triggerEvent('setManualScroll', scroll);
    window.triggerEvent('closeMobileMenu');
    return true;
  }

  findSection()
  {
    const { href } = window.location;
    this.scrollToId(href);
  }
}
