import { TweenLite, ScrollToPlugin } from 'gsap/all';
// eslint-disable-next-line
const plugins = [ScrollToPlugin]; // correct importing on production

export default class DataScroll
{
  constructor()
  {
    if (!this.setVars()) return;

    DataScroll.setTweenConfig();
    this.getConfig(true);
    this.setEvents();
    this.setBarEvents();
  }

  setVars()
  {
    this.atts = {
      body: 'data-scroll', // optionally; add to body, value is empty
      bar: 'data-scroll-bar', // add to scroll bar container, value is empty
      barButton: 'data-scroll-bar-button', // add to draggable button inside scroll bar, value is empty
    };

    this.body      = document.querySelector(`[${this.atts.body}]`);
    this.scrollBar = document.querySelector(`[${this.atts.bar}]`);
    if (!this.body || !this.scrollBar) return;

    this.scrollButton = this.scrollBar.querySelector(`[${this.atts.barButton}]`);

    this.settings = {
      scroll: {
        top: null,
        step: 120,
        max: null,
      },
      barHeight: null,
      keyCodes: {
        arrowUp: 38,
        arrowDown: 40,
        pageUp: 33,
        pageDown: 34,
        home: 36,
        end: 35,
      },
    };
    this.status = {
      isMobile: this.detectMobile(),
      isDisabled: false,
    };

    return true;
  }

  detectMobile()
  {
    const isMobile = (typeof window.orientation !== 'undefined')
      || (navigator.userAgent.indexOf('IEMobile') !== -1);
    if (isMobile) this.body.removeAttribute('data-scroll');
    return isMobile;
  }

  setEvents()
  {
    /* ---
      Config
    --- */
    window.addEventListener('load', this.getConfig.bind(this));
    window.addEventListener('resize', this.getConfig.bind(this));
    window.addEventListener('wpfFormUpdate', this.getConfig.bind(this));
    window.addEvent('refreshWindowHeight', this.getConfig.bind(this));

    /* ---
      Scroll triggers
    --- */
    window.addEventListener('DOMMouseScroll', this.scrollPage.bind(this), { passive: false });
    window.addEventListener('mousewheel', this.scrollPage.bind(this), { passive: false });
    window.addEventListener('keydown', this.scrollPageByKey.bind(this));

    /* ---
      Events
    --- */
    window.addEvent('lockScroll', (status) => {
      this.status.isDisabled = (status === true);
    });
    window.addEvent('setManualScroll', (value) => {
      this.updateScrollValue(value, true);
    });
  }

  setBarEvents()
  {
    const startEvent = () => {
      window.addEventListener('mousemove', actionEvent); // eslint-disable-line
      window.addEventListener('mouseup', endEvent); // eslint-disable-line
      this.scrollButton.removeEventListener('mousedown', startEvent);
    };
    const actionEvent = (e) => {
      const percent = e.clientY / this.settings.barHeight;
      this.scrollToPosition(this.settings.scroll.max * percent);
    };
    const endEvent = () => {
      window.removeEventListener('mousemove', actionEvent);
      window.removeEventListener('mouseup', endEvent);
      this.scrollButton.addEventListener('mousedown', startEvent);
    };

    this.scrollButton.addEventListener('mousedown', startEvent);
    this.scrollBar.addEventListener('click', actionEvent);
  }

  /* ---
    Config
  --- */

  static setTweenConfig()
  {
    TweenLite.lagSmoothing();
    TweenLite.ticker.fps(60); // eslint-disable-line
  }

  getConfig(isDefault = false)
  {
    const scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
    this.settings.scroll.max = (document.body.scrollHeight - window.innerHeight);
    if (isDefault || (scrollTop > this.settings.scroll.max)) this.settings.scroll.top = scrollTop;
    this.settings.barHeight = this.scrollBar.offsetHeight;
    this.updateScrollbar(scrollTop);
  }

  /* ---
    Init scrolling
  --- */

  scrollPage(e)
  {
    if (this.status.isMobile || this.status.isDisabled
      || (DataScroll.preventScrolling(e) === true)) return;
    e.preventDefault();

    let delta = 0;
    if (e.wheelDelta) delta = e.wheelDelta / 120; // eslint-disable-line
    else if (e.detail) delta = -e.detail / 3; // eslint-disable-line

    const value = this.settings.scroll.top + (-delta * this.settings.scroll.step);
    this.updateScrollValue(value);
  }

  static preventScrolling(e)
  {
    let element = e.target;

    while ((element !== document) && (element.nodeName.toLowerCase() !== 'body')) {
      const overflowY  = window.getComputedStyle(element, null).getPropertyValue('overflow-y');
      const isScrollUp = e.wheelDelta ? (-e.wheelDelta < 0) : 0;

      /* ---
        Block body scrolling if any container inside body is scrollable and scrolling position is not maximum
      --- */
      if (((overflowY === 'scroll') || (overflowY === 'auto'))
        && ((!isScrollUp && (element.scrollTop < (element.scrollHeight - element.clientHeight)))
          || (isScrollUp && (element.scrollTop > 0)))) return true;

      element = element.parentNode;
    }

    return false;
  }

  updateScrollValue(value, isManualScroll = false)
  {
    let scroll = value;
    if (scroll < 0) scroll = 0;
    else if (scroll > this.settings.scroll.max) scroll = this.settings.scroll.max;
    if (scroll === this.settings.scroll.top) return;
    this.scrollToPosition(scroll, isManualScroll);
  }

  /* ---
    Keyboard
  --- */

  scrollPageByKey(e)
  {
    if (e.target.nodeName.toLowerCase() !== 'body') return;
    const { settings } = this;
    let value          = null;

    switch (e.keyCode) {
    case settings.keyCodes.arrowUp:
      value = settings.scroll.top - settings.scroll.step;
      break;
    case settings.keyCodes.arrowDown:
      value = settings.scroll.top + settings.scroll.step;
      break;
    case settings.keyCodes.pageUp:
      value = settings.scroll.top - window.innerHeight;
      break;
    case settings.keyCodes.pageDown:
      value = settings.scroll.top + window.innerHeight;
      break;
    case settings.keyCodes.home:
      value = 0;
      break;
    case settings.keyCodes.end:
      value = settings.scroll.max;
      break;
    default:
      break;
    }

    if (value === null) return;
    e.preventDefault();
    this.updateScrollValue(value);
  }

  /* ---
    Scrolling animation
  --- */

  scrollToPosition(scroll, isManualScroll)
  {
    this.settings.scroll.top = scroll;
    TweenLite.to(window, 1, {
      force3D: true,
      scrollTo: {
        y: scroll,
        autoKill: false,
      },
      ease: Power1.easeOut,
      onUpdate: () => {
        const value = document.body.scrollTop || document.documentElement.scrollTop;
        window.triggerEvent('afterCustomScroll', value);
        if (isManualScroll) window.triggerEvent('afterManualScroll', value);
        this.updateScrollbar(value);
      },
    });
  }

  updateScrollbar(newScroll)
  {
    const percent = newScroll / this.settings.scroll.max * 100;
    this.scrollButton.style.height = `${percent}%`;
    this.scrollButton.style.borderRadius = (percent === 100) ? '0px' : '';
  }
}
