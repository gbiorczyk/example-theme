class DataStickyCore
{
  constructor(wrapper, atts)
  {
    this.wrapper = wrapper;
    this.atts    = atts;
    if (!this.setVars()) return;

    this.setEvents();
    this.refreshConfig();
  }

  setVars()
  {
    this.sidebar = this.wrapper.querySelector(`[${this.atts.sidebar}]`);
    if (!this.sidebar) return;

    this.config = {
      status: 'default',
      offsetTop: 70,
      scroll: {
        min: 0,
        max: 0,
        inner: 0,
      },
      position: {
        left: 0,
        width: 0,
        transform: 0,
      },
    };

    return true;
  }

  setEvents()
  {
    window.addEvent('afterCustomScroll', this.setStickySidebar.bind(this));

    window.addEventListener('resize', this.refreshConfig.bind(this));
    window.addEventListener('wpfFormUpdate', this.refreshConfig.bind(this));
    window.addEvent('refreshWindowHeight', this.refreshConfig.bind(this));
  }

  refreshConfig()
  {
    this.setSidebarStatus('default');

    const scrollTop  = document.body.scrollTop || document.documentElement.scrollTop;
    const posWrapper = this.wrapper.getBoundingClientRect();
    const posSidebar = this.sidebar.getBoundingClientRect();

    const { offsetTop, scroll, position } = this.config;
    scroll.min   = scrollTop + posSidebar.top - offsetTop;
    scroll.max   = scroll.min + (posWrapper.height - posSidebar.height);
    scroll.inner = this.sidebar.scrollHeight - posSidebar.height;

    position.left      = posSidebar.left;
    position.width     = posSidebar.width;
    position.transform = (posWrapper.height - posSidebar.height);

    this.setStickySidebar();
  }

  setStickySidebar()
  {
    const value = document.body.scrollTop || document.documentElement.scrollTop;
    const { scroll } = this.config;
    if (value < scroll.min) this.setSidebarStatus('default');
    else if (value > scroll.max) this.setSidebarStatus('blocked');
    else this.setSidebarStatus('sticky');
  }

  setSidebarStatus(status)
  {
    if (this.config.status === status) return;
    this.config.status = status;

    switch (status) {
    case 'blocked':
      this.setPositionBlocked();
      break;
    case 'sticky':
      this.setPositionSticky();
      break;
    default:
      this.setPositionDefault();
      break;
    }
  }

  setPositionDefault()
  {
    this.sidebar.style.position  = '';
    this.sidebar.style.top       = '';
    this.sidebar.style.left      = '';
    this.sidebar.style.width     = '';
    this.sidebar.style.transform = '';
  }

  setPositionBlocked()
  {
    this.setPositionDefault();
    const { position } = this.config;

    this.sidebar.style.transform = `translateY(${position.transform}px)`;
  }

  setPositionSticky()
  {
    const { offsetTop, position } = this.config;

    this.sidebar.style.position  = 'fixed';
    this.sidebar.style.top       = `${offsetTop}px`;
    this.sidebar.style.left      = `${position.left}px`;
    this.sidebar.style.width     = `${position.width}px`;
    this.sidebar.style.transform = '';
  }
}

export default class DataSticky
{
  constructor()
  {
    this.atts = {
      wrapper: 'data-sticky', // add to select container, value is empty
      sidebar: 'data-sticky-sidebar', // add to sticky element, value is empty
    };

    this.sections = document.querySelectorAll(`[${this.atts.wrapper}]`);
    if (!this.sections.length) return;

    this.sections.forEach((section) => {
      new DataStickyCore(section, this.atts);
    });
  }
}
