export default class DataVideo
{
  constructor()
  {
    this.setVars();
    this.setEvents();
    this.bindVideoPlayEvents();
  }

  setVars()
  {
    this.atts = {
      container: 'data-video', // add to video container, value is video ID or video URL
      poster: 'data-video-poster', // optionally; add to video poster, value is class to hiding this element
      play: 'data-video-play', // optionally; add to play button, value is class for loading animation on this element (value is optionally)
      output: 'data-video-output', // add to video output container, value is empty
    };
    this.config = {
      apiUrl: {
        youtube: 'https://www.youtube.com/player_api',
        vimeo: 'https://player.vimeo.com/api/player.js',
      },
      mode: [
        {
          regex: new RegExp('^.*(youtu.be/|v/|embed/|v=|^)([a-zA-Z0-9-_]{11}).*$'),
          type: 'youtube',
        },
        {
          regex: new RegExp('^.*(vimeo.com/|video/|^)([0-9]{6,11}).*$'),
          type: 'vimeo',
        },
      ],
      detect: {
        youtube: () => (typeof YT !== 'undefined') && (typeof YT.Player !== 'undefined'),
        vimeo: () => (typeof Vimeo !== 'undefined') && (typeof Vimeo.Player !== 'undefined'),
      },
      detectStep: 10,
    };

    this.videos = this.getVideos();
    this.status = {
      apiReady: {
        youtube: '',
        vimeo: '',
      },
    };
  }

  setEvents()
  {
    /* ---
      Push CustomEvent (named playVideo binded on window) to trigger function
      (as detail give video wrapper - DOM element with data-video attribute, example below)

      window.dispatchEvent(new CustomEvent('playVideo', {
        detail: document.querySelector('[data-video]'),
      }));
    --- */
    window.addEventListener('playVideo', (e) => {
      const index = this.findVideoWrapper(e.detail);
      if (index) this.initVideo(this.videos[index], index);
    });

    /* ---
      Push CustomEvent (named pauseVideo binded on window) to trigger function
      (example below)

      window.dispatchEvent(new CustomEvent('pauseVideo'));
    --- */
    window.addEventListener('pauseVideo', () => {
      this.pauseOtherVideos();
    });
  }

  findVideoWrapper(wrapper)
  {
    const { length } = this.videos;
    for (let i = 0; i < length; i++) {
      if (this.videos[i].wrapper !== wrapper) continue;
      return i;
    }
    return this.addNewVideo(wrapper);
  }

  addNewVideo(wrapper)
  {
    const data = this.getVideoObject(wrapper);
    if (!data) return;
    this.videos.push(data);
    return (this.videos.length - 1);
  }

  /* ---
    Videos list
  --- */
  getVideos()
  {
    const sections = document.querySelectorAll(`[${this.atts.container}]`);
    const list     = [];
    const { length } = sections;
    for (let i = 0; i < length; i++) {
      const data = this.getVideoObject(sections[i]);
      if (data) list.push(data);
    }
    return list;
  }

  getVideoObject(player)
  {
    const poster = player.querySelector(`[${this.atts.poster}]`);
    const play   = player.querySelector(`[${this.atts.play}]`);
    const output = player.querySelector(`[${this.atts.output}]`);
    if (!output) return;

    const videoData = this.getVideoData(player.getAttribute(this.atts.container));
    if (!videoData) return;

    const data = {
      wrapper: player,
      poster,
      posterClass: poster ? poster.getAttribute(this.atts.poster) : '',
      play,
      playClass: play ? play.getAttribute(this.atts.play) : '',
      output,
    };
    data.videoType = videoData.videoType;
    data.videoId   = videoData.videoId;
    return data;
  }

  getVideoData(url)
  {
    const { length } = this.config.mode;
    for (let i = 0; i < length; i++) {
      const matches = url.match(this.config.mode[i].regex);
      if (!matches) continue;
      return {
        videoType: this.config.mode[i].type,
        videoId: matches[2],
      };
    }
    return null;
  }

  /* ---
    Init playing
  --- */
  bindVideoPlayEvents()
  {
    const { length } = this.videos;
    for (let i = 0; i < length; i++) {
      this.videos[i].play.addEventListener('click', (e) => {
        e.preventDefault();
        this.initVideo(this.videos[i], i);
      });
    }
  }

  initVideo(data, index)
  {
    if (this.config.apiUrl[data.videoType] === undefined) return;
    if (data.playClass) data.play.classList.add(data.playClass);

    if (this.status.apiReady[data.videoType]) {
      this.playVideo(data, index);
    } else {
      this.loadApi(data.videoType, () => {
        this.status.apiReady[data.videoType] = true;
        this.playVideo(data, index);
      });
    }
  }

  loadApi(videoType, callback)
  {
    const tag      = document.createElement('script');
    tag.src        = this.config.apiUrl[videoType];
    const firstTag = document.getElementsByTagName('script')[0];
    firstTag.parentNode.insertBefore(tag, firstTag);

    const isApiReady = () => {
      if (this.config.detect[videoType]()) callback();
      else setTimeout(() => { isApiReady(); }, this.config.detectStep);
    };
    isApiReady();
  }

  /* ---
    Play video
  --- */
  playVideo(data, index)
  {
    if (data.videoType === 'youtube') this.playYoutubeVideo(data, index);
    else if (data.videoType === 'vimeo') this.playVimeoVideo(data, index);
  }

  playYoutubeVideo(data, index)
  {
    if (this.videos[index].player !== undefined) {
      data.player.playVideo();
      return;
    }

    this.videos[index].player = new YT.Player(data.output, {
      videoId: data.videoId,
      playerVars: {
        controls: 1,
        loop: 1,
        rel: 0,
        showinfo: 0,
        ecver: 2,
        origin: `${window.location.protocol}//${window.location.hostname}`,
      },
      events: {
        onReady: () => {
          data.player.playVideo();
        },
        onStateChange: (e) => {
          if (e.data === YT.PlayerState.PAUSED) this.afterVideoPause(index);
          if (e.data !== YT.PlayerState.PLAYING) return;

          this.pauseOtherVideos(index);
          if (data.poster) data.poster.classList.add(data.posterClass);
          if (data.playClass) data.play.classList.remove(data.playClass);
        },
      },
    });
  }

  playVimeoVideo(data, index)
  {
    if (this.videos[index].player !== undefined) {
      this.videos[index].player.play();
      return;
    }

    this.videos[index].player = new Vimeo.Player(data.output, {
      id: data.videoId,
      responsive: true,
      background: true,
    });
    this.videos[index].player.play();

    this.videos[index].player.on('play', () => {
      this.videos[index].player.on('timeupdate', () => {
        this.videos[index].player.off('timeupdate');
        this.pauseOtherVideos(index);
        if (data.poster) data.poster.classList.add(data.posterClass);
        if (data.playClass) data.play.classList.remove(data.playClass);
      });
    });
  }

  pauseOtherVideos(index = -1)
  {
    const { length } = this.videos;
    for (let i = 0; i < length; i++) {
      if ((i === index) || (this.videos[i].player === undefined)) continue;

      if (this.videos[i].videoType === 'youtube') this.videos[i].player.pauseVideo();
      else if (this.videos[i].videoType === 'vimeo') this.videos[i].player.pause();
      this.afterVideoPause(i);
    }
  }

  afterVideoPause(index)
  {
    if (!this.videos[index].poster) return;
    this.videos[index].poster.classList.remove(this.videos[index].posterClass);
  }
}
