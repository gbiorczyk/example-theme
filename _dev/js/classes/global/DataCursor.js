class DataCursorCore
{
  constructor(wrapper, atts)
  {
    this.section = wrapper;
    this.atts    = atts;
    if (!this.setVars()) return;

    this.getWrapperCenter();
    this.getForceOfItems();
    this.setEvents();
  }

  setVars()
  {
    this.wrapper = this.section.querySelector(`[${this.atts.items}]`);
    this.items   = this.wrapper.querySelectorAll(`[${this.atts.item}]`);

    this.settings = {
      isLocked: false,
      wrapper: {
        x: 0,
        y: 0,
        width: 0,
        height: 0,
      },
      forces: [],
      count: this.items.length,
      offset: 120,
      fpsTime: (1000 / 120), // eslint-disable-line
    };

    return true;
  }

  getWrapperCenter()
  {
    const position = this.section.getBoundingClientRect();
    this.settings.wrapper.x      = position.left;
    this.settings.wrapper.y      = position.top;
    this.settings.wrapper.width  = position.width / 2;
    this.settings.wrapper.height = position.height / 2;
  }

  getForceOfItems()
  {
    const position = this.wrapper.getBoundingClientRect();

    const items = [];
    for (let i = 0; i < this.settings.count; i++) {
      items.push(this.getForceOfItem(this.items[i], position));
    }
    this.settings.forces = items;
  }

  getForceOfItem(item, wrapper)
  {
    const position = item.getBoundingClientRect();
    const offsetX  = (position.left - wrapper.left - wrapper.width) / (wrapper.width * 2);
    const offsetY  = (position.top - wrapper.top - wrapper.height) / (wrapper.height * 2);
    return {
      forceX: offsetX * this.settings.offset,
      forceY: offsetY * this.settings.offset,
      offsetX: 0,
      offsetY: 0,
    };
  }

  setEvents()
  {
    this.section.addEventListener('mousemove', this.runAnimation.bind(this));
  }

  runAnimation(e)
  {
    if (this.settings.isLocked) return;

    this.settings.isLocked = true;
    setTimeout(() => {
      this.settings.isLocked = false;
    }, this.settings.fpsTime);

    this.animateItems(e);
  }

  animateItems(e)
  {
    const { wrapper } = this.settings;
    const { forces }  = this.settings;
    const { items }   = this;
    const percentX    = ((e.clientX - wrapper.x) / wrapper.width) - 1;
    const percentY    = ((e.clientY - wrapper.y) / wrapper.height) - 1;

    for (let i = 0; i < this.settings.count; i++) {
      DataCursorCore.animateItem(percentX, percentY, items[i], forces[i]);
    }
  }

  static animateItem(percentX, percentY, item, data)
  {
    const offsetX = percentX * data.forceX;
    const offsetY = percentY * data.forceY;

    if (offsetX >= 0) data.offsetX = Math.min((data.offsetX + 1), offsetX);
    else data.offsetX = Math.max((data.offsetX - 1), offsetX);
    if (offsetY >= 0) data.offsetY = Math.min((data.offsetY + 1), offsetY);
    else data.offsetY = Math.max((data.offsetY - 1), offsetY);

    item.style.transform = `translate(${data.offsetX}px, ${data.offsetY}px)`;
  }
}

export default class DataCursor
{
  constructor()
  {
    this.atts = {
      wrapper: 'data-cursor', // section container, value is empty
      items: 'data-cursor-items', // wrapper of elements, value is empty
      item: 'data-cursor-item', // movable element, value is percent of animation (0-100)
    };

    this.sections = document.querySelectorAll(`[${this.atts.wrapper}]`);
    if (!this.sections.length) return;

    this.sections.forEach((section) => {
      new DataCursorCore(section, this.atts);
    });
  }
}
