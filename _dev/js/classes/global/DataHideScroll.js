export default class DataHideScroll
{
  constructor()
  {
    if (!this.setVars()) return;

    this.setEvents();
  }

  setVars()
  {
    this.atts = {
      body: 'data-hide-scroll-wrapper', // optionally; add to body, value is empty
      item: 'data-hide-scroll', // add to DOM element, value is empty
    };

    this.body = document.querySelector(`[${this.atts.body}]`);
    if (!this.body) return;

    this.config = {
      isRtl: (document.querySelectorAll('html[dir="rtl"]').length > 0),
      step: 100,
      move: 50, // eslint-disable-line
      offset: {
        top: 60,
        window: 0.75, // central point of animation (100% completion, value is window height percent)
        animation: 0.25, // distance of animation from central point (value is window height percent)
        maxLength: 150, // maximum length of element animation (value in px)
      },
      position: {
        beforeScroll: 0,
        afterScroll: 0,
      },
    };
    this.list    = [];
    this.timeout = null;

    return true;
  }

  setEvents()
  {
    window.addEventListener('load', () => {
      this.refreshConfig();
      this.getItems();
      this.showItems();
    });
    window.addEventListener('resize', () => {
      this.refreshConfig();
      this.getItems();
    });
    window.addEventListener('wpfFormUpdate', this.refreshAfterWindowResize.bind(this));
    window.addEvent('refreshWindowHeight', this.refreshAfterWindowResize.bind(this));
    window.addEvent('afterCustomScroll', this.showItems.bind(this));
  }

  refreshAfterWindowResize()
  {
    this.refreshConfig();
    this.getItems();
    this.showItems();
  }

  refreshConfig()
  {
    clearTimeout(this.timeout);
    this.config.position.beforeScroll = window.innerHeight * this.config.offset.animation;
    this.config.position.afterScroll  = this.config.position.beforeScroll + window.innerHeight;
  }

  getItems()
  {
    const items = this.body.querySelectorAll(`[${this.atts.item}]`);
    if (!items.length) return;

    this.list    = [];
    const { offset } = this.config;
    const center     = window.innerHeight * offset.window;

    const { length } = items;
    for (let i = 0; i < length; i++) {
      const data = this.getItem(items[i], center, offset);
      if (data !== null) this.list.push(data);
    }
  }

  getItem(item, windowCenter, config)
  {
    const data = DataHideScroll.getItemData(item, config);
    if (data.top < window.innerHeight) return null;
    return {
      dom: item,
      mode: item.getAttribute(this.atts.item),
      top: data.top,
      center: (data.center - windowCenter),
      bottom: data.bottom,
    };
  }

  static getItemData(item, config)
  {
    const scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
    const position  = item.getBoundingClientRect();
    const height    = (window.innerHeight < position.height) ? window.innerHeight : position.height;
    const offset    = position.top + scrollTop;
    const data      = {
      top: offset,
      center: offset + Math.min(((height - config.top) / 2), config.maxLength),
      bottom: offset + position.height,
    };
    return data;
  }

  showItems(scrollValue = null)
  {
    let scroll = scrollValue;
    if (scroll === null) scroll = (document.body.scrollTop || document.documentElement.scrollTop);

    const { list }     = this;
    const config       = this.config.position;
    const scrollTop    = scroll - config.beforeScroll;
    const scrollBottom = scroll + config.afterScroll;

    const { length } = list;
    for (let i = 0; i < length; i++) {
      if ((list[i].top > scrollBottom) || (list[i].bottom < scrollTop)) continue;

      this.setStyles(
        list[i],
        Math.max(Math.min((list[i].center - scroll), config.beforeScroll), 0) / config.beforeScroll
      );
    }
  }

  setStyles(item, percent)
  {
    let transValue  = percent * this.config.move;
    const itemStyle = item.dom.style;

    itemStyle.opacity = Math.min(((1 - percent) * 2), 1);
    switch (item.mode) {
    case 'left':
      if (this.config.isRtl) transValue = -transValue;
      itemStyle.transform = `translateX(${-transValue}px)`;
      break;
    case 'right':
      if (this.config.isRtl) transValue = -transValue;
      itemStyle.transform = `translateX(${transValue}px)`;
      break;
    case 'top':
      itemStyle.transform = `translateY(${-transValue}px)`;
      break;
    default:
      itemStyle.transform = `translateY(${transValue}px)`;
      break;
    }
  }
}
