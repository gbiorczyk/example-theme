export default class DataHash
{
  constructor()
  {
    if (!this.setVars()) return;

    this.setEvents();
  }

  setVars()
  {
    this.items = document.querySelectorAll('a[href="#"]');
    if (!this.items.length) return;

    return true;
  }

  setEvents()
  {
    const { length } = this.items;
    for (let i = 0; i < length; i++) {
      this.items[i].addEventListener('click', (e) => {
        e.preventDefault();
      });
    }
  }
}
