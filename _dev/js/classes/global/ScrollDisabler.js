export default class ScrollDisabler
{
  constructor()
  {
    this.setVars();
    this.setEvents();
  }

  setVars()
  {
    this.settings = {
      touchStart: 0,
      scrollY: 0,
      events: {
        touch: this.updateTouchStart.bind(this),
        scroll: this.preventScrolling.bind(this),
        lockScroll: this.restoreScrollPostion.bind(this),
      },
    };
  }

  setEvents()
  {
    window.addEvent('lockScroll', (status) => {
      if (status === true) this.lockScroll();
      else this.unlockScroll();
    });
  }

  lockScroll()
  {
    this.scrollY = document.body.scrollTop || document.documentElement.scrollTop;

    window.addEventListener('mousewheel', this.settings.events.scroll, { passive: false });
    window.addEventListener('touchmove', this.settings.events.scroll, { passive: false });
    window.addEventListener('touchstart', this.settings.events.touch);
    window.addEventListener('scroll', this.settings.events.lockScroll);
  }

  unlockScroll()
  {
    window.removeEventListener('mousewheel', this.settings.events.scroll, { passive: false });
    window.removeEventListener('touchmove', this.settings.events.scroll, { passive: false });
    window.removeEventListener('touchstart', this.settings.events.touch);
    window.removeEventListener('scroll', this.settings.events.lockScroll);
  }

  updateTouchStart(e)
  {
    this.settings.touchStart = e.touches[0].pageY;
  }

  preventScrolling(e)
  {
    let element          = e.target;
    const { touchStart } = this.settings;

    while ((element !== document) && (element.nodeName.toLowerCase() !== 'body')) {
      const overflowY  = window.getComputedStyle(element, null).getPropertyValue('overflow-y');
      const isScrollUp = e.wheelDelta ? (-e.wheelDelta < 0) : (e.touches[0].pageY > touchStart);

      /* ---
        Do not block scrolling if any container inside body is scrollable and scrolling position is not maximum
      --- */
      if (((overflowY === 'scroll') || (overflowY === 'auto'))
        && ((!isScrollUp && (element.scrollTop < (element.scrollHeight - element.clientHeight)))
          || (isScrollUp && (element.scrollTop > 0)))) return true;

      element = element.parentNode;
    }

    if (e.cancelable) e.preventDefault();
    return true;
  }

  restoreScrollPostion()
  {
    window.scroll(0, this.scrollY);
  }
}
