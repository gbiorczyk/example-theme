class DataSelectCore
{
  constructor(index, atts)
  {
    this.index = index;
    this.atts  = atts;
    if (!this.setVars()) return;

    this.setEvents();
    this.buildSelect();
    this.sendSelectEvent();
    this.setSelectEvents();
    this.setOptionsEvents();
  }

  setVars()
  {
    this.wrappers = document.querySelectorAll(`[${this.atts.wrapper}]`);
    if (!this.wrappers.length || (this.wrappers[this.index] === undefined)) return;

    this.select = this.wrappers[this.index].querySelector('select');
    if (!this.select) return;

    this.values    = this.select.querySelectorAll('option');
    this.container = this.wrappers[this.index].querySelector(`[${this.atts.container}]`);
    this.button    = this.wrappers[this.index].querySelector(`[${this.atts.button}]`);
    this.options   = this.wrappers[this.index].querySelectorAll(`[${this.atts.option}]`);

    return true;
  }

  setEvents()
  {
    window.addEventListener('wpfFormReady', this.refreshEvents.bind(this));
  }

  refreshEvents()
  {
    if (!this.setVars()) return;

    this.buildSelect();
    this.setSelectEvents();
    this.setOptionsEvents();
  }

  buildSelect()
  {
    if (this.options.length > 0) return;

    const select   = this.getSelectElement();
    this.container = select.querySelector(`[${this.atts.container}]`);
    this.button    = select.querySelector(`[${this.atts.button}]`);
    this.options   = select.querySelectorAll(`[${this.atts.option}]`);

    const selected = this.select.querySelector('option[selected]');
    this.setActiveOption(selected ? this.select.selectedIndex : 0);
  }

  getSelectElement()
  {
    const wrapper     = document.createElement('div');
    wrapper.innerHTML = this.getSelectHtml();

    const select = wrapper.children[0];
    this.select.parentNode.insertBefore(select, this.select);
    return select;
  }

  getSelectHtml()
  {
    return `<div ${this.atts.container}>
      <button type="button" ${this.atts.button}>${this.values[0].innerHTML}</button>
      <ul ${this.atts.options}>
        ${this.getValuesHtml()}
      </ul>
    </div>`;
  }

  getValuesHtml()
  {
    let code = '';
    const { length } = this.values;
    for (let i = 0; i < length; i++) {
      const atts = this.getValueAtts(this.values[i]);
      code += `<li ${atts}>${this.values[i].innerHTML}</li>`;
    }
    return code;
  }

  getValueAtts(value)
  {
    const atts = [this.atts.option];
    if (value.getAttribute('disabled') !== null) atts.push(this.atts.optionDisabled);
    if (value.getAttribute('selected') !== null) atts.push(this.atts.optionActive);
    atts.push(`data-text="${value.innerHTML}"`);
    return atts.join(' ');
  }

  setSelectEvents()
  {
    this.select.addEventListener('change', () => {
      this.setActiveOption(this.select.selectedIndex, true);
    });

    const closeEvent = (e) => {
      e.preventDefault();
      this.container.removeAttribute(this.atts.containerOpen);
      document.removeEventListener('click', closeEvent);
    };

    this.button.addEventListener('click', (e) => {
      e.preventDefault();
      this.container.setAttribute(this.atts.containerOpen, '');
      document.removeEventListener('click', closeEvent);
      setTimeout(() => { document.addEventListener('click', closeEvent); }, 1);
    });
  }

  setOptionsEvents()
  {
    const { length } = this.options;
    for (let i = 0; i < length; i++) {
      this.options[i].addEventListener('click', (e) => {
        e.preventDefault();
        this.setActiveOption(i);
      });
    }
  }

  setActiveOption(index = 0, isEvent = false)
  {
    const { length } = this.options;
    for (let i = 0; i < length; i++) {
      if (i === index) this.options[i].setAttribute(this.atts.optionActive, '');
      else this.options[i].removeAttribute(this.atts.optionActive);
    }

    this.button.innerHTML = this.values[index].innerHTML;
    this.select.value     = this.values[index].getAttribute('value');
    this.button.setAttribute(this.atts.buttonValue, this.select.value);
    if (!isEvent) this.sendSelectEvent();
  }

  sendSelectEvent()
  {
    const event = document.createEvent('HTMLEvents');
    event.initEvent('change', true, true);
    this.select.dispatchEvent(event);
  }
}

export default class DataSelect
{
  constructor()
  {
    this.atts = {
      wrapper: 'data-select', // add to select container, value is empty
        // add select HTML tag inside container
      container: 'data-select-container', // dynamically added
      containerOpen: 'data-select-container-open', // dynamically added
      button: 'data-select-button', // dynamically added
      buttonValue: 'data-select-button-value', // dynamically added
      options: 'data-select-options', // dynamically added
      option: 'data-select-option', // dynamically added
      optionActive: 'data-select-option-active', // dynamically added
      optionDisabled: 'data-select-option-disabled', // dynamically added
    };

    this.sections = document.querySelectorAll(`[${this.atts.wrapper}]`);
    if (!this.sections.length) return;

    this.sections.forEach((section, i) => {
      new DataSelectCore(i, this.atts);
    });
  }
}
