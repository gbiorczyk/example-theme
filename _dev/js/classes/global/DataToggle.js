class DataToggleCore
{
  constructor(wrapper, atts)
  {
    this.section = wrapper;
    this.atts    = atts;
    if (!this.setVars()) return;

    this.setEvents();
    this.setTouchEvents();
    this.updateStatus(true);
  }

  setVars()
  {
    this.buttons       = this.section.querySelectorAll(`[${this.atts.button}]`);
    this.itemsScroll   = this.section.querySelector(`[${this.atts.itemsScroll}]`);
    this.items         = this.section.querySelectorAll(`[${this.atts.item}]`);
    this.navSwipe      = this.section.querySelector(`[${this.atts.swipeArea}]`);
    this.navPrev       = this.section.querySelector(`[${this.atts.navPrev}]`);
    this.navNext       = this.section.querySelector(`[${this.atts.navNext}]`);
    this.statusCurrent = this.section.querySelector(`[${this.atts.status.current}]`);

    this.settings = {
      isLoop: (this.section.getAttribute(this.atts.settings.loop) !== null),
      navCurrent: -1,
      navMax: (this.countItems() - 1),
      scrollOffset: 70,
    };

    return true;
  }

  countItems()
  {
    const indexes    = [];
    const { length } = this.items;
    for (let i = 0; i < length; i++) {
      const currentIndex = this.items[i].getAttribute(this.atts.item);
      if (indexes.indexOf(currentIndex) > -1) continue;
      indexes.push(currentIndex);
    }
    return indexes.length;
  }

  setEvents()
  {
    const { length } = this.buttons;
    for (let i = 0; i < length; i++) {
      this.buttons[i].addEventListener('click', (e) => {
        e.preventDefault();
        const index    = this.buttons[i].getAttribute(this.atts.button);
        const isToggle = this.buttons[i].getAttribute(this.atts.buttonToggle);
        this.setItem(index, (isToggle !== null));
      });
    }

    if (this.navPrev) {
      this.navPrev.addEventListener('click', (e) => {
        e.preventDefault();
        this.setPrevItem();
      });
    }
    if (this.navNext) {
      this.navNext.addEventListener('click', (e) => {
        e.preventDefault();
        this.setNextItem();
      });
    }
  }

  setTouchEvents()
  {
    if (!this.navSwipe) return;

    let start   = 0;
    let current = 0;

    const eventMove = (e) => {
      current = e.clientX || e.touches[0].clientX;
    };
    const eventEnd = () => {
      if (current > start) this.setPrevItem(true);
      else if (current < start) this.setNextItem(true);
      window.removeEventListener('touchmove', eventMove);
      window.removeEventListener('touchend', eventEnd);
    };

    this.navSwipe.addEventListener('touchstart', (e) => {
      start   = e.touches[0].clientX;
      current = start;
      window.addEventListener('touchmove', eventMove);
      window.addEventListener('touchend', eventEnd);
    });
  }

  setItem(index, isToggle)
  {
    const current  = parseInt(index);
    const newIndex = (isToggle && (current === this.settings.navCurrent)) ? -1 : current;
    this.settings.navCurrent = newIndex;

    this.toggleClassForItems(this.buttons, newIndex, 'button', 'buttonClass');
    this.toggleClassForItems(this.items, newIndex, 'item', 'itemClass');
    this.updateStatus();
    window.triggerEvent('refreshWindowHeight');
  }

  updateStatus(isDefault = false)
  {
    this.toggleButtonsStatus();
    this.updateNavProgress();
    if (!isDefault) this.scrollToItems();
  }

  toggleButtonsStatus()
  {
    if (this.settings.isLoop) return;

    if (this.navPrev) this.navPrev.removeAttribute(this.atts.settings.buttonDisabled);
    if (this.navNext) this.navNext.removeAttribute(this.atts.settings.buttonDisabled);

    if (this.navPrev && (this.settings.navCurrent === 0)) {
      this.navPrev.setAttribute(this.atts.settings.buttonDisabled, true);
    } else if (this.navNext && (this.settings.navCurrent === this.settings.navMax)) {
      this.navNext.setAttribute(this.atts.settings.buttonDisabled, true);
    }
  }

  updateNavProgress()
  {
    if (!this.statusCurrent) return;
    this.statusCurrent.innerHTML = (this.settings.navCurrent + 1);
  }

  scrollToItems()
  {
    if (!this.itemsScroll) return;

    const position = this.itemsScroll.getBoundingClientRect();
    if (position.top <= (window.innerHeight - this.settings.scrollOffset)) return;

    const scrollAdd    = position.top - this.settings.scrollOffset;
    const windowScroll = document.body.scrollTop || document.documentElement.scrollTop;
    window.triggerEvent('setManualScroll', (windowScroll + scrollAdd));
  }

  toggleClassForItems(items, index, attrIndex, attrClass)
  {
    const { length } = items;
    for (let i = 0; i < length; i++) {
      const currentIndex = items[i].getAttribute(this.atts[attrIndex]);
      const currentClass = items[i].getAttribute(this.atts[attrClass]);
      if (currentIndex == index) items[i].addClass(currentClass); // eslint-disable-line
      else items[i].removeClass(currentClass);
    }
  }

  setPrevItem(isLoop = false)
  {
    if ((!this.settings.isLoop && !isLoop)
      && (this.settings.navCurrent === 0)) return;
    this.settings.navCurrent--;
    if (this.settings.navCurrent < 0) this.settings.navCurrent = this.settings.navMax;

    this.setItem(this.settings.navCurrent);
  }

  setNextItem(isLoop = false)
  {
    if ((!this.settings.isLoop && !isLoop)
      && (this.settings.navCurrent === this.settings.navMax)) return;
    this.settings.navCurrent++;
    if (this.settings.navCurrent > this.settings.navMax) this.settings.navCurrent = 0;

    this.setItem(this.settings.navCurrent);
  }
}

export default class DataToggle
{
  constructor()
  {
    this.atts = {
      wrapper: 'data-toggle', // add to section container, value is empty
      button: 'data-toggle-button', // optionally; add to each toggle button, value is index (from 0)
        // [data-toggle-button-disabled] attribute is automatically added if button is inactive
      buttonClass: 'data-toggle-button-class', // optionally; add to each toggle button, value is class for active button
      buttonToggle: 'data-toggle-button-toggle', // optionally; add to button if you want it to act as a toggle
      itemsScroll: 'data-toggle-scroll', // optionally; add to items wrapper for scrolling after set item, value is empty
      item: 'data-toggle-item', // add to each switchable item, value is index (from 0, matching button index)
      itemClass: 'data-toggle-item-class', // add to each switchable item, value is class for active button
      navPrev: 'data-toggle-prev', // optionally; add to prev nav button, value is empty
      navNext: 'data-toggle-next', // optionally; add to next nav button, value is empty
      swipeArea: 'data-toggle-swipe', // optionally; container where swipe-nav works
      settings: {
        loop: 'data-toggle-settings-loop', // add to section container, value is empty (turns on loop for nav)
        buttonDisabled: 'data-toggle-button-disabled',
      },
      status: {
        current: 'data-toggle-status-current', // optionally; add to container with current index number
          // innerHTML is dynamically updated
      },
    };

    this.sections = document.querySelectorAll(`[${this.atts.wrapper}]`);
    if (!this.sections.length) return;

    this.sections.forEach((section) => {
      new DataToggleCore(section, this.atts);
    });
  }
}
