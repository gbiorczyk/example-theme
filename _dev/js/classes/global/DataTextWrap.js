export default class DataTextWrap
{
  constructor()
  {
    if (!this.setVars()) return;

    this.setEvents();
    this.refreshItems();
  }

  setVars()
  {
    this.items = [];

    this.atts = {
      item: 'data-text-wrap', // add to DOM element
    };

    return true;
  }

  setEvents()
  {
    window.addEventListener('load', this.refreshItems.bind(this));
    window.addEventListener('resize', this.refreshItems.bind(this));
    window.addEvent('refreshTextWrap', this.refreshItems.bind(this));
  }

  refreshItems()
  {
    const items      = document.querySelectorAll(`[${this.atts.item}]`);
    const { length } = items;
    for (let i = 0; i < length; i++) {
      DataTextWrap.refreshItem(items[i]);
    }
  }

  static refreshItem(element)
  {
    let title = DataTextWrap.getItemTitle(element);
    element.innerHTML = title;

    const height = element.offsetHeight;
    DataTextWrap.setStylesForItem(element);
    title = DataTextWrap.setTitleForItem(element, title, height);

    if (element.getAttribute('title') === title) element.removeAttribute('title');
    DataTextWrap.setStylesForItem(element, false);
  }

  static getItemTitle(element)
  {
    let title = element.getAttribute('title') ? element.getAttribute('title') : element.innerHTML;
    title     = title.replace(/<[^>]+>[^<]{0,}<\/[^>]+>/ig, '').replace(/<[^>]+>/ig, '');
    element.setAttribute('title', title);
    return title;
  }

  static setStylesForItem(element, isClear = true)
  {
    if (!isClear) {
      element.style.position  = '';
      element.style.width     = '';
      element.style.height    = '';
      element.style.maxHeight = '';
    } else {
      element.style.position  = 'absolute';
      element.style.width     = `${element.offsetWidth}px`;
      element.style.height    = 'auto';
      element.style.maxHeight = 'inherit';
    }
  }

  static setTitleForItem(element, defaultTitle, maxHeight)
  {
    const words      = defaultTitle.split(' ');
    const { length } = words;
    for (let i = (length - 1); i >= 0; i--) {
      let value = words.slice(0, i).join(' ');
      if (i > 0) value += '...';
      element.innerHTML = value;
      if (element.offsetHeight <= maxHeight) return value;
    }
    return '';
  }
}
