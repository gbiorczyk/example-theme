export default class DataAdjustHeight
{
  constructor()
  {
    if (!this.setVars()) return;

    this.getGroupsByPosition();
    this.setEvents();
  }

  setVars()
  {
    this.items = document.querySelectorAll('[data-adjust-height]');
    if (!this.items.length) return;

    return true;
  }

  setEvents()
  {
    window.addEventListener('load', this.getGroupsByPosition.bind(this));
    window.addEventListener('resize', this.getGroupsByPosition.bind(this));
  }

  getGroupsByPosition()
  {
    const groups     = [];
    const { length } = this.items;
    for (let i = 0; i < length; i++) {
      const position = this.items[i].getBoundingClientRect();

      if (groups[position.top] === undefined) groups[position.top] = [];
      groups[position.top].push(this.items[i]);
    }

    const list = Object.entries(groups);
    DataAdjustHeight.adjustGroups(list);
  }

  static adjustGroups(list)
  {
    const { length } = list;
    for (let i = 0; i < length; i++) {
      DataAdjustHeight.adjustItems(list[i][1]);
    }
  }

  static adjustItems(items)
  {
    const height     = DataAdjustHeight.getMaxHeight(items);
    const { length } = items;
    if (length === 1) return;

    for (let i = 0; i < length; i++) {
      items[i].style.height = `${height}px`;
    }
  }

  static getMaxHeight(items)
  {
    let value        = 0;
    const { length } = items;
    for (let i = 0; i < length; i++) {
      items[i].style.height = '';
      if (items[i].offsetHeight > value) value = items[i].offsetHeight;
    }
    return value;
  }
}
