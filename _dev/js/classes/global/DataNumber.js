import { TweenLite } from 'gsap/TweenLite';

export default class DataNumber
{
  constructor()
  {
    if (!this.setVars()) return;

    this.setEvents();
  }

  setVars()
  {
    this.atts = {
      group: 'data-number-group', // add to number's group container, value is empty
      groupThousands: 'data-number-group-thousands', // add to numbnumber's group container
        // value is separator between every group of thousands
      groupDecimals: 'data-number-group-decimals', // add to number's group container
        // value is separator for the decimal point
      item: 'data-number', // add to number container, value is target number
        // innerHTML is dynamically updated
      itemPattern: 'data-number-pattern', // add to number container
       // value is pattern for displaying value (e.q. %s% - %s is replaced to numeric value)
    };

    this.groups = document.querySelectorAll(`[${this.atts.group}]`);
    if (!this.groups.length) return;

    this.queues   = {};
    this.timeouts = {};
    this.settings = {
      time: {
        throttle: 250,
        animation: 3000,
        step: 500,
      },
      seps: {
        thousands: ' ',
        decimals: '.',
      },
    };

    return true;
  }

  setEvents()
  {
    const scrollEvent = () => {
      this.throttleScroll(scrollEvent);
    };
    window.addEventListener('scroll', scrollEvent);
    window.addEventListener('load', this.runGroups.bind(this));
  }

  throttleScroll(scrollEvent)
  {
    window.removeEventListener('scroll', scrollEvent);
    this.runGroups();
    setTimeout(() => {
      window.addEventListener('scroll', scrollEvent);
    }, this.settings.time.throttle);
  }

  runGroups()
  {
    const { length } = this.groups;
    for (let i = 0; i < length; i++) {
      this.runGroup(i);
    }
  }

  runGroup(index)
  {
    if ((this.queues[index] !== undefined)
      || !this.detectIfGroupIsVisible(index)) return;

    const items      = this.groups[index].querySelectorAll(`[${this.atts.item}]`);
    const { length } = items;
    for (let i = 0; i < length; i++) {
      const sepThousands = this.groups[index].getAttribute(this.atts.groupThousands);
      const sepDecimals  = this.groups[index].getAttribute(this.atts.groupDecimals);
      this.addItemToQueue(items[i], index, sepThousands, sepDecimals);
    }
  }

  detectIfGroupIsVisible(index)
  {
    const position = this.groups[index].getBoundingClientRect();
    return (position.top < window.innerHeight);
  }

  addItemToQueue(item, queueKey, sepThousands, sepDecimals)
  {
    this.addNewQueue(queueKey);
    this.queues[queueKey].push(item);
    if (this.timeouts[queueKey] === false) this.runAnimations(queueKey, sepThousands, sepDecimals);
  }

  addNewQueue(key)
  {
    if (this.queues[key] !== undefined) return;
    this.queues[key]   = [];
    this.timeouts[key] = false;
  }

  runAnimations(queueKey, sepThousands, sepDecimals)
  {
    const animation = this.getFirstAnimation(queueKey);
    if (animation === null) return;
    this.animateNumber(animation, sepThousands, sepDecimals);

    this.timeouts[queueKey] = setTimeout(() => {
      this.timeouts[queueKey] = false;
      this.runAnimations(queueKey, sepThousands, sepDecimals);
    }, this.settings.time.step);
  }

  getFirstAnimation(queueKey)
  {
    const { length } = this.queues[queueKey];
    if (length === 0) return null;

    const value = this.queues[queueKey][0];
    this.queues[queueKey].splice(0, 1);
    return value;
  }

  animateNumber(item, sepThousands, sepDecimals)
  {
    const value    = item.getAttribute(this.atts.item);
    const decimal  = (value.split('.')[1] === undefined) ? 0 : (value.split('.').length - 1);
    const pattern  = item.getAttribute(this.atts.itemPattern);
    const count    = { val: 0 };
    const duration = this.settings.time.animation / 1000; // eslint-disable-line

    TweenLite.to(count, duration, {
      ease: Power1.easeOut,
      val: (decimal === 0) ? value : (value * (10 ** decimal)), // eslint-disable-line
      roundProps: 'val',
      onUpdate: () => {
        item.innerHTML = this.parseOutput(count.val, decimal, pattern, sepThousands, sepDecimals);
      },
    });
  }

  parseOutput(value, decimal, pattern, sepThousands, sepDecimals)
  {
    let number = Math.round(value);
    if (decimal > 0) number /= (10 ** decimal); // eslint-disable-line

    let output = pattern.replace('%s', number.toFixed(decimal));
    output     = output.replace(/\./g, sepDecimals || this.settings.seps.decimals);
    output     = output.replace(/(\d)(?=(\d{3})+$)/g, `$1${sepThousands || this.settings.seps.thousands}`); // eslint-disable-line
    return output;
  }
}
