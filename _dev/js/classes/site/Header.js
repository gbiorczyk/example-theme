export default class Header
{
  constructor()
  {
    if (!this.setVars()) return;

    this.setEvents();
  }

  setVars()
  {
    this.section = document.querySelector('.header');
    if (!this.section) return;

    this.searchToggle = this.section.querySelector('.header__buttonLink--search');
    this.searchForm   = this.section.querySelector('.header__search');
    this.searchInput  = this.searchForm.querySelector('.header__searchInput');

    this.classes = {
      searchFormActive: 'header__search--active',
    };
    this.config = {
      eventClose: () => {
        this.searchForm.removeClass(this.classes.searchFormActive);
        window.removeEventListener('click', this.searchForm);
      },
    };

    return true;
  }

  setEvents()
  {
    this.searchToggle.addEventListener('click', (e) => {
      e.preventDefault();
      e.stopPropagation();
      this.openSearch();
    });
    this.searchForm.addEventListener('click', (e) => {
      e.stopPropagation();
    });
    window.addEvent('mobileMenuStatus', (status) => {
      if (status) this.config.eventClose();
    });
  }

  openSearch()
  {
    this.searchForm.addClass(this.classes.searchFormActive);
    setTimeout(() => { this.searchInput.focus(); }, 100);

    setTimeout(() => { window.addEventListener('click', this.config.eventClose); }, 0);
    window.triggerEvent('setMobileMenuStatus', false);
  }
}
