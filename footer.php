  <?php get_template_part('includes/layout/footer'); ?>

  <div class="customScroll__bar" data-scroll-bar>
    <button class="customScroll__barButton" data-scroll-bar-button></button>
  </div>

  <div class="dataSelect">
    <div class="dataSelect__selected"></div>
    <ul class="dataSelect__options">
      <li class="dataSelect__option"></li>
    </ul>
  </div>

  <?php wp_footer(); ?>
</body>
</html>