<section class="contentBanner section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="contentBanner__inner">
    <div class="container">
      <div class="contentBanner__wrapper <?= ($section['has_bg']) ? 'contentBanner__wrapper--bg' : ''; ?>">
        <div class="contentBanner__content wysiwyg"
          data-hide-scroll="left"><?= $section['content']; ?></div>
        <?php if ($section['button']) : ?>
          <div class="contentBanner__button"
            data-hide-scroll="right">
            <a href="<?= $section['button']['url']; ?>" target="<?= $section['button']['target']; ?>"
              class="button button--border button--red"><?= $section['button']['title']; ?></a>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>