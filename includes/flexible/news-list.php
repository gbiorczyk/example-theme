<section class="newsList section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="newsList__inner">
    <div class="container">
      <?php if ($section['headline']) : ?>
        <h3 class="newsList__headline section__headline"
          data-hide-scroll="left"><?= $section['headline']; ?></h3>
      <?php endif; ?>
      <?php if ($items = apply_filters('insights_assigned', [], 3, get_the_ID())) : ?>
        <ul class="newsList__items row"
          data-hide-scroll>
          <?php foreach ($items as $item) : ?>
            <div class="newsList__item col-4 col-md-12">
              <a href="<?= $item['url']; ?>" class="newsList__itemInner newsItem">
                <div class="newsItem__inner">
                  <div class="newsItem__image">
                    <div class="newsItem__imageInner"
                      style="background-image: url(<?= $item['image']; ?>);"></div>
                  </div>
                  <h5 class="newsItem__title" data-text="<?= $item['title']; ?>"><?= $item['title']; ?></h5>
                </div>
              </a>
            </div>
          <?php endforeach; ?>
        </ul>
        <div class="newsList__button" data-hide-scroll="right">
          <?php if (is_singular('technologies')) : ?>
            <a href="<?= get_post_type_archive_link('insights'); ?>?technology=<?= get_the_ID(); ?>"
              class="button button--border button--red"><?= __('Load more', 'lang'); ?></a>
          <?php elseif (is_singular('industries')) : ?>
            <a href="<?= get_post_type_archive_link('insights'); ?>?industry=<?= get_the_ID(); ?>"
              class="button button--border button--red"><?= __('Load more', 'lang'); ?></a>
          <?php else : ?>
            <a href="<?= get_post_type_archive_link('insights'); ?>"
              class="button button--border button--red"><?= __('Load more', 'lang'); ?></a>
          <?php endif; ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
</section>