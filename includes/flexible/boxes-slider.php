<section class="numberSlider section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="numberSlider__inner">
    <div class="container">
      <div class="numberSlider__wrapper row">
        <div class="col-12">
          <h3 class="numberSlider__headline section__headline"
            data-hide-scroll="left"><?= $section['headline']; ?></h3>
        </div>
        <div class="col-10 offset-col-2 col-md-12">
          <div class="numberSlider__main numberSlider__main--marginTop swiper-container"
            data-hide-scroll="right">
            <div class="numberSlider__nav">
              <div class="numberSlider__navNumber numberSlider__navNumber--current">01</div>
              <div class="numberSlider__navLine">
                <div class="numberSlider__navLineInner" style="width: 0%;"></div>
              </div>
              <div class="numberSlider__navNumber"><?= str_pad(count($section['boxes']), 2, '0', STR_PAD_LEFT); ?></div>
            </div>
            <ul class="numberSlider__items swiper-wrapper">
              <?php foreach ($section['boxes'] as $index => $item) : ?>
                <li class="numberSlider__item swiper-slide">
                  <?php if ($item['has_link']) : ?>
                    <a href="<?= $item['link']['url']; ?>" target="<?= $item['link']['target']; ?>"
                      class="numberSlider__itemLink <?= ($index === 0) ? 'numberSlider__itemLink--active' : ''; ?>">
                      <div class="numberSlider__itemImage"
                        style="background-image: url(<?= $item['image']['sizes']['image-small']; ?>);"></div>
                      <h5 class="numberSlider__itemTitle">
                        <span class="numberSlider__itemTitleLink"
                          data-text="<?= $item['link']['title']; ?>"><?= $item['link']['title']; ?></span>
                      </h5>
                    </a>
                  <?php else : ?>
                    <div class="numberSlider__itemLink <?= ($index === 0) ? 'numberSlider__itemLink--active' : ''; ?>">
                      <div class="numberSlider__itemImage"
                        style="background-image: url(<?= $item['image']['sizes']['image-small']; ?>);"></div>
                      <h5 class="numberSlider__itemTitle">
                        <span class="numberSlider__itemTitleLink"><?= $item['title']; ?></span>
                      </h5>
                    </div>
                  <?php endif; ?>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>