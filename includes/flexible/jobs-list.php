<?php if ($response = apply_filters('jobs_api_response', [])) : ?>
  <section class="jobsList section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
    data-scroll-id="<?= $section['section_id']; ?>">
    <div class="jobsList__inner">
      <div class="container">
        <h3 class="jobsList__headline section__headline"
          data-hide-scroll="left"><?= $section['headline']; ?></h3>
        <div class="dataLoader" data-hide-scroll>
          <data-loader
            :api-url="'<?= apply_filters('jobs_api_url', null); ?>'"
            :default-form="{ department: '', type: '', location: '' }"
            :default-values='{ department: "", type: "", location: "" }'
            :default-items="'<?= base64_encode(json_encode($response['data'])); ?>'"
            :default-pagination="{ page: 1, pages: <?= $response['pages']; ?> }"
            :loader-config="{ time: 1000, timeCache: 250, delay: 500, delayKeys: [], spacing: 20, scrollOffset: 70 }"
            inline-template v-cloak>
            <component>
              <ul class="dataLoader__filters dataLoader__filters row">
                <li class="dataLoader__filter col-4 col-md-12">
                  <label class="dataLoader__filterInner" data-select>
                    <div class="dataLoader__filterTitle"><?= __('Choose department', 'lang'); ?></div>
                    <div class="dataLoader__filterInput" data-select>
                      <select v-model="form.department" ref="form.department">
                        <option value="" selected><?= __('All departments', 'lang'); ?></option>
                        <?php if ($filters = get_terms('jobs-departments')) : ?>
                          <?php foreach ($filters as $filter) : ?>
                            <option value="<?= $filter->term_id; ?>"><?= $filter->name; ?></option>
                          <?php endforeach; ?>
                        <?php endif; ?>
                      </select>
                    </div>
                  </label>
                </li>
                <li class="dataLoader__filter col-4 col-md-12">
                  <label class="dataLoader__filterInner" data-select>
                    <div class="dataLoader__filterTitle"><?= __('Choose job type', 'lang'); ?></div>
                    <div class="dataLoader__filterInput" data-select>
                      <select v-model="form.type" ref="form.type">
                        <option value="" selected><?= __('All jobs', 'lang'); ?></option>
                        <?php if ($filters = get_terms('jobs-types')) : ?>
                          <?php foreach ($filters as $filter) : ?>
                            <option value="<?= $filter->term_id; ?>"><?= $filter->name; ?></option>
                          <?php endforeach; ?>
                        <?php endif; ?>
                      </select>
                    </div>
                  </label>
                </li>
                <li class="dataLoader__filter col-4 col-md-12">
                  <label class="dataLoader__filterInner" data-select>
                    <div class="dataLoader__filterTitle"><?= __('Choose job location', 'lang'); ?></div>
                    <div class="dataLoader__filterInput" data-select>
                      <select v-model="form.location" ref="form.location">
                        <option value="" selected><?= __('All locations', 'lang'); ?></option>
                        <?php if ($filters = get_terms('jobs-locations')) : ?>
                          <?php foreach ($filters as $filter) : ?>
                            <option value="<?= $filter->term_id; ?>"><?= $filter->name; ?></option>
                          <?php endforeach; ?>
                        <?php endif; ?>
                      </select>
                    </div>
                  </label>
                </li>
              </ul>
              <div class="jobsList__wrapper">
                <div class="jobsList__wrapperOutput dataLoader__wrapper" ref="wrapper"
                  v-bind:class="{ 'dataLoader__wrapper--loading': status.loading }">
                  <div class="dataLoader__wrapperOutput">
                    <!-- BEGIN Error message -->
                    <div class="dataLoader__wrapperMessage" v-if="status.empty && !status.loading">
                      <div class="dataLoader__wrapperMessageInner">
                        <div class="dataLoader__wrapperMessageText"><?= __('Sorry, we did not find what you are looking for.', 'lang'); ?></div>
                        <div class="dataLoader__wrapperMessageButton">
                          <button type="button" class="button button--border button--red"
                            v-on:click="resetFilters"><?= __('Reset filters', 'lang'); ?></button>
                        </div>
                      </div>
                    </div>
                    <!-- END Error message -->

                    <!-- BEGIN Items list -->
                    <ul class="dataLoader__wrapperItems jobsList__items row row--flex">
                      <li v-for="item in items" class="jobsList__item col-3 col-mdl-6 col-md-12">
                        <a :href="item.url" target="_blank" class="jobsList__itemLink">
                          <div class="jobsList__itemInner">
                            <div class="jobsList__itemImage">
                              <div class="jobsList__itemImageInner"
                                :style="'background-image: url(' + item.image + ');'"></div>
                            </div>
                            <div class="jobsList__itemContent">
                              <div v-for="value in item.departments"
                                class="jobsList__itemCategory">{{ value.title }}</div>
                              <h5 class="jobsList__itemTitle" :data-text="item.title">{{ item.title }}</h5>
                              <ul class="jobsList__itemValues">
                                <li v-for="value in item.experience"
                                  class="jobsList__itemValue icon-layers-2">{{ value.title }}</li>
                                <li v-for="value in item.types"
                                  class="jobsList__itemValue icon-clock">{{ value.title }}</li>
                                <li v-for="value in item.locations"
                                  class="jobsList__itemValue icon-location">{{ value.title }} </li>
                              </ul>
                              <div class="jobsList__itemButton"></div>
                            </div>
                          </div>
                        </a>
                      </li>
                      <?php foreach ($response['data'] as $item) : ?>
                        <li v-if="!status.updated" class="jobsList__item col-3 col-mdl-6 col-md-12">
                          <a href="<?= $item['url']; ?>" target="_blank" class="jobsList__itemLink">
                            <div class="jobsList__itemInner">
                              <div class="jobsList__itemImage">
                                <div class="jobsList__itemImageInner"
                                  style="background-image: url(<?= $item['image']; ?>);"></div>
                              </div>
                              <div class="jobsList__itemContent">
                                <?php foreach ($item['departments'] as $value) : ?>
                                  <div class="jobsList__itemCategory"><?= $value['title']; ?></div>
                                <?php endforeach; ?>
                                <h5 class="jobsList__itemTitle" data-text="<?= $item['title']; ?>"><?= $item['title']; ?></h5>
                                <ul class="jobsList__itemValues">
                                  <?php foreach ($item['experience'] as $value) : ?>
                                    <li class="jobsList__itemValue icon-layers-2"><?= $value['title']; ?></li>
                                  <?php endforeach; ?>
                                  <?php foreach ($item['types'] as $value) : ?>
                                    <li class="jobsList__itemValue icon-clock"><?= $value['title']; ?></li>
                                  <?php endforeach; ?>
                                  <?php foreach ($item['locations'] as $value) : ?>
                                    <li class="jobsList__itemValue icon-location"><?= $value['title']; ?></li>
                                  <?php endforeach; ?>
                                </ul>
                                <div class="jobsList__itemButton"></div>
                              </div>
                            </div>
                          </a>
                        </li>
                      <?php endforeach; ?>
                    </ul>
                    <!-- END Items list -->

                    <div class="dataLoader__wrapperLoader">
                      <div class="cssLoader" ref="loader"></div>
                    </div>
                  </div>
                </div>

                <!-- BEGIN Pagination next -->
                <div class="dataLoader__pagination" v-if="form.page < pagination.pages">
                  <button type="button" class="button button--border button--red"
                    v-on:click="loadMore"><?= __('Load more', 'lang'); ?></button>
                </div>
                <!-- END Pagination next -->
              </div>
            </component>
          </data-loader>
        </div>
      </div>
    </div>
  </section>
<?php endif;