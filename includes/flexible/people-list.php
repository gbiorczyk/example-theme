<section class="peopleList section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="peopleList__inner">
    <div class="container">
      <?php if ($section['content']) : ?>
        <div class="peopleList__content">
          <div class="peopleList__contentInner wysiwyg"
            data-hide-scroll="left">
            <?= preg_replace('/<(\/)?(h2|h3)>/', '<$1h1>', $section['content']); ?>
          </div>
        </div>
      <?php endif; ?>
      <div class="peopleList__wrapper">
        <?php foreach (array_slice($section['people'], 0, 1) as $item) : ?>
          <div class="peopleList__featured">
            <div class="personBox personBox--large" data-hide-scroll>
              <div class="personBox__image">
                <div class="personBox__imageOuter">
                  <?php if ($item['image']) : ?>
                    <div class="personBox__imageInner"
                      style="background-image: url(<?= $item['image']['sizes']['image-medium']; ?>);"></div>
                  <?php endif; ?>
                </div>
              </div>
              <div class="personBox__content">
                <div class="personBox__contentInner">
                  <h4 class="personBox__contentTitle"><?= $item['name']; ?></h4>
                  <div class="personBox__contentDesc"><?= $item['position']; ?></div>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
      <div class="peopleList__archive">
        <div class="row">
          <div class="col-6 offset-col-5 col-mdl-9 offset-col-mdl-3 col-md-12">
            <ul class="peopleList__items">
              <?php foreach (array_slice($section['people'], 1) as $index => $item) : ?>
                <li class="peopleList__item <?= ($index >= ($section['items_count'] - 1)) ? 'peopleList__item--hidden' : ''; ?>">
                  <div class="personBox" data-hide-scroll>
                    <div class="personBox__image">
                      <div class="personBox__imageOuter">
                        <?php if ($item['image']) : ?>
                          <div class="personBox__imageInner"
                            style="background-image: url(<?= $item['image']['sizes']['image-medium']; ?>);"></div>
                        <?php endif; ?>
                      </div>
                    </div>
                    <div class="personBox__content">
                      <div class="personBox__contentInner">
                        <h4 class="personBox__contentTitle"><?= $item['name']; ?></h4>
                        <div class="personBox__contentDesc"><?= $item['position']; ?></div>
                      </div>
                    </div>
                  </div>
                </li>
              <?php endforeach; ?>
            </ul>
            <?php if (count($section['people']) > $section['items_count']) : ?>
              <div class="peopleList__more" data-hide-scroll="right">
                <button type="button" class="peopleList__moreButton button button--border button--red"><?= __('Load more', 'lang'); ?></button>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>