<section class="linkBoxes section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="linkBoxes__inner">
    <div class="container">
      <h3 class="linkBoxes__headline section__headline"
        data-hide-scroll="left"><?= $section['headline']; ?></h3>
      <ul class="linkBoxes__items row">
        <?php foreach ($section['boxes'] as $item) : ?>
          <li class="linkBoxes__item col-6 col-md-12">
            <a href="<?= $item['link']['url']; ?>" target="<?= $item['link']['target']; ?>"
              class="linkBoxes__itemLink" data-hide-scroll>
              <div class="linkBoxes__itemIcon icon-<?= $item['icon']; ?>"></div>
              <h5 class="linkBoxes__itemTitle"><?= $item['link']['title']; ?></h5>
            </a>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
</section>