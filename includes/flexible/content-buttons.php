<section class="contentButtons section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="contentButtons__inner">
    <div class="container">
      <div class="row">
        <div class="col-6 col-mdl-12">
          <div class="contentButtons__content" data-hide-scroll="left">
            <div class="contentButtons__contentInner wysiwyg">
              <?= preg_replace('/<h(?:[0-9])>(.*)<\/h(?:[0-9])>/', '<h3>${1}</h3>', $section['content']); ?>
            </div>
          </div>
        </div>
        <div class="col-6 col-mdl-12">
          <div class="contentButtons__sidebar" data-hide-scroll="right">
            <div class="contentButtons__sidebarOuter">
              <div class="contentButtons__sidebarInner">
                <div class="contentButtons__sidebarText"><?= $section['buttons_title']; ?></div>
                <ul class="contentButtons__sidebarButtons">
                  <?php foreach ($section['buttons'] as $button) : ?>
                    <li class="contentButtons__sidebarButton">
                      <a href="<?= $button['link']['url']; ?>" target="<?= $button['link']['target']; ?>"
                        class="contentButtons__sidebarButtonLink button button--border button--red"><?= $button['link']['title']; ?></a>
                    </li>
                  <?php endforeach; ?>
                </ul>
              </div>
              <img src="<?= $section['image']['sizes']['image-small']; ?>" alt="<?= $section['image']['alt']; ?>"
                class="contentButtons__sidebarImage">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>