<section class="contentColumns section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="contentColumns__inner">
    <div class="container">
      <h3 class="contentColumns__headline section__headline"
        data-hide-scroll="left"><?= $section['headline']; ?></h3>
      <ul class="contentColumns__columns row" data-hide-scroll>
        <?php foreach ($section['columns'] as $column) : ?>
          <li class="contentColumns__column col-3 col-md-6 col-sml-12">
            <h5 class="contentColumns__columnTitle" data-adjust-height><?= $column['title']; ?></h5>
            <div class="contentColumns__columnContent <?= ($section['is_large_content']) ? 'contentColumns__columnContent--large' : ''; ?>">
              <?=
                ($section['is_large_content'])
                  ? $column['content']
                  : preg_replace('/<a(.*)>(.*)<\/a>/', '<a${1} data-text="${2}">${2}</a>', $column['content']);
              ?>
            </div>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
</section>