<section class="quoteSection section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="quoteSection__inner">
    <div class="container">
      <?php if ($section['has_author']) : ?>
        <div class="row">
          <div class="col-5 col-mdl-12">
            <div class="quoteSection__wrapper quoteSection__wrapper--side">
              <div class="quoteSection__wrapperOuter">
                <div class="quoteSection__wrapperInner">
                  <div class="quoteSection__wrapperContent"
                    data-hide-scroll><?= $section['content']; ?></div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-7 col-mdl-12">
            <div class="quoteSection__author">
              <div class="personBox personBox--large" data-hide-scroll="right">
                <div class="personBox__image">
                  <div class="personBox__imageOuter">
                    <?php if ($section['author']['image']) : ?>
                      <div class="personBox__imageInner"
                        style="background-image: url(<?= $section['author']['image']['sizes']['image-medium']; ?>);"></div>
                    <?php endif; ?>
                  </div>
                </div>
                <div class="personBox__content">
                  <div class="personBox__contentInner">
                    <h4 class="personBox__contentTitle"><?= $section['author']['name']; ?></h4>
                    <div class="personBox__contentDesc"><?= $section['author']['position']; ?></div>
                    <?php if ($section['author']['button']) : ?>
                      <div class="personBox__contentButton">
                        <a href="<?= $section['author']['button']['url']; ?>"
                          target="<?= $section['author']['button']['target']; ?>"
                          class="button button--border button--red"><?= $section['author']['button']['title']; ?></a>
                      </div>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php else : ?>
        <div class="row">
          <div class="col-10 offset-col-1 col-mdl-12">
            <div class="quoteSection__wrapper">
              <div class="quoteSection__wrapperOuter">
                <div class="quoteSection__wrapperInner">
                  <div class="quoteSection__wrapperContent"
                    data-hide-scroll><?= $section['content']; ?></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
    </div>
  </div>
</section>