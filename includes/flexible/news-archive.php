<?php
  $args = [
    'page'       => get_query_var('paged') ?: 1,
    'industry'   => $_GET['industry'] ?? '',
    'technology' => $_GET['technology'] ?? '',
  ];
  if ($response = apply_filters('insights_api_response', [], $args)) :
?>
  <section class="newsArchive section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
    data-scroll-id="<?= $section['section_id']; ?>">
    <div class="newsArchive__inner">
      <div class="container" data-sticky>
        <div class="newsArchive__columns row">
          <div class="newsArchive__column newsArchive__column--content col-8 col-mdl-12">
            <div class="dataLoader">
              <data-loader
                :api-url="'<?= apply_filters('insights_api_url', null); ?>'"
                :default-form="{ industry: '', technology: '' }"
                :default-values='{ industry: "<?= $args['industry']; ?>", technology: "<?= $args['technology']; ?>" }'
                :default-items="'<?= base64_encode(json_encode($response['data'])); ?>'"
                :default-pagination="{ page: <?= $args['page']; ?>, pages: <?= $response['pages']; ?> }"
                :loader-config="{ time: 1000, timeCache: 250, delay: 500, delayKeys: [], spacing: 20, scrollOffset: 70 }"
                :url-pattern="'<?= get_post_type_archive_link('insights'); ?>!page|page/#!/?!industry|industry=#!&!technology|technology=#!'"
                inline-template v-cloak>
                <component>
                  <ul class="dataLoader__filters row">
                    <li class="dataLoader__filter col-6 col-sml-12">
                      <h5 class="dataLoader__filterTitle"><?= __('Industry', 'lang'); ?></h5>
                      <div class="dataLoader__filterInput" data-select>
                        <?php if ($options = apply_filters('industries_posts', [], ['orderby' => 'title', 'order' => 'asc'])) : ?>
                          <select v-model="form.industry" ref="form.industry">
                            <option value=""><?= __('All industries', 'lang'); ?></option>
                            <?php foreach ($options as $option) : ?>
                              <option value="<?= $option['id']; ?>" <?= ($args['industry'] == $option['id']) ? 'selected' : ''; ?>>
                                <?= $option['title']; ?>
                              </option>
                            <?php endforeach; ?>
                          </select>
                        <?php endif; ?>
                      </div>
                    </li>
                    <li class="dataLoader__filter col-6 col-sml-12">
                      <h5 class="dataLoader__filterTitle"><?= __('Technologies', 'lang'); ?></h5>
                      <div class="dataLoader__filterInput" data-select>
                        <?php if ($options = apply_filters('technologies_posts', [], ['orderby' => 'title', 'order' => 'asc'])) : ?>
                          <select v-model="form.technology" ref="form.technology">
                            <option value=""><?= __('All technologies', 'lang'); ?></option>
                            <?php foreach ($options as $option) : ?>
                              <option value="<?= $option['id']; ?>" <?= ($args['technology'] == $option['id']) ? 'selected' : ''; ?>>
                                <?= $option['title']; ?>
                              </option>
                            <?php endforeach; ?>
                          </select>
                        <?php endif; ?>
                      </div>
                    </li>
                  </ul>
                  <div class="newsArchive__wrapper">
                    <!-- BEGIN Pagination prev -->
                    <div class="dataLoader__pagination" v-if="pagination.min > 1">
                      <button type="button" class="button button--border button--red"
                        v-on:click="loadPrev"><?= __('Load prev', 'lang'); ?></button>
                    </div>
                    <!-- END Pagination prev -->

                    <div class="newsArchive__wrapperOutput dataLoader__wrapper" ref="wrapper"
                      v-bind:class="{ 'dataLoader__wrapper--loading': status.loading }">
                      <div class="dataLoader__wrapperOutput">
                        <!-- BEGIN Error message -->
                        <div class="dataLoader__wrapperMessage" v-if="status.empty && !status.loading">
                          <div class="dataLoader__wrapperMessageInner">
                            <div class="dataLoader__wrapperMessageText"><?= __('Sorry, we did not find what you are looking for.', 'lang'); ?></div>
                            <div class="dataLoader__wrapperMessageButton">
                              <button type="button" class="button button--border button--red"
                                v-on:click="resetFilters"><?= __('Reset filters', 'lang'); ?></button>
                            </div>
                          </div>
                        </div>
                        <!-- END Error message -->

                        <!-- BEGIN Items list -->
                        <ul class="dataLoader__wrapperItems newsArchive__items row">
                          <li v-for="item in items" class="newsArchive__item col-6 col-sml-12">
                            <a :href="item.url" class="newsArchive__itemInner newsItem">
                              <div class="newsItem__inner">
                                <div class="newsItem__image">
                                  <div class="newsItem__imageInner"
                                    :style="'background-image: url(' + item.image + ');'"></div>
                                </div>
                                <h5 class="newsItem__title" :data-text="item.title">{{ item.title }}</h5>
                              </div>
                            </a>
                          </li>
                          <?php foreach ($response['data'] as $item) : ?>
                            <li v-if="!status.updated" class="newsArchive__item col-6 col-sml-12">
                              <a href="<?= $item['url']; ?>" class="newsArchive__itemInner newsItem">
                                <div class="newsItem__inner">
                                  <div class="newsItem__image">
                                    <div class="newsItem__imageInner"
                                      style="background-image: url(<?= $item['image']; ?>);"></div>
                                  </div>
                                  <h5 class="newsItem__title" data-text="<?= $item['title']; ?>"><?= $item['title']; ?></h5>
                                </div>
                              </a>
                            </li>
                          <?php endforeach; ?>
                        </ul>
                        <!-- END Items list -->

                        <div class="dataLoader__wrapperLoader">
                          <div class="cssLoader" ref="loader"></div>
                        </div>
                      </div>
                    </div>

                    <!-- BEGIN Pagination next -->
                    <div class="dataLoader__pagination" v-if="form.page < pagination.pages">
                      <button type="button" class="button button--border button--red"
                        v-on:click="loadMore"><?= __('Load more', 'lang'); ?></button>
                    </div>
                    <!-- END Pagination next -->
                  </div>
                </component>
              </data-loader>
            </div>
          </div>
          <div class="newsArchive__column newsArchive__column--side col-3 offset-col-1 col-mdl-12">
            <?php if ($popular = apply_filters('insights_popular', [], 3)) : ?>
              <div class="newsArchive__side" data-sticky-sidebar>
                <h4 class="newsArchive__sideTitle"><?= __('Most popular', 'lang'); ?></h4>
                <ul class="newsArchive__sideItems row">
                  <?php foreach ($popular as $item) : ?>
                    <li class="newsArchive__sideItem col-12 col-mdl-4 col-md-6 col-sml-12">
                      <a href="<?= $item['url']; ?>" class="newsArchive__sideItemInner newsItem">
                        <div class="newsItem__inner">
                          <div class="newsItem__image">
                            <div class="newsItem__imageInner"
                              style="background-image: url(<?= $item['image']; ?>);"></div>
                          </div>
                          <h5 class="newsItem__title newsItem__title--small"
                            data-text="<?= $item['title']; ?>"><?= $item['title']; ?></h5>
                        </div>
                      </a>
                    </li>
                  <?php endforeach; ?>
                </ul>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif;