<section class="tabsStats section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="tabsStats__inner">
    <cats-tabs
      :default-tab="0"
      :default-category="<?= $section['tabs'][0]['technologies'][0]['technology']; ?>"
      inline-template>
      <div class="container">
        <div class="row">
          <div class="col-10 offset-col-1 col-mdl-12">
            <h3 class="tabsStats__headline section__headline"
              data-hide-scroll="left"><?= $section['headline']; ?></h3>
            <div class="tabsStats__buttonsWrapper"
              data-hide-scroll="right">
              <ul class="tabsStats__buttons" ref="tabs">
                <?php foreach ($section['tabs'] as $index => $tab) : ?>
                  <li class="tabsStats__button">
                    <a href="#" class="tabsStats__buttonLink" ref="tab"
                      v-bind:class="{ 'tabsStats__buttonLink--active': (status.tab == <?= $index; ?>) }"
                      v-on:click="setTab($event, <?= $index; ?>, <?= $tab['technologies'][0]['technology']; ?>)">
                      <div class="tabsStats__buttonTextWrapper">
                        <span class="tabsStats__buttonText">
                          <strong><?= str_pad(($index + 1), 2, '0', STR_PAD_LEFT); ?></strong> 
                          <?= $tab['title']; ?>
                        </span>
                        <span class="tabsStats__buttonText">
                          <strong><?= str_pad(($index + 1), 2, '0', STR_PAD_LEFT); ?></strong> 
                          <?= $tab['title']; ?>
                        </span>
                      </div>
                    </a>
                  </li>
                <?php endforeach; ?>
              </ul>
            </div>
            <div class="tabsStats__slider sliderArrows">
              <div class="tabsStats__sliderInner sliderArrows__slider swiper-container"
                ref="slider">
                <ul class="tabsStats__buttons swiper-wrapper">
                  <?php foreach ($section['tabs'] as $index => $tab) : ?>
                    <li class="tabsStats__button swiper-slide">
                      <div class="tabsStats__buttonLink">
                        <div class="tabsStats__buttonTextWrapper">
                          <span class="tabsStats__buttonText">
                            <strong><?= str_pad(($index + 1), 2, '0', STR_PAD_LEFT); ?></strong> 
                            <?= $tab['title']; ?>
                          </span>
                          <span class="tabsStats__buttonText">
                            <strong><?= str_pad(($index + 1), 2, '0', STR_PAD_LEFT); ?></strong> 
                            <?= $tab['title']; ?>
                          </span>
                        </div>
                      </div>
                    </li>
                  <?php endforeach; ?>
                </ul>
              </div>
              <button class="sliderArrows__nav sliderArrows__nav--prev"
                ref="sliderPrev"></button>
              <button class="sliderArrows__nav sliderArrows__nav--next"
                ref="sliderNext"></button>
              <div class="tabsStats__sliderProgress sliderArrows__progress"
                ref="sliderProgress"></div>
            </div>
          </div>
        </div>
        <div class="tabsStats__wrapper"> 
          <div class="row">
            <div class="col-10 offset-col-1 col-mdl-12">
              <div class="tabsStats__wrapperInner">
                <div class="tabsStats__boxesWrapper"
                  data-hide-scroll="left">
                  <?php if ($items = apply_filters('technologies_tabs', [], $section['tabs'])) : ?>
                    <ul class="tabsStats__boxes boxesList">
                      <?php foreach ($items as $index => $item) : ?>
                        <li class="boxesList__item"
                          v-bind:class="{ 'boxesList__item--disabled': (<?= json_encode($item['_tab_ids']); ?>.indexOf(status.tab) === -1) }"
                          v-on:click="setCategory($event, <?= $item['id']; ?>)">
                          <a href="#" class="boxesList__itemLink boxesList__itemLink--<?= $item['color']; ?>"
                            v-bind:class="{ 'boxesList__itemLink--active': (status.category == <?= $item['id']; ?>) }">
                            <div class="boxesList__itemIcon icon-<?= $item['icon']; ?>"></div>
                            <div class="boxesList__itemInner">
                              <h4 class="boxesList__itemTitle"><?= $item['title']; ?></h4>
                            </div>
                          </a>
                        </li>
                      <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
                </div>
                <ul class="tabsStats__items" ref="items" data-hide-scroll>
                  <?php foreach ($section['tabs'] as $tabId => $tab) : ?>
                    <?php foreach ($tab['technologies'] as $index => $technology) : ?>
                      <li class="tabsStats__item"
                        v-bind:class="{ 'tabsStats__item--active':  ((<?= $tabId; ?> == status.tab) && (<?= $technology['technology']; ?> == status.category)) }">
                        <div class="tabsStats__itemContent"><?= $technology['content']; ?></div>
                        <div class="tabsStats__itemButton">
                          <a href="<?= $technology['button']['url']; ?>"
                            target="<?= $technology['button']['target']; ?>"
                            class="button button--fill button--red"><?= $technology['button']['title']; ?></a>
                        </div>
                      </li>
                    <?php endforeach; ?>
                  <?php endforeach; ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </cats-tabs>
    <div class="container">
      <div class="tabsStats__footer">
        <div class="row">
          <div class="col-10 offset-col-1 col-mdl-12">
            <ul class="tabsStats__stats"
              data-number-group
              data-hide-scroll>
              <?php foreach ($section['stats'] as $index => $stat) : ?>
                <li class="tabsStats__stat">
                  <div class="tabsStats__statInner" data-toggle>
                    <div class="tabsStats__statTitle icon-<?= $stat['icon']; ?>">
                      <?= preg_replace_callback('/<strong>(.*)<\/strong>/', function($text) use ($stat) {
                        ob_start();
                        ?>
                          <a href="#" class="tabsStats__statToggle">
                            <?= $text[1]; ?>
                            <ul class="tabsStats__statOptions">
                              <?php foreach ($stat['numbers'] as $index => $number) : ?>
                                <li class="tabsStats__statOption">
                                  <div class="tabsStats__statOptionLink <?= ($index === 0) ? 'tabsStats__statOptionLink--active' : ''; ?>"
                                    data-text="<?= $number['name']; ?>"
                                    data-toggle-button="<?= $index; ?>"
                                    data-toggle-button-class="tabsStats__statOptionLink--active"><?= $number['name']; ?></div>
                                </li>
                              <?php endforeach; ?>
                            </ul>
                          </a>
                          <?php
                        $output = ob_get_contents();
                        ob_end_clean();
                        return $output;
                      }, strip_tags($stat['title'], '<strong>')); ?>
                    </div>
                    <ul class="tabsStats__statNumbers">
                      <?php foreach ($stat['numbers'] as $index => $number) : ?>
                        <?php if ($index === 0) : ?>
                          <li class="tabsStats__statNumber tabsStats__statNumber--active"
                            data-number="<?= $number['value']; ?>" data-number-pattern="%s"
                            data-toggle-item="0"
                            data-toggle-item-class="tabsStats__statNumber--active">0</li>
                        <?php else : ?>
                          <li class="tabsStats__statNumber"
                            data-toggle-item="<?= $index; ?>"
                            data-toggle-item-class="tabsStats__statNumber--active">
                            <?= number_format($number['value'], 0, '', ' '); ?>
                          </li>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </ul>
                  </div>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>