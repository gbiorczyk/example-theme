<section class="mapTabs section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="mapTabs__inner">
    <div class="container">
      <h3 class="mapTabs__headline section__headline"><?= $section['headline']; ?></h3>
      <div class="mapTabs__wrapper"
        data-map-key="<?= get_field('google_maps_api_key', 'options'); ?>"
        data-map-pins="<?= get_template_directory_uri() . '/public/img/'; ?>">
        <div class="mapTabs__tabs tabsLine">
          <ul class="tabsLine__items tabsLine__items--desktop">
            <?php foreach ($section['tabs'] as $index => $tab) : ?>
              <li class="tabsLine__item">
                <a href="#" class="tabsLine__itemLink <?= ($index === 0) ? 'tabsLine__itemLink--active' : ''; ?>">
                  <div class="tabsLine__itemText"
                    data-text="<?= $tab['name']; ?>"><?= $tab['name']; ?></div>
                </a>
              </li>
            <?php endforeach; ?>
          </ul>
          <div class="tabsLine__slider sliderArrows">
            <div class="tabsLine__sliderInner sliderArrows__slider swiper-container">
              <ul class="tabsLine__items tabsLine__items--mobile swiper-wrapper">
                <?php foreach ($section['tabs'] as $index => $tab) : ?>
                  <li class="tabsLine__item swiper-slide">
                    <a href="#" class="tabsLine__itemLink <?= ($index === 0) ? 'tabsLine__itemLink--active' : ''; ?>">
                      <div class="tabsLine__itemText"
                        data-text="<?= $tab['name']; ?>"><?= $tab['name']; ?></div>
                    </a>
                  </li>
                <?php endforeach; ?>
              </ul>
            </div>
            <button class="sliderArrows__nav sliderArrows__nav--prev"
              ref="sliderPrev"></button>
            <button class="sliderArrows__nav sliderArrows__nav--next"
              ref="sliderNext"></button>
            <div class="tabsStats__sliderProgress sliderArrows__progress"
              ref="sliderProgress"></div>
          </div>
        </div>
        <ul class="mapTabs__items">
          <?php foreach ($section['tabs'] as $index => $tab) : ?>
            <li class="mapTabs__item <?= ($index === 0) ? 'mapTabs__item--active' : ''; ?>"
              data-map-zoom="<?= $tab['zoom']; ?>"
              data-map-positions='<?= json_encode(array_column($tab['locations'], 'position')); ?>'>
              <div class="row">
                <div class="col-4 col-lg-5 col-mdl-12">
                  <div class="mapTabs__itemContent wysiwyg"><?= $tab['content']; ?></div>
                  <h5 class="mapTabs__itemTitle"><?= $tab['headline']; ?></h5>
                  <ul class="mapTabs__itemLocations">
                    <?php foreach ($tab['locations'] as $index => $location) : ?>
                      <li class="mapTabs__itemLocation <?= ($index === 0) ? 'mapTabs__itemLocation--active' : ''; ?>">
                        <div class="mapTabs__itemLocationContent wysiwyg"><?= $location['content']; ?></div>
                      </li>
                    <?php endforeach; ?>
                  </ul>
                </div>
                <div class="col-7 offset-col-1 col-lg-7 col-mdl-12">
                  <div class="mapTabs__itemMap"></div>
                </div>
              </div>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>
  </div>
</section>