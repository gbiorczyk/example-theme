<section class="iconsDrop section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="iconsDrop__inner">
    <div class="container">
      <h3 class="iconsDrop__headline section__headline"
        data-hide-scroll="left"><?= $section['headline']; ?></h3>
      <ul class="iconsDrop__items row row--flex" data-hide-scroll>
        <?php foreach ($section['items'] as $index => $item) : ?>
          <li class="iconsDrop__item col-4 col-mdl-12">
            <div class="iconsDrop__itemInner">
              <a href="#" class="iconsDrop__itemHeader">
                <div class="iconsDrop__itemIcon icon-<?= $item['icon']; ?>"></div>
                <h5 class="iconsDrop__itemTitle"><?= $item['title']; ?></h5>
              </a>
              <div class="iconsDrop__itemDrop">
                <div class="iconsDrop__itemDropInner">
                  <div class="iconsDrop__itemDropContent"><?= $item['content']; ?></div>
                  <div class="iconsDrop__itemDropButton">
                    <a href="<?= $item['button']['url']; ?>" target="<?= $item['button']['target']; ?>"
                      class="button button--fill button--red"><?= $item['button']['title']; ?></a>
                  </div>
                </div>
              </div>
              <div class="iconsDrop__itemFooter">
                <a href="#" class="iconsDrop__itemButton"></a>
              </div>
            </div>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
</section>