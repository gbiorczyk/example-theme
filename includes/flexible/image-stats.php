<section class="imageStats section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="imageStats__inner">
    <div class="container">
      <ul class="imageStats__columns row row--flex">
        <li class="imageStats__column imageStats__column--content col-5 col-mdl-12">
          <div class="imageStats__content wysiwyg"><?= $section['content']; ?></div>
        </li>
        <li class="imageStats__column imageStats__column--media col-7 col-mdl-12">
          <div class="imageStats__image">
            <img src="<?= $section['image']['sizes']['image-medium']; ?>" alt="<?= $section['image']['alt']; ?>"
              class="imageStats__imageInner">
            <ul class="imageStats__stats"
              data-number-group>
              <?php foreach ($section['stats'] as $stat) : ?>
                <li class="imageStats__stat"
                  style="top: <?= $stat['position']['top']; ?>%; left: <?= $stat['position']['left']; ?>%;">
                  <div class="imageStats__statValue"
                    data-number="<?= $stat['value']; ?>"
                    data-number-pattern="%s">0</div>
                  <h5 class="imageStats__statTitle"><?= $stat['title']; ?></h5>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
        </li>
      </ul>
    </div>
  </div>
</section>