<section class="tabsContent section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="tabsContent__inner">
    <div class="container">
      <div class="tabsContent__wrapper row" data-toggle>
        <div class="tabsContent__nav col-2 col-mdl-12">
          <div class="tabsContent__navButtonsWrapper">
            <ul class="tabsContent__navButtons">
              <?php foreach ($section['tabs'] as $index => $tab) : ?>
                <li class="tabsContent__navButton">
                  <a href="#" class="tabsContent__navButtonLink <?= ($index === 0) ? 'tabsContent__navButtonLink--active' : ''; ?>"
                    data-toggle-button="<?= $index; ?>" data-toggle-button-class="tabsContent__navButtonLink--active">
                    <div class="tabsContent__navButtonInner" data-hide-scroll>
                      <div class="tabsContent__navButtonImages">
                        <img src="<?= $tab['icon']['url']; ?>" alt="<?= $tab['icon']['alt']; ?>"
                          class="tabsContent__navButtonImage">
                        <img src="<?= $tab['icon_hover']['url']; ?>" alt="<?= $tab['icon_hover']['alt']; ?>"
                          class="tabsContent__navButtonImage">
                      </div>
                      <h4 class="tabsContent__navButtonTitle"><?= $tab['title']; ?></h4>
                    </div>
                  </a>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
          <div class="tabsContent__slider sliderArrows sliderArrows--white">
            <div class="tabsContent__sliderInner sliderArrows__slider swiper-container">
              <ul class="tabsContent__navButtons swiper-wrapper">
                <?php foreach ($section['tabs'] as $index => $tab) : ?>
                  <li class="tabsContent__navButton swiper-slide">
                    <div class="tabsContent__navButtonLink">
                      <div class="tabsContent__navButtonImages">
                        <img src="<?= $tab['icon']['url']; ?>" alt="<?= $tab['icon']['alt']; ?>"
                          class="tabsContent__navButtonImage">
                      </div>
                      <h4 class="tabsContent__navButtonTitle"><?= $tab['title']; ?></h4>
                    </div>
                  </li>
                <?php endforeach; ?>
              </ul>
            </div>
            <button class="sliderArrows__nav sliderArrows__nav--prev"></button>
            <button class="sliderArrows__nav sliderArrows__nav--next"></button>
            <div class="tabsContent__sliderProgress sliderArrows__progress"></div>
          </div>
        </div>
        <div class="tabsContent__content col-5 col-mdl-7 col-md-12">
          <ul class="tabsContent__items" data-hide-scroll="right">
            <?php foreach ($section['tabs'] as $index => $tab) : ?>
              <li class="tabsContent__item <?= ($index === 0) ? 'tabsContent__item--active' : ''; ?>"
                data-toggle-item="<?= $index; ?>" data-toggle-item-class="tabsContent__item--active">
                <div class="tabsContent__itemInner">
                  <div class="tabsContent__itemContent"><?= $tab['content']; ?></div>
                  <div class="tabsContent__itemDescButton">
                    <a href="<?= $tab['button']['url']; ?>" target="<?= $tab['button']['target']; ?>"
                      class="button button--border button--red"><?= $tab['button']['title']; ?></a>
                  </div>
                </div>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
        <div class="tabsContent__imagesWrapper col-5 col-md-12">
          <div class="tabsContent__imagesInner" data-hide-scroll="top">
            <ul class="tabsContent__images">
              <?php foreach ($section['tabs'] as $index => $tab) : ?>
                <li class="tabsContent__image <?= ($index === 0) ? 'tabsContent__image--active' : ''; ?>"
                  data-toggle-item="<?= $index; ?>" data-toggle-item-class="tabsContent__image--active"
                  style="background-image: url(<?= $tab['image']['sizes']['image-medium']; ?>);"></li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>