<section class="imageContent section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="imageContent__inner">
    <div class="container">
      <?php if ($section['headline']) : ?>
        <h3 class="imageContent__headline section__headline"
          data-hide-scroll="left"><?= $section['headline']; ?></h3>
      <?php endif; ?>
      <ul class="imageContent__wrapper <?= ($section['image_position'] === 'left') ? 'imageContent__wrapper--reverse' : ''; ?> imageContent__wrapper--overlay<?= ucfirst($section['overlay_position']); ?> <?= ($section['is_align_top']) ? 'imageContent__wrapper--contentTop' : ''; ?> row row--flex">
        <li class="imageContent__column imageContent__column--content col-8 col-md-12"
          data-hide-scroll="<?= ($section['image_position'] === 'left') ? 'right' : 'left'; ?>">
          <div class="imageContent__content">
            <div class="imageContent__contentText wysiwyg"><?= $section['content']; ?></div>
            <?php if ($section['button']) : ?>
              <div class="imageContent__button">
                <a href="<?= $section['button']['url']; ?>" target="<?= $section['button']['target']; ?>"
                  class="button button--border button--red"><?= $section['button']['title']; ?></a>
              </div>
            <?php endif; ?>
          </div>
        </li>
        <li class="imageContent__column imageContent__column--media col-4 col-md-12"
          data-hide-scroll="<?= ($section['image_position'] === 'left') ? 'left' : 'right'; ?>">
          <div class="imageContent__media">
            <div class="imageContent__mediaInner imageContent__mediaInner--bg<?= ucfirst($section['overlay_color']); ?>">
              <img src="<?= $section['image']['sizes']['image-medium']; ?>"
                alt="<?= $section['image']['alt']; ?>"
                class="imageContent__mediaImage">
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</section>