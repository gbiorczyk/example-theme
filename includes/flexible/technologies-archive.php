<?php if ($items = apply_filters('technologies_posts', [], ['orderby' => 'title'])) : ?>
  <section class="iconsList section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
    data-scroll-id="<?= $section['section_id']; ?>">
    <div class="iconsList__inner">
      <div class="container">
        <h3 class="iconsList__headline section__headline"
          data-hide-scroll="left"><?= $section['headline']; ?></h3>
        <ul class="iconsList__items row row--flex">
          <?php foreach ($items as $item) : ?>
            <li class="iconsList__item iconsList__item--color<?= ucfirst($item['color']); ?> col-4 col-mdl-6 col-md-12">
              <div class="iconsList__itemOuter" data-hide-scroll>
                <div class="iconsList__itemInner">
                  <div class="iconsList__itemIconWrapper">
                    <a href="<?= $item['url']; ?>" class="iconsList__itemIcon icon-<?= $item['icon']; ?>"></a>
                  </div>
                  <div class="iconsList__itemContent">
                    <div class="iconsList__itemContentInner">
                      <h5 class="iconsList__itemTitle">
                        <a href="<?= $item['url']; ?>" class="iconsList__itemTitleLink"
                          data-text="<?= $item['title']; ?>"><?= $item['title']; ?></a>
                      </h5>
                      <div class="iconsList__itemDesc" data-adjust-height><?= $item['desc']; ?></div>
                      <div class="iconsList__itemButton">
                        <a href="<?= $item['url']; ?>" class="button button--border button--red"><?= __('Read more', 'lang'); ?></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>
  </section>
<?php endif;