<section class="textSection section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="textSection__inner">
    <div class="container">
      <div class="row">
        <div class="<?= ($section['is_wide']) ? 'col-10 offset-col-1' : 'col-8 offset-col-2'; ?> col-mdl-12">
          <div class="textSection__wrapper wysiwyg" data-hide-scroll><?= $section['content']; ?></div>
        </div>
      </div>
    </div>
  </div>
</section>