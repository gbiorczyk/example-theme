<section class="instructionSection section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="instructionSection__inner">
    <div class="container">
      <h3 class="instructionSection__headline section__headline"
        data-hide-scroll="left"><?= $section['headline']; ?></h3>
      <div class="instructionSection__columns row">
        <div class="instructionSection__column instructionSection__column--content col-6 col-mdl-12">
          <ul class="instructionSection__groups"
            data-hide-scroll>
            <?php foreach ($section['items'] as $group) : ?>
              <li class="instructionSection__group">
                <a href="#" class="instructionSection__groupTitle">
                  <?= $group['title']; ?>
                </a>
                <div class="instructionSection__groupContent">
                  <ul class="instructionSection__groupItems">
                    <?php foreach ($group['points'] as $index => $point) : ?>
                      <li class="instructionSection__groupItem">
                        <a href="#"
                          class="instructionSection__groupItemLink <?= ($index === 0) ? 'instructionSection__groupItemLink--active' : ''; ?>"
                          data-text="<?= $point['name']; ?>"><?= $point['name']; ?></a>
                        <div class="instructionSection__image instructionSection__image--mobile <?= ($index === 0) ? 'instructionSection__image--active' : ''; ?>">
                          <?php if ($point['image']) : ?>
                            <div class="instructionSection__imageOuter">
                              <div class="instructionSection__imageInner"
                                style="background-image: url(<?= $point['image']['sizes']['image-medium']; ?>);"></div>
                            </div>
                          <?php endif; ?>
                        </div>
                      </li>
                    <?php endforeach; ?>
                  </ul>
                  <?php if ($group['file']) : ?>
                    <div class="instructionSection__groupFile">
                      <div class="instructionSection__groupFileInner fileItem">
                        <a href="<?= $group['file']['url']; ?>" target="_blank"
                          class="fileItem__inner">
                          <div class="fileItem__content">
                            <h5 class="fileItem__title"><?= $group['file_name']; ?></h5>
                            <div class="fileItem__desc">
                              <?= apply_filters('helpers_filesize', '', $group['file']['id']); ?>, 
                              <?= sprintf(__('%s File', 'lang'), strtoupper($group['file']['subtype'])); ?>
                            </div>
                          </div>
                          <div class="fileItem__button"></div>
                        </a>
                      </div>
                    </div>
                  <?php endif; ?>
                </div>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
        <div class="instructionSection__column instructionSection__column--images col-6 col-mdl-12">
          <div class="instructionSection__media">
            <?php if ($section['video_url']) : ?>
              <div class="instructionSection__mediaPlayer videoPlayer"
                data-video="<?= $section['video_url']; ?>"
                data-hide-scroll="right">
                <div class="videoPlayer__poster"
                  style="background-image: url(<?= $section['image']['sizes']['image-medium']; ?>);"
                  data-video-poster="videoPlayer__poster--hidden">
                  <a href="#" class="videoPlayer__play" data-video-play="videoPlayer__play--loading">
                    <div class="videoPlayer__playButton"></div>
                    <div class="videoPlayer__playLoading cssLoader"></div>
                  </a>
                </div>
                <div class="videoPlayer__output">
                  <div class="videoPlayer__outputInner" data-video-output></div>
                </div>
              </div>
            <?php else : ?>
              <img src="<?= $section['image']['sizes']['image-medium']; ?>" alt="<?= $section['image']['alt']; ?>"
                class="instructionSection__mediaImage">
            <?php endif; ?>
          </div>
          <ul class="instructionSection__imagesGroups"
            data-hide-scroll="right">
            <?php foreach ($section['items'] as $group) : ?>
              <li class="instructionSection__imagesGroup">
                <ul class="instructionSection__images">
                  <?php foreach ($group['points'] as $index => $point) : ?>
                    <li class="instructionSection__image <?= ($index === 0) ? 'instructionSection__image--active' : ''; ?>">
                      <?php if ($point['image']) : ?>
                        <div class="instructionSection__imageOuter">
                          <div class="instructionSection__imageInner"
                            style="background-image: url(<?= $point['image']['sizes']['image-medium']; ?>);"></div>
                        </div>
                      <?php endif; ?>
                    </li>
                  <?php endforeach; ?>
                </ul>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>