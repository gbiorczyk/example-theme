<section class="videoSection section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="videoSection__inner">
    <div class="container">
      <div class="videoSection__wrapper">
        <div class="videoSection__player videoPlayer"
          data-video="<?= $section['video_url']; ?>"
          data-hide-scroll="right">
          <div class="videoPlayer__poster"
            style="background-image: url(<?= $section['image']['sizes']['image-large']; ?>);"
            data-video-poster="videoPlayer__poster--hidden">
            <a href="#" class="videoPlayer__play" data-video-play="videoPlayer__play--loading">
              <div class="videoPlayer__playButton"></div>
              <div class="videoPlayer__playLoading cssLoader"></div>
            </a>
          </div>
          <div class="videoPlayer__output">
            <div class="videoPlayer__outputInner" data-video-output></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>