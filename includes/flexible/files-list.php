<section class="filesList section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="filesList__inner">
    <div class="container">
      <h3 class="filesList__headline section__headline"
        data-hide-scroll="left"><?= $section['headline']; ?></h3>
      <ul class="filesList__items row" data-hide-scroll>
        <?php foreach ($section['files'] as $item) : ?>
          <li class="filesList__item fileItem col-3 col-mdl-6 col-mds-12">
            <a href="<?= $item['file']['url']; ?>" target="_blank"
              class="fileItem__inner">
              <div class="fileItem__content">
                <h5 class="fileItem__title"><?= $item['name']; ?></h5>
                <div class="fileItem__desc">
                  <?= apply_filters('helpers_filesize', '', $item['file']['id']); ?>, 
                  <?= sprintf(__('%s File', 'lang'), strtoupper($item['file']['subtype'])); ?>
                </div>
              </div>
              <div class="fileItem__button"></div>
            </a>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
</section>