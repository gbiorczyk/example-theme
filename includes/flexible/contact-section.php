<section class="contactSection section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="contactSection__inner">
    <div class="container">
      <div class="contactSection__wrapper <?= ($section['sidebar_position'] === 'left') ? 'contactSection__wrapper--reverse' : ''; ?> row">
        <?php if ($section['is_content_sidebar']) : ?>
          <div class="contactSection__sidebar col-5 col-md-12">
            <div class="contactSection__sidebarContent"
              data-hide-scroll="<?= ($section['sidebar_position'] === 'left') ? 'left' : 'right'; ?>">
              <div class="contactSection__sidebarContentInner wysiwyg">
                <?= preg_replace('/<h(?:[0-9])>(.*)<\/h(?:[0-9])>/', '<h3>${1}</h3>', $section['content']); ?>
              </div>
            </div>
          </div>
        <?php endif; ?>
        <div class="contactSection__content col-6 col-mdl-7 col-md-12">
          <div class="contactSection__contentInner"
            data-hide-scroll="<?= ($section['sidebar_position'] === 'left') ? 'right' : 'left'; ?>">
            <?php if (!$section['is_content_sidebar']) : ?>
              <div class="contactSection__contentText">
                <?= (!$section['is_large_title'])
                  ? $section['content']
                  : preg_replace('/<h(?:[0-9])>(.*)<\/h(?:[0-9])>/', '<h1>${1}</h1>', $section['content']); ?>
              </div>
            <?php endif; ?>
            <div class="contactSection__contentForm <?= ($section['is_large_title']) ? 'contactSection__contentForm--spacing' : ''; ?>">
              <?php do_action('wpf_forms_load', $section['form_id']); ?>
            </div>
          </div>
        </div>
        <?php if (!$section['is_content_sidebar']) : ?>
          <div class="contactSection__sidebar col-5 col-md-12">
            <div class="contactSection__imageOuter"
              data-hide-scroll="<?= ($section['sidebar_position'] === 'left') ? 'left' : 'right'; ?>">
              <img src="<?= $section['image']['sizes']['image-medium']; ?>" alt="<?= $section['image']['alt']; ?>"
                class="contactSection__imageInner">
            </div>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>