<section class="imageSection section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="imageSection__inner">
    <div class="container">
      <div class="row">
        <div class="col-10 offset-col-1 col-mdl-12">
          <div class="imageSection__wrapper">
            <img src="<?= $section['image']['sizes']['image-medium']; ?>" alt="<?= $section['image']['alt']; ?>"
              class="imageSection__image" data-hide-scroll="left">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>