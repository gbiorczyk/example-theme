<section class="iconsList section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="iconsList__inner">
    <div class="container">
      <h3 class="iconsList__headline section__headline"
        data-hide-scroll="left"><?= $section['headline']; ?></h3>
      <ul class="iconsList__items row row--flex">
        <?php foreach ($section['icons'] as $item) : ?>
          <li class="iconsList__item iconsList__item--color<?= ucfirst($item['icon_color']); ?> <?= ($section['layout'] === 'columns') ? 'iconsList__item--columns' : ''; ?> <?= ($section['size'] === 'large') ? 'iconsList__item--large' : ''; ?> <?= ($section['item_width'] === 'large') ? 'col-6' : 'col-4'; ?> col-mdl-6 col-md-12">
            <div class="iconsList__itemOuter" data-hide-scroll>
              <div class="iconsList__itemInner">
                <div class="iconsList__itemIconWrapper">
                  <div class="iconsList__itemIcon icon-<?= $item['icon']; ?>"></div>
                </div>
                <div class="iconsList__itemContent <?= (!$item['desc'] && !$item['button']) ? 'iconsList__itemContent--alignCenter' : ''; ?>">
                  <div class="iconsList__itemContentInner">
                    <?php if ($item['title']) : ?>
                      <h5 class="iconsList__itemTitle iconsList__itemTitle--nonLink <?= ($section['title_color'] === 'violet') ? 'iconsList__itemTitle--violet' : ''; ?>">
                        <?= $item['title']; ?>
                      </h5>
                    <?php endif; ?>
                    <?php if ($item['desc']) : ?>
                      <div class="iconsList__itemDesc" data-adjust-height2><?= $item['desc']; ?></div>
                    <?php endif; ?>
                    <?php if ($item['button']) : ?>
                      <div class="iconsList__itemButton">
                        <a href="<?= $item['button']['url']; ?>" target="<?= $item['button']['target']; ?>"
                          class="button button--border button--red"><?= $item['button']['title']; ?></a>
                      </div>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
          </li>
        <?php endforeach; ?>
      </ul>
      <?php if ($section['button']) : ?>
        <div class="iconsList__button" data-hide-scroll>
          <a href="<?= $section['button']['url']; ?>" target="<?= $section['button']['target']; ?>"
            class="button button--border button--red"><?= $section['button']['title']; ?></a>
        </div>
      <?php endif; ?>
    </div>
  </div>
</section>