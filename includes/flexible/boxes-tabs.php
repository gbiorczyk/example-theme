<section class="boxesTabs section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="boxesTabs__inner">
    <div class="container">
      <h3 class="boxesTabs__headline section__headline"
        data-hide-scroll="left"><?= $section['headline']; ?></h3>
      <div class="boxesTabs__wrapper row" data-toggle>
        <div class="col-6 col-md-12">
          <div class="boxesTabs__buttons"
            data-hide-scroll="left">
            <ul class="boxesTabs__buttonsInner boxesList">
              <?php foreach ($section['tabs'] as $index => $tab) : ?>
                <?php if ($data = apply_filters('technologies_post', [], $tab['technology'])) : ?>
                  <li class="boxesList__item">
                    <a href="#" class="boxesList__itemLink boxesList__itemLink--<?= $data['color']; ?> <?= ($index === 0) ? 'boxesList__itemLink--active' : ''; ?>"
                      data-toggle-button="<?= $index; ?>"
                      data-toggle-button-class="boxesList__itemLink--active">
                      <div class="boxesList__itemIcon icon-<?= $data['icon']; ?>"></div>
                      <div class="boxesList__itemInner">
                        <h4 class="boxesList__itemTitle"><?= $data['title']; ?></h4>
                      </div>
                    </a>
                  </li>
                <?php endif; ?>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
        <div class="col-6 col-md-12">
          <ul class="boxesTabs__items"
            data-toggle-scroll data-hide-scroll>
            <?php foreach ($section['tabs'] as $index => $tab) : ?>
              <?php if ($data = apply_filters('technologies_post', [], $tab['technology'])) : ?>
                <li class="boxesTabs__item <?= ($index === 0) ? 'boxesTabs__item--active' : ''; ?>"
                  data-toggle-item="<?= $index; ?>"
                  data-toggle-item-class="boxesTabs__item--active">
                  <h4 class="boxesTabs__itemTitle"><?= $data['title']; ?></h4>
                  <div class="boxesTabs__itemContent"><?= $tab['content']; ?></div>
                  <div class="boxesTabs__itemButton">
                    <a href="<?= $data['url']; ?>"
                      class="button button--border button--red"><?= __('Go to the technology page', 'lang'); ?></a>
                  </div>
                </li>
              <?php endif; ?>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>