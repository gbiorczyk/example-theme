<section class="statsList section <?= ($section['section_is_space']) ? 'section--spaceLarge' : ''; ?>"
  data-scroll-id="<?= $section['section_id']; ?>">
  <div class="statsList__inner">
    <div class="container">
      <div class="statsList__wrapper">
        <ul class="statsList__items row row--flex"
          data-number-group data-number-group-thousands=","
          data-hide-scroll>
          <?php foreach ($section['stats'] as $stat) : ?>
            <li class="statsList__item col-3 col-mdl-6 col-mds-12">
              <div class="statsList__itemInner">
                <div class="statsList__itemIcon"></div>
                <h3 class="statsList__itemValue">
                  <strong class="statsList__itemValueNumber"
                    data-number="<?= $stat['value']; ?>"
                    data-number-pattern="<?= $stat['pattern']; ?>">
                    <?= str_replace('%s', '0', $stat['pattern']); ?>
                  </strong> <?= $stat['suffix']; ?>
                </h3>
                <div class="statsList__itemDesc"><?= $stat['desc']; ?></div>
              </div>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>
  </div>
</section>