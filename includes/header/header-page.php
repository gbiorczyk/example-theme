<?php if ($header = get_field('header')) : ?>
  <section class="pageHeader">
    <div class="pageHeader__inner <?= ($header['bg_color']) ? 'pageHeader__inner--bg' . ucfirst($header['bg_color']) : ''; ?> <?= ($isHidden = get_field('header_is_hidden')) ? 'pageHeader__inner--small' : ''; ?>">
      <div class="container">
        <div class="row">
          <div class="col-7 col-md-12">
            <div class="pageHeader__content <?= ($header['is_hidden'] || !$header['is_box_active']) ? 'pageHeader__content--small' : ''; ?>">
              <?php if ($items = apply_filters('wpf_breadcrumbs', [])) : ?>
                <div class="pageHeader__breadcrumbs">
                  <ul class="pageHeader__breadcrumbsItems">
                    <?php foreach ($items as $index => $item) : ?>
                      <li class="pageHeader__breadcrumbsItem">
                        <?php if ($index < (count($items) - 1)) : ?>
                          <a href="<?= $item['url']; ?>" class="pageHeader__breadcrumbsItemLink"><?= $item['title']; ?></a>
                        <?php else : ?>
                          <?= $item['title']; ?>
                        <?php endif; ?>
                      </li>
                      <?php if ($index < (count($items) - 1)) : ?>
                        <li class="pageHeader__breadcrumbsItem pageHeader__breadcrumbsItem--arrow"></li>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </ul>
                </div>
              <?php endif; ?>
              <?php if (!$header['is_hidden']) : ?>
                <h1 class="pageHeader__contentTitle"><?= get_the_title(); ?></h1>
                <?php if ($header['desc']) : ?>
                  <div class="pageHeader__contentDesc"><?= $header['desc']; ?></div>
                <?php endif; ?>
                <?php if ($header['button']) : ?>
                  <div class="pageHeader__contentButton">
                    <a href="<?= $header['button']['url']; ?>" target="<?= $header['button']['target']; ?>"
                      class="pageHeader__contentButtonInner button button--border <?= ($header['bg_color'] === 'none') ? 'button--red' : (($header['bg_color'] === 'yellow') ? 'button--violet' : 'button--white'); ?>">
                      <?= $header['button']['title']; ?>
                    </a>
                  </div>
                <?php endif; ?>
              <?php endif; ?>
            </div>
          </div>
          <?php if (!$header['is_hidden'] && $header['is_box_active']) : ?>
            <div class="col-4 <?= ($header['box']['is_space_before']) ? 'offset-col-1' : ''; ?> col-md-12">
              <div class="pageHeader__media">
                <div class="pageHeader__mediaInner <?= ($header['box']['is_square']) ? 'pageHeader__mediaInner--square' : ''; ?> <?= (in_array($header['box']['image_alignment'], ['top-left', 'top-right'])) ? 'pageHeader__mediaInner--alignTop' : 'pageHeader__mediaInner--alignBottom'; ?> <?= (in_array($header['box']['image_alignment'], ['top-left', 'bottom-left'])) ? 'pageHeader__mediaInner--alignLeft' : 'pageHeader__mediaInner--alignRight'; ?>">
                  <div class="pageHeader__mediaBox pageHeader__mediaBox--bg<?= ucfirst($header['box']['bg_color']); ?>">
                    <?php if ($icon = $header['box']['icon'] ?? '') : ?>
                      <div class="pageHeader__mediaIcon icon-<?= $icon; ?>"></div>
                    <?php endif; ?>
                  </div>
                  <img src="<?= $header['box']['image']['sizes']['image-medium']; ?>" alt="<?= $header['box']['image']['alt']; ?>"
                    class="pageHeader__mediaImage">
                </div>
              </div>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </section>
<?php endif;