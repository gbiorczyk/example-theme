<section class="pageHeader">
  <div class="pageHeader__inner pageHeader__inner--small">
    <div class="container">
      <div class="row">
        <div class="col-7 col-md-12">
          <div class="pageHeader__content pageHeader__content--small">
            <?php if ($items = apply_filters('wpf_breadcrumbs', [])) : ?>
              <div class="pageHeader__breadcrumbs">
                <ul class="pageHeader__breadcrumbsItems">
                  <?php foreach ($items as $index => $item) : ?>
                    <li class="pageHeader__breadcrumbsItem">
                      <?php if ($index < (count($items) - 1)) : ?>
                        <a href="<?= $item['url']; ?>" class="pageHeader__breadcrumbsItemLink"><?= $item['title']; ?></a>
                      <?php else : ?>
                        <?= $item['title']; ?>
                      <?php endif; ?>
                    </li>
                    <?php if ($index < (count($items) - 1)) : ?>
                      <li class="pageHeader__breadcrumbsItem pageHeader__breadcrumbsItem--arrow"></li>
                    <?php endif; ?>
                  <?php endforeach; ?>
                </ul>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>