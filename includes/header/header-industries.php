<?php if ($header = get_field('header')) : ?>
  <section class="pageHeader">
    <div class="pageHeader__inner pageHeader__inner--bgPurple">
      <div class="container">
        <div class="row">
          <div class="col-7 col-md-12">
            <div class="pageHeader__content">
              <?php if ($items = apply_filters('wpf_breadcrumbs', [])) : ?>
                <div class="pageHeader__breadcrumbs">
                  <ul class="pageHeader__breadcrumbsItems">
                    <?php foreach ($items as $index => $item) : ?>
                      <li class="pageHeader__breadcrumbsItem">
                        <?php if ($index < (count($items) - 1)) : ?>
                          <a href="<?= $item['url']; ?>" class="pageHeader__breadcrumbsItemLink"><?= $item['title']; ?></a>
                        <?php else : ?>
                          <?= $item['title']; ?>
                        <?php endif; ?>
                      </li>
                      <?php if ($index < (count($items) - 1)) : ?>
                        <li class="pageHeader__breadcrumbsItem pageHeader__breadcrumbsItem--arrow"></li>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </ul>
                </div>
              <?php endif; ?>
              <h1 class="pageHeader__contentTitle"><?= get_the_title(); ?></h1>
              <?php if ($header['desc']) : ?>
                <div class="pageHeader__contentDesc"><?= $header['desc']; ?></div>
              <?php endif; ?>
            </div>
          </div>
          <div class="col-4 <?= ($header['box']['is_space_before']) ? 'offset-col-1' : ''; ?> col-md-12">
            <div class="pageHeader__media">
              <div class="pageHeader__mediaInner <?= (in_array($header['box']['image_alignment'], ['top-left', 'top-right'])) ? 'pageHeader__mediaInner--alignTop' : 'pageHeader__mediaInner--alignBottom'; ?> <?= (in_array($header['box']['image_alignment'], ['top-left', 'bottom-left'])) ? 'pageHeader__mediaInner--alignLeft' : 'pageHeader__mediaInner--alignRight'; ?>">
                <div class="pageHeader__mediaBox pageHeader__mediaBox--bg<?= ucfirst($header['box']['bg_color']); ?>"></div>
                <img src="<?= $header['box']['image']['sizes']['image-medium']; ?>" alt="<?= $header['box']['image']['alt']; ?>"
                  class="pageHeader__mediaImage">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif;