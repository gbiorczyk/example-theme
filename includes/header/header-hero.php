<section class="heroHeader" data-full-height>
  <div class="heroHeader__inner" data-full-height data-cursor>
    <div class="heroHeader__content">
      <div class="container">
        <div class="heroHeader__contentText">
          <h1 class="heroHeader__contentTitle"><?= get_field('header_title'); ?></h1>
          <?php if ($link = get_field('header_link')) : ?>
            <a href="<?= $link['url']; ?>" target="<?= $link['target']; ?>"
              class="heroHeader__contentButton button button--border button--red"><?= $link['title']; ?></a>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <div class="heroHeader__bg">
      <div class="heroHeader__bgOuter container">
        <div class="heroHeader__bgInner">
          <div class="heroHeader__bgImage" data-cursor-items>
            <?php if ($image = get_field('header_image')) : ?>
              <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>"
                class="heroHeader__bgImageInner">
            <?php endif; ?>
            <?php if ($pins = get_field('header_pins')) : ?>
              <?php foreach ($pins as $pin) : ?>
                <div class="heroHeader__pin heroHeader__pin--<?= $pin['color']; ?>"
                  style="top: <?= $pin['position']['top']; ?>%; left: <?= $pin['position']['left']; ?>%; font-size: <?= $pin['size']['hexagon']; ?>px;" data-cursor-item>
                  <div class="heroHeader__pinInner">
                    <div class="heroHeader__pinIcon icon-<?= $pin['icon']; ?>"
                      style="font-size: <?= $pin['size']['icon']; ?>px;"></div>
                  </div>
                </div>
              <?php endforeach; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>