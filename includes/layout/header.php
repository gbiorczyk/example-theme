<header class="header">
  <div class="header__inner">
    <div class="header__wrapper">
      <?php if ($image = get_field('header_logo', 'options')) : ?>
        <div class="header__logo">
          <a href="<?= home_url('/'); ?>" class="header__logoLink">
            <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>"
              class="header__logoImage">
          </a>
        </div>
      <?php endif; ?>
      <?php if ($items = apply_filters('wpf_menu', [], 'main_nav')) : ?>
        <div class="header__menu menu">
          <ul class="menu__items">
            <?php foreach ($items as $item) : ?>
              <li class="menu__item <?= ($item['children']) ? 'menu__item--hasChildren' : ''; ?>">
                <a href="<?= $item['url']; ?>" target="<?= $item['target']; ?>"
                  class="menu__itemLink <?= ($item['active']) ? 'menu__itemLink--active' : ''; ?>"
                  data-text="<?= $item['title']; ?>"><?= $item['title']; ?></a>
                <ul class="menu__submenu">
                  <?php foreach ($item['children'] as $item) : ?>
                    <li class="menu__submenuItem">
                      <a href="<?= $item['url']; ?>" target="<?= $item['target']; ?>"
                        class="menu__submenuItemLink <?= ($item['active']) ? 'menu__submenuItemLink--active' : ''; ?>"
                        data-text="<?= $item['title']; ?>"><?= $item['title']; ?></a>
                    </li>
                  <?php endforeach; ?>
                </ul>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      <?php endif; ?>
      <ul class="header__buttons">
        <?php if (($langs = apply_filters('wpf_langs', [], 'title')) && $langs['others']) : ?>
          <?php foreach ($langs['others'] as $lang) : ?>
            <li class="header__button">
              <a href="<?= $lang['url']; ?>" class="header__buttonLink">
                <span class="header__buttonText header__buttonText--lang"><?= $lang['title']; ?></span>
              </a>
            </li>
          <?php endforeach; ?>
        <?php endif; ?>
        <li class="header__button">
          <a href="#" class="header__buttonLink header__buttonLink--search">
            <span class="header__buttonIcon icon-loupe"></span>
          </a>
        </li>
      </ul>
      <div class="header__search">
        <div class="container">
          <form action="<?= home_url('/'); ?>" class="header__searchForm">
            <input type="text" name="s" class="header__searchInput" placeholder="<?= __('Search', 'lang'); ?>">
            <button type="submit" class="header__searchButton"></button>
          </form>
        </div>
      </div>
      <div class="header__menuMobile">
        <?php get_template_part('includes/layout/menu-mobile'); ?>
      </div>
      <button class="header__menuToggle menuToggle" data-mobile-toggle="menuToggle--active">
        <span class="menuToggle__line"></span>
        <span class="menuToggle__line"></span>
        <span class="menuToggle__line"></span>
      </button>
    </div>
  </div>
</header>