<footer class="footer <?= (is_search()) ? 'footer--lineGray' : ''; ?>">
  <div class="footer__inner">
    <div class="container">
      <div class="footer__main">
        <div class="footer__mainRow">
          <div class="footer__menuOuter row">
            <div class="footer__menuInner col-10 col-mdl-12">
              <?php if ($items = apply_filters('wpf_menu', [], 'footer_nav')) : ?>
                <div class="footer__menu">
                  <ul class="footer__menuItems">
                    <?php foreach ($items as $item) : ?>
                      <li class="footer__menuItem">
                        <a href="<?= $item['url']; ?>" target="<?= $item['target']; ?>"
                          class="footer__menuItemLink <?= ($item['active']) ? 'footer__menuItemLink--active' : ''; ?>">
                          <span class="footer__menuItemText"
                            data-text="<?= $item['title']; ?>"><?= $item['title']; ?></span>
                        </a>
                      </li>
                    <?php endforeach; ?>
                  </ul>
                </div>
              <?php endif; ?>
            </div>
          </div>
        </div>
        <div class="footer__mainRow">
          <ul class="footer__columns row">
            <li class="footer__column col-5 col-mdl-6 col-md-12">
              <div class="footer__newsletter">
                <h5 class="footer__newsletterTitle"><?= get_field('footer_newsletter_title', 'options'); ?></h5>
                <div class="footer__newsletterForm">
                  <?php do_action('wpf_forms_load', get_field('footer_newsletter_form', 'options')); ?>
                </div>
              </div>
            </li>
            <li class="footer__column col-5 offset-col-1 col-md-12">
              <div class="footer__info"><?= get_field('footer_contact', 'options'); ?></div>
            </li>
          </ul>
        </div>
      </div>
      <div class="footer__bottom">
        <div class="footer__text"><?= get_field('footer_bottom', 'options'); ?></div>
        <?php if ($icons = get_field('footer_socials', 'options')) : ?>
          <ul class="footer__socials">
            <?php foreach ($icons as $icon) : ?>
              <li class="footer__social">
                <a href="<?= $icon['url']; ?>" target="_blank"
                  class="footer__socialLink icon-<?= $icon['icon']; ?>"></a>
              </li>
            <?php endforeach; ?>
          </ul>
        <?php endif; ?>
      </div>
    </div>
  </div>
</footer>