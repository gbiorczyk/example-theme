<section class="errorAlert">
  <div class="errorAlert__inner">
    <div class="container">
      <div class="errorAlert__wrapper">
        <div class="errorAlert__number">
          <div class="errorAlert__numberBack">404</div>
          <div class="errorAlert__numberFront">404</div>
        </div>
        <div class="errorAlert__content">
          <div class="errorAlert__contentText wysiwyg">
            <h1><?= __('Oops...', 'lang'); ?></h1>
            <p><?= __('The page you requested could not be found.', 'lang'); ?></p>
          </div>
          <div class="errorAlert__contentButtom">
            <a href="<?= home_url('/'); ?>"
              class="errorAlert__contentButtomInner button button--border button--white"><?= __('Back to Home', 'lang'); ?></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>