<?php if ($items = apply_filters('insights_related', [], 3, get_the_ID())) : ?>
  <section class="newsList section">
    <div class="newsList__inner">
      <div class="container">
        <ul class="newsList__items row"
          data-hide-scroll>
          <?php foreach ($items as $item) : ?>
            <div class="newsList__item col-4 col-md-12">
              <a href="<?= $item['url']; ?>" class="newsList__itemInner newsItem">
                <div class="newsItem__inner">
                  <div class="newsItem__image">
                    <div class="newsItem__imageInner"
                      style="background-image: url(<?= $item['image']; ?>);"></div>
                  </div>
                  <h5 class="newsItem__title" data-text="<?= $item['title']; ?>"><?= $item['title']; ?></h5>
                </div>
              </a>
            </div>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>
  </section>
<?php endif;