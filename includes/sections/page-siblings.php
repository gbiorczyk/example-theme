<?php if ($items = apply_filters('pages_children', [], get_the_ID())) : ?>
  <div class="pageSiblings section">
    <div class="pageSiblings__inner">
      <div class="container">
        <div class="tabsLine">
          <ul class="tabsLine__items">
            <?php foreach ($items as $item) : ?>
              <li class="tabsLine__item">
                <a href="<?= $item['url']; ?>" class="tabsLine__itemLink <?= ($item['is_active']) ? 'tabsLine__itemLink--active' : ''; ?>">
                  <div class="tabsLine__itemText"
                    data-text="<?= $item['title']; ?>"><?= $item['title']; ?></div>
                </a>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
<?php endif;