<?php if ($items = apply_filters('technologies_related', [], get_the_ID())) : ?>
  <section class="numberSlider section">
    <div class="numberSlider__inner">
      <div class="container">
        <div class="numberSlider__wrapper row">
          <div class="col-4 col-md-12">
            <h3 class="numberSlider__headline section__headline"
              data-hide-scroll="left"><?= __('All of our technologies', 'lang'); ?></h3>
          </div>
          <div class="col-8 col-md-12">
            <div class="numberSlider__main swiper-container"
              data-hide-scroll="right">
              <div class="numberSlider__nav">
                <div class="numberSlider__navNumber numberSlider__navNumber--current">01</div>
                <div class="numberSlider__navLine">
                  <div class="numberSlider__navLineInner" style="width: 0%;"></div>
                </div>
                <div class="numberSlider__navNumber"><?= str_pad(count($items), 2, '0', STR_PAD_LEFT); ?></div>
              </div>
              <ul class="numberSlider__items swiper-wrapper">
                <?php foreach ($items as $index => $item) : ?>
                  <li class="numberSlider__item swiper-slide">
                    <a href="<?= $item['url']; ?>" class="numberSlider__itemLink <?= ($index === 0) ? 'numberSlider__itemLink--active' : ''; ?>">
                      <div class="numberSlider__itemButton numberSlider__itemButton--<?= $item['color']; ?>"></div>
                      <div class="numberSlider__itemImage"
                        style="background-image: url(<?= $item['image']; ?>);"></div>
                      <h5 class="numberSlider__itemTitle">
                        <span class="numberSlider__itemTitleLink"
                          data-text="<?= $item['title']; ?>"><?= $item['title']; ?></span>
                      </h5>
                    </a>
                  </li>
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <?php foreach ($items as $index => $item) : ?>
        <?php if ($item['has_widget'] && ($widget = get_field('widget', $item['id']))) : ?>
          <div class="numberSlider__popup contentPopup">
            <div class="contentPopup__outer">
              <div class="contentPopup__bg contentPopup__bg--<?= $item['color']; ?>"
                style="background-image: url(<?= $widget['image']['sizes']['image-full']; ?>);"></div>
              <div class="contentPopup__inner">
                <div class="contentPopup__innerInner">
                  <div class="container">
                    <div class="contentPopup__wrapper">
                      <button class="contentPopup__close"></button>
                      <ul class="contentPopup__columns row">
                        <li class="contentPopup__column col-4 col-mdl-12">
                          <div class="contentPopup__text">
                            <div class="contentPopup__textInner"><?= $widget['info']; ?></div>
                            <div class="contentPopup__textButton">
                              <a href="<?= $item['url']; ?>"
                                class="button button--fill button--red"><?= __('Read more', 'lang'); ?></a>
                            </div>
                          </div>
                        </li>
                        <li class="contentPopup__column col-8 col-mdl-12">
                          <ul class="contentPopup__stats row row--flex">
                            <?php foreach ($widget['columns'] as $column) : ?>
                              <li class="contentPopup__stat col-6 col-md-12">
                                <div class="contentPopup__statInner">
                                  <div class="contentPopup__statValue"><?= strip_tags($column['title'], '<strong>'); ?></div>
                                  <div class="contentPopup__statText"><?= $column['desc']; ?></div>
                                </div>
                              </li>
                            <?php endforeach; ?>
                          </ul>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php endif; ?>
      <?php endforeach; ?>
    </div>
  </section>
<?php endif;