<?php if ($info = get_field('info')) : ?>
  <section class="infoHeader section">
    <div class="infoHeader__inner">
      <div class="container">
        <div class="infoHeader__columnsWrapper">
          <ul class="infoHeader__columns">
            <?php if ($items = get_the_terms(get_the_ID(), 'insights-clients')) : ?>
              <li class="infoHeader__column">
                <div class="infoHeader__columnName"><?= __('Client', 'lang'); ?></div>
                <h4 class="infoHeader__columnValue">
                  <?php foreach ($items as $index => $item) : ?>
                    <?= (($index > 0) ? ', ' : '') . $item->name; ?>
                  <?php endforeach; ?>
                </h4>
              </li>
            <?php endif; ?>
            <?php if ($items = get_field('industry_ids')) : ?>
              <li class="infoHeader__column">
                <div class="infoHeader__columnName"><?= __('Industry', 'lang'); ?></div>
                <h4 class="infoHeader__columnValue">
                  <?php foreach ($items as $index => $itemId) : ?>
                    <?= (($index > 0) ? ', ' : '') . get_the_title($itemId); ?>
                  <?php endforeach; ?>
                </h4>
              </li>
            <?php endif; ?>
            <?php if ($items = get_the_terms(get_the_ID(), 'insights-locations')) : ?>
              <li class="infoHeader__column">
                <div class="infoHeader__columnName"><?= __('Location', 'lang'); ?></div>
                <h4 class="infoHeader__columnValue">
                  <?php foreach ($items as $index => $item) : ?>
                    <?= (($index > 0) ? ', ' : '') . $item->name; ?>
                  <?php endforeach; ?>
                </h4>
              </li>
            <?php endif; ?>
            <?php if ($info['website']) : ?>
              <li class="infoHeader__column">
                <div class="infoHeader__columnName"><?= __('Website', 'lang'); ?></div>
                <h4 class="infoHeader__columnValue">
                  <a href="<?= $info['website']; ?>" target="_blank"
                    class="infoHeader__columnValueLink"><?= parse_url($info['website'], PHP_URL_HOST); ?></a>
                </h4>
              </li>
            <?php endif; ?>
          </ul>
        </div>
        <div class="row">
          <div class="col-11 offset-col-1 col-mdl-12">
            <div class="infoHeader__content">
              <div class="infoHeader__contentInner" data-hide-scroll>
                <div class="infoHeader__contentText"><?= $info['solutions_desc']; ?></div>
                <ul class="infoHeader__contentValues">
                  <?php foreach ($info['solutions'] as $item) : ?>
                    <li class="infoHeader__contentValue">
                      <div class="infoHeader__contentValueIcon icon-<?= get_field('icon', $item['solution']); ?>"></div>
                      <h5 class="infoHeader__contentValueTitle"><?= $item['solution']->name; ?></h5>
                    </li>
                  <?php endforeach; ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif;