<?php
  $args = [
    'page'     => get_query_var('paged') ?: 1,
    'phrase'   => $_GET['s'] ?? '',
    'category' => isset($_GET['category']) ? explode(',', $_GET['category']) : [],
    'sort'     => $_GET['sort'] ?? 'date',
  ];
  if ($response = apply_filters('search_api_response', [], $args)) :
?>
  <section class="searchResults">
    <div class="searchResults__inner">
      <div class="dataLoader">
        <data-loader
          :api-url="'<?= apply_filters('search_api_url', null); ?>'"
          :default-form="{ phrase: '', category: [], sort: 'date' }"
          :default-values='{ phrase: "<?= $args['phrase']; ?>", category: <?= json_encode($args['category']); ?>, sort: "<?= $args['sort']; ?>" }'
          :default-items="'<?= base64_encode(json_encode($response['data'])); ?>'"
          :default-pagination="{ page: <?= $args['page']; ?>, pages: <?= $response['pages']; ?> }"
          :loader-config="{ time: 1000, timeCache: 250, delay: 500, delayKeys: ['phrase'], spacing: 20, scrollOffset: 70 }"
          :url-pattern="'<?= home_url('/?s='); ?>!phrase|#!&!page|page=#!&!category|category=#!&!sort|sort=#!'"
          inline-template v-cloak>
          <component>
            <div class="container">
              <div class="searchResults__header">
                <input type="text" v-model="form.phrase" ref="form.phrase"
                  placeholder="<?= __('Search', 'lang'); ?>"
                  class="searchResults__headerInput">
              </div>
            </div>
            <div class="searchResults__content">
              <div class="container">
                <div class="row">
                  <div class="col-3 col-md-12">
                    <?php if ($filters = apply_filters('search_filters', [])) : ?>
                      <ul class="searchResults__filters">
                        <?php foreach ($filters as $filterIndex => $filter) : ?>
                          <li class="searchResults__filter">
                            <h5 class="searchResults__filterTitle"><?= $filter['label']; ?>:</h5>
                            <ul class="searchResults__filterValues">
                              <?php foreach ($filter['values'] as $value => $label) : ?>
                                <li class="searchResults__filterValue">
                                  <input type="<?= $filter['type']; ?>" id="filter_<?= $filterIndex; ?>_<?= $value; ?>"
                                    v-model="form.<?= $filter['key']; ?>" value="<?= $value; ?>" 
                                    class="searchResults__filterInput">
                                  <label for="filter_<?= $filterIndex; ?>_<?= $value; ?>"
                                    class="searchResults__filterLabel"><?= $label; ?></label>
                                </li>
                              <?php endforeach; ?>
                            </ul>
                          </li>
                        <?php endforeach; ?>
                      </ul>
                    <?php endif; ?>
                  </div>
                  <div class="col-8 offset-col-1 col-md-12">
                    <div class="searchResults__wrapper">
                      <!-- BEGIN Pagination prev -->
                      <div class="dataLoader__pagination" v-if="pagination.min > 1">
                        <button type="button" class="button button--border button--red"
                          v-on:click="loadPrev"><?= __('Load prev', 'lang'); ?></button>
                      </div>
                      <!-- END Pagination prev -->

                      <div class="searchResults__wrapperOutput dataLoader__wrapper" ref="wrapper"
                        v-bind:class="{ 'dataLoader__wrapper--loading': status.loading }">
                        <div class="dataLoader__wrapperOutput">
                          <!-- BEGIN Error message -->
                          <div class="dataLoader__wrapperMessage" v-if="status.empty && !status.loading">
                            <div class="dataLoader__wrapperMessageInner">
                              <div class="dataLoader__wrapperMessageText"><?= __('Sorry, we did not find what you are looking for.', 'lang'); ?></div>
                              <div class="dataLoader__wrapperMessageButton">
                                <button type="button" class="button button--border button--red"
                                  v-on:click="resetFilters"><?= __('Reset filters', 'lang'); ?></button>
                              </div>
                            </div>
                          </div>
                          <!-- END Error message -->

                          <!-- BEGIN Items list -->
                          <ul class="dataLoader__wrapperItems searchResults__items">
                            <li v-for="item in items" class="searchResults__item">
                              <a :href="item.url" class="searchResults__itemInner">
                                <div class="searchResults__itemCategory">{{ item.category }}</div>
                                <h4 class="searchResults__itemTitle"
                                  :data-text="item.title">{{ item.title }}</h4>
                                <div class="searchResults__itemDesc"
                                  v-if="item.desc" v-html="item.desc" data-text-wrap></div>
                              </a>
                            </li>
                            <?php foreach ($response['data'] as $item) : ?>
                              <li v-if="!status.updated" class="searchResults__item">
                                <a href="<?= $item['url']; ?>" class="searchResults__itemInner">
                                  <div class="searchResults__itemCategory"><?= $item['category']; ?></div>
                                  <h4 class="searchResults__itemTitle"
                                    data-text="<?= $item['title']; ?>"><?= $item['title']; ?></h4>
                                  <?php if ($item['desc']) : ?>
                                    <div class="searchResults__itemDesc" data-text-wrap><?= $item['desc']; ?></div>
                                  <?php endif; ?>
                                </a>
                              </li>
                            <?php endforeach; ?>
                          </ul>
                          <!-- END Items list -->

                          <div class="dataLoader__wrapperLoader">
                            <div class="cssLoader" ref="loader"></div>
                          </div>
                        </div>
                      </div>

                      <!-- BEGIN Pagination next -->
                      <div class="dataLoader__pagination" v-if="form.page < pagination.pages">
                        <button type="button" class="button button--border button--red"
                          v-on:click="loadMore"><?= __('Load more', 'lang'); ?></button>
                      </div>
                      <!-- END Pagination next -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </component>
        </data-loader>
      </div>
    </div>
  </section>
<?php endif;