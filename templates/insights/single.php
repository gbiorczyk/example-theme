<?php
  get_header();
  get_template_part('includes/header/header', 'insights');
?>
<main class="sections">
  <?php
    get_template_part('includes/sections/insights-info');
    get_template_part('includes/flexible/_core');
    get_template_part('includes/sections/insights-related');
  ?>
</main>
<?php get_footer();