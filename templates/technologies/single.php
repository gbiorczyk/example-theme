<?php
  get_header();
  get_template_part('includes/header/header', 'technologies');
?>
<main class="sections">
  <?php
    get_template_part('includes/flexible/_core');
    get_template_part('includes/sections/technologies-related');
  ?>
</main>
<?php get_footer();