<?php
  get_header();
  get_template_part('includes/header/header', 'breadcrumbs');
?>
<main class="sections">
  <?php
    get_template_part('includes/sections/search-results');
  ?>
</main>
<?php get_footer();