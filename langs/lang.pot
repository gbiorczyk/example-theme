#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: Basic project\n"
"POT-Creation-Date: 2019-12-11 15:47+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-Flags-xgettext: --add-comments=translators:\n"
"X-Poedit-WPHeader: style.css\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"
"Language: "

#: functions/framework/post-types.php:8 includes/flexible/news-archive.php:43
msgid "Technologies"
msgstr ""

#: functions/framework/post-types.php:23
msgid "Industries"
msgstr ""

#: functions/framework/post-types.php:38
msgid "Insights"
msgstr ""

#: functions/framework/post-types.php:53
msgid "Jobs"
msgstr ""

#: functions/framework/taxonomies.php:9
msgid "Clients"
msgstr ""

#: functions/framework/taxonomies.php:25 functions/framework/taxonomies.php:91
msgid "Locations"
msgstr ""

#: functions/framework/taxonomies.php:41
msgid "Solutions"
msgstr ""

#: functions/framework/taxonomies.php:59
msgid "Departments"
msgstr ""

#: functions/framework/taxonomies.php:75
msgid "Types"
msgstr ""

#: functions/framework/taxonomies.php:107
msgid "Experience"
msgstr ""

#: includes/flexible/boxes-tabs.php:41
msgid "Go to the technology page"
msgstr ""

#: includes/flexible/files-list.php:16
#: includes/flexible/instruction-section.php:43
#, php-format
msgid "%s File"
msgstr ""

#: includes/flexible/industries-archive.php:24
#: includes/flexible/technologies-archive.php:24
#: includes/flexible/technologies-list.php:57
#: includes/sections/technologies-related.php:56
msgid "Read more"
msgstr ""

#: includes/flexible/jobs-list.php:21
msgid "Choose department"
msgstr ""

#: includes/flexible/jobs-list.php:24
msgid "All departments"
msgstr ""

#: includes/flexible/jobs-list.php:36
msgid "Choose job type"
msgstr ""

#: includes/flexible/jobs-list.php:39
msgid "All jobs"
msgstr ""

#: includes/flexible/jobs-list.php:51
msgid "Choose job location"
msgstr ""

#: includes/flexible/jobs-list.php:54
msgid "All locations"
msgstr ""

#: includes/flexible/jobs-list.php:72 includes/flexible/news-archive.php:72
#: includes/sections/search-results.php:70
msgid "Sorry, we did not find what you are looking for."
msgstr ""

#: includes/flexible/jobs-list.php:75 includes/flexible/news-archive.php:75
#: includes/sections/search-results.php:73
msgid "Reset filters"
msgstr ""

#: includes/flexible/jobs-list.php:149 includes/flexible/news-archive.php:119
#: includes/flexible/news-list.php:29 includes/flexible/news-list.php:32
#: includes/flexible/news-list.php:35 includes/flexible/people-list.php:62
#: includes/sections/search-results.php:114
msgid "Load more"
msgstr ""

#: includes/flexible/news-archive.php:28 includes/sections/insights-info.php:19
#: functions/api/Search/Filters.php:34
msgid "Industry"
msgstr ""

#: includes/flexible/news-archive.php:32
msgid "All industries"
msgstr ""

#: includes/flexible/news-archive.php:47
msgid "All technologies"
msgstr ""

#: includes/flexible/news-archive.php:62
#: includes/sections/search-results.php:60
msgid "Load prev"
msgstr ""

#: includes/flexible/news-archive.php:130
msgid "Most popular"
msgstr ""

#: includes/layout/header.php:53 includes/sections/search-results.php:26
msgid "Search"
msgstr ""

#: includes/sections/error-alert.php:11
msgid "Oops..."
msgstr ""

#: includes/sections/error-alert.php:12
msgid "The page you requested could not be found."
msgstr ""

#: includes/sections/error-alert.php:16
msgid "Back to Home"
msgstr ""

#: includes/sections/insights-info.php:9
msgid "Client"
msgstr ""

#: includes/sections/insights-info.php:29
msgid "Location"
msgstr ""

#: includes/sections/insights-info.php:39
msgid "Website"
msgstr ""

#: includes/sections/technologies-related.php:8
msgid "All of our technologies"
msgstr ""

#: functions/api/Insights/Filters.php:20
msgid "Filter by \"Technologies\""
msgstr ""

#: functions/api/Insights/Filters.php:33
msgid "Filter by \"Industries\""
msgstr ""

#: functions/api/Search/Filters.php:24
msgid "Date"
msgstr ""

#: functions/api/Search/Filters.php:25
msgid "Title"
msgstr ""

#: functions/api/Search/Filters.php:33
msgid "Technology"
msgstr ""

#: functions/api/Search/Filters.php:35
msgid "Insight"
msgstr ""

#: functions/api/Search/Filters.php:36
msgid "Other"
msgstr ""

#: functions/api/Site/Form.php:25
msgid "Choose a file"
msgstr ""

#. Name of the theme
msgid "Example website"
msgstr ""

#. Author of the theme
msgid "Mateusz Gbiorczyk"
msgstr ""

#. Author URI of the theme
msgid "https://gbiorczyk.pl/"
msgstr ""
