/* ---
  Docs: https://www.npmjs.com/package/mati-mix/
--- */
const mix = require('mati-mix');

mix.js([
  '_dev/js/vendors/polyfills/**/*.js',
  '_dev/js/vendors/helpers/*.js',
  '_dev/js/classes/Core.js',
], 'public/build/js/scripts.js');

mix.sass(
  '_dev/scss/core.scss'
, 'public/build/css/styles.css');

mix.sass(
  '_dev/admin-scss/core.scss'
, 'public/build/css/admin.css');

/* ---
  Config
--- */
mix
  // .sassMobileFirst()
  // .aliases({
  //   'class': __dirname + '/_dev/js/classes',
  // })
  .browserSync('website.test', [
    './public/build/css/styles.css',
  ])
  // .version()
;