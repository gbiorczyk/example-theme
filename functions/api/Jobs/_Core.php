<?php

  namespace SiteManagement\Jobs;

  class _Core
  {
    public function __construct()
    {
      new Archive();
      new Columns();
      new Link();
      new Post();
      new Posts();
    }
  }