<?php

  namespace SiteManagement\Jobs;
  use SiteManagement\Abstracts\ArchiveAbstract;
  use SiteManagement\Interfaces\ArchiveInterface;

  class Archive extends ArchiveAbstract implements ArchiveInterface
  {
    /* ---
      Functions
    --- */

    public function getNamePrefix()
    {
      return 'jobs';
    }

    public function isArchive()
    {
      return false;
    }

    public function getPostsPerPage()
    {
      return 4;
    }

    public function getApiParams()
    {
      return [
        'department' => [
          'description'       => 'Department ID',
          'required'          => false,
          'validate_callback' => function($value, $request, $param) {
            return (preg_match('/(^[0-9]+$|^$)/', $value) > 0);
          },
        ],
        'type' => [
          'description'       => 'Type ID',
          'required'          => false,
          'validate_callback' => function($value, $request, $param) {
            return (preg_match('/(^[0-9]+$|^$)/', $value) > 0);
          },
        ],
        'location' => [
          'description'       => 'Location ID',
          'required'          => false,
          'validate_callback' => function($value, $request, $param) {
            return (preg_match('/(^[0-9]+$|^$)/', $value) > 0);
          },
        ],
        'experience' => [
          'description'       => 'Experience ID',
          'required'          => false,
          'validate_callback' => function($value, $request, $param) {
            return (preg_match('/(^[0-9]+$|^$)/', $value) > 0);
          },
        ],
      ];
    }
  }