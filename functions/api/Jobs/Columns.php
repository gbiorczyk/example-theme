<?php

  namespace SiteManagement\Jobs;

  class Columns
  {
    public function __construct()
    {
      add_filter('wpf_manage-jobs_columns', [$this, 'addColumns']);
    }

    /* ---
      Functions
    --- */

    public function addColumns($columns)
    {
      return array_merge($columns, [
        'expiration' => [
          'label'        => 'Expiration date',
          'action_value' => function($objectId) {
            return get_field('expiration_date', $objectId) ?? '-';
          },
          'action_sort'  => function($args, $defaultArgs, $order) {
            $args['orderby']  = 'meta_value_num';
            $args['meta_key'] = 'expiration_date';
            return $args;
          },
        ],
      ]);
    }
  }