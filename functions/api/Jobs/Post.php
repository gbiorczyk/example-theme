<?php

  namespace SiteManagement\Jobs;
  use SiteManagement\Abstracts\PostAbstract;
  use SiteManagement\Interfaces\PostInterface;

  class Post extends PostAbstract implements PostInterface
  {
    /* ---
      Functions
    --- */

    public function getNamePrefix()
    {
      return 'jobs';
    }

    public function getPostData($postId)
    {
      $data = [
        'id'          => $postId,
        'url'         => get_field('offer_url', $postId),
        'title'       => get_the_title($postId),
        'image'       => ($image = get_field('image', $postId)) ? $image['sizes']['image-small'] ?? '' : '',
        'departments' => $this->getTermsForPost($postId, 'jobs-departments'),
        'types'       => $this->getTermsForPost($postId, 'jobs-types'),
        'locations'   => $this->getTermsForPost($postId, 'jobs-locations'),
        'experience'  => $this->getTermsForPost($postId, 'jobs-experience'),
      ];
      return $data;
    }

    private function getTermsForPost($postId, $taxonomy)
    {
      $terms = get_the_terms($postId, $taxonomy);
      $list  = [];

      foreach ($terms as $term) {
        $list[] = [
          'id'    => $term->term_id,
          'title' => $term->name,
        ];
      }
      return $list;
    }
  }