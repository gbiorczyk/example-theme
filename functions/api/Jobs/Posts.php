<?php

  namespace SiteManagement\Jobs;
  use SiteManagement\Abstracts\PostsAbstract;
  use SiteManagement\Interfaces\PostsInterface;

  class Posts extends PostsAbstract implements PostsInterface
  {
    /* ---
      Functions
    --- */

    public function getNamePrefix()
    {
      return 'jobs';
    }

    public function getDefaultArgs()
    {
      return [
        'post_type'  => 'jobs',
        'meta_query' => [
          [
            'relation'  => 'OR',
            [
              'key'     => 'expiration_date',
              'compare' => '>=',
              'type'    => 'numeric',
              'value'   => current_time('Ymd'),
            ],
            [
              'key'     => 'expiration_date',
              'compare' => '=',
              'value'   => '',
            ],
          ],
        ],
      ];
    }

    public function getArgsByParam($args, $key, $value)
    {
      switch ($key) {
        case 'department':
          $args['tax_query'][] = [
            'taxonomy'         => 'jobs-departments',
            'field'            => 'id',
            'terms'            => [$value],
            'include_children' => false,
            'operator'         => 'IN'
          ];
          break;
        case 'type':
          $args['tax_query'][] = [
            'taxonomy'         => 'jobs-types',
            'field'            => 'id',
            'terms'            => [$value],
            'include_children' => false,
            'operator'         => 'IN'
          ];
          break;
        case 'location':
          $args['tax_query'][] = [
            'taxonomy'         => 'jobs-locations',
            'field'            => 'id',
            'terms'            => [$value],
            'include_children' => false,
            'operator'         => 'IN'
          ];
          break;
        case 'experience':
          $args['tax_query'][] = [
            'taxonomy'         => 'jobs-experience',
            'field'            => 'id',
            'terms'            => [$value],
            'include_children' => false,
            'operator'         => 'IN'
          ];
          break;
      }
      return $args;
    }
  }