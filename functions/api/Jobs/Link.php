<?php

  namespace SiteManagement\Jobs;

  class Link
  {
    public function __construct()
    {
      add_filter('acf/load_value/type=link', [$this, 'removeLinkToJobsSection'], 10, 3);
    }

    /* ---
      Functions
    --- */

    public function removeLinkToJobsSection($value, $postId, $field)
    {
      if (!$value || !isset($value['url'])
        || (strpos($value['url'], '#jobs')) === false) return $value;

      $response = apply_filters('jobs_api_response', []);
      if (!$response || ($response['data'] === [])) return null;
      else return $value;
    }
  }