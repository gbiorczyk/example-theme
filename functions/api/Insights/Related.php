<?php

  namespace SiteManagement\Insights;

  class Related
  {
    public function __construct()
    {
      add_filter('insights_related', [$this, 'getPosts'], 10, 3);
    }

    /* ---
      Functions
    --- */

    public function getPosts($value, $count, $currentId)
    {
      $args = [
        'posts_per_page' => $count,
        'orderby'        => 'rand',
        'post__not_in'   => [$currentId],
      ];

      return apply_filters('insights_posts', $value, $args);
    }
  }