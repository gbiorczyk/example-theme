<?php

  namespace SiteManagement\Insights;

  class Columns
  {
    public function __construct()
    {
      add_filter('wpf_manage-insights_columns', [$this, 'addColumns'], 100);
    }

    /* ---
      Functions
    --- */

    public function addColumns($columns)
    {
      if (isset($columns['wpf_insights-clients'])) unset($columns['wpf_insights-clients']);
      if (isset($columns['wpf_insights-locations'])) unset($columns['wpf_insights-locations']);
      if (isset($columns['wpf_insights-solutions'])) unset($columns['wpf_insights-solutions']);

      return array_merge($columns, [
        'technologies' => [
          'label'        => 'Technologies',
          'action_value' => function($objectId) {
            $postIds = get_field('technology_ids', $objectId);
            return implode(', ', $this->getPostsList($postIds));
          },
          'action_sort'  => null,
        ],
        'industries' => [
          'label'        => 'Industries',
          'action_value' => function($objectId) {
            $postIds = get_field('industry_ids', $objectId);
            return implode(', ', $this->getPostsList($postIds));
          },
          'action_sort'  => null,
        ],
      ]);
    }

    private function getPostsList($postIds)
    {
      $list = [];
      foreach ($postIds as $postId) {
        $list[] = sprintf('<a href="%s">%s</a>', get_edit_post_link($postId), get_the_title($postId));
      }
      return $list;
    }
  }