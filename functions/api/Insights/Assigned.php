<?php

  namespace SiteManagement\Insights;

  class Assigned
  {
    public function __construct()
    {
      add_filter('insights_assigned', [$this, 'getPosts'], 10, 3);
    }

    /* ---
      Functions
    --- */

    public function getPosts($value, $count, $currentId)
    {
      $postType = get_post_type($currentId);

      $args = [
        'posts_per_page' => $count,
        'orderby'        => 'rand',
        'post__not_in'   => [$currentId],
      ];
      $params = [];
      if ($postType === 'technologies') $params['technology'] = $currentId;
      elseif ($postType === 'industries') $params['industry'] = $currentId;

      return apply_filters('insights_posts', $value, $args, $params);
    }
  }