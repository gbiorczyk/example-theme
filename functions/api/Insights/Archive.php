<?php

  namespace SiteManagement\Insights;
  use SiteManagement\Abstracts\ArchiveAbstract;
  use SiteManagement\Interfaces\ArchiveInterface;

  class Archive extends ArchiveAbstract implements ArchiveInterface
  {
    /* ---
      Functions
    --- */

    public function getNamePrefix()
    {
      return 'insights';
    }

    public function isArchive()
    {
      return (is_post_type_archive(['insights']) || is_tax(['']));
    }

    public function getPostsPerPage()
    {
      return 6;
    }

    public function getApiParams()
    {
      return [
        'technology' => [
          'description'       => 'Technology ID',
          'required'          => false,
          'validate_callback' => function($value, $request, $param) {
            return (preg_match('/(^[0-9]+$|^$)/', $value) > 0);
          },
        ],
        'industry' => [
          'description'       => 'Industry ID',
          'required'          => false,
          'validate_callback' => function($value, $request, $param) {
            return (preg_match('/(^[0-9]+$|^$)/', $value) > 0);
          },
        ],
      ];
    }
  }