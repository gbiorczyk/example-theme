<?php

  namespace SiteManagement\Insights;
  use SiteManagement\Abstracts\PostsAbstract;
  use SiteManagement\Interfaces\PostsInterface;

  class Posts extends PostsAbstract implements PostsInterface
  {
    /* ---
      Functions
    --- */

    public function getNamePrefix()
    {
      return 'insights';
    }

    public function getDefaultArgs()
    {
      return [
        'post_type' => 'insights',
      ];
    }

    public function getArgsByParam($args, $key, $value)
    {
      switch ($key) {
        case 'technology':
          $args['meta_query'][] = [
            'key'     => 'technology_ids',
            'value'   => '"' . $value . '"',
            'compare' => 'LIKE',
          ];
          break;
        case 'industry':
          $args['meta_query'][] = [
            'key'     => 'industry_ids',
            'value'   => '"' . $value . '"',
            'compare' => 'LIKE',
          ];
          break;
      }
      return $args;
    }
  }