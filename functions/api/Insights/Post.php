<?php

  namespace SiteManagement\Insights;
  use SiteManagement\Abstracts\PostAbstract;
  use SiteManagement\Interfaces\PostInterface;

  class Post extends PostAbstract implements PostInterface
  {
    /* ---
      Functions
    --- */

    public function getNamePrefix()
    {
      return 'insights';
    }

    public function getPostData($postId)
    {
      $data = [
        'id'    => $postId,
        'url'   => get_permalink($postId),
        'title' => get_the_title($postId),
        'image' => ($image = get_field('image', $postId)) ? $image['sizes']['image-small'] ?? '' : '',
      ];
      return $data;
    }
  }