<?php

  namespace SiteManagement\Insights;

  class _Core
  {
    public function __construct()
    {
      new Archive();
      new Assigned();
      new Columns();
      new Filters();
      new Popular();
      new Post();
      new Posts();
      new Related();
    }
  }