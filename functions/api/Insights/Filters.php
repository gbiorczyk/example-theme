<?php

  namespace SiteManagement\Insights;

  class Filters
  {
    public function __construct()
    {
      add_filter('wpf_manage-insights_filters', [$this, 'addFilters']);
    }

    /* ---
      Functions
    --- */

    public function addFilters($filters)
    {
      return array_merge($filters, [
        'technologies' => [
          'label'        => __('Filter by "Technologies"', 'lang'),
          'values'       => $this->getPostsList('technologies'),
          'action_query' => function($args, $defaultArgs, $value) {
            if (!isset($args['meta_query'])) $args['meta_query'] = [];
            $args['meta_query'][] = [
              'key'     => 'technology_ids',
              'value'   => '"' . $value . '"',
              'compare' => 'LIKE',
            ];
            return $args;
          },
        ],
        'industries' => [
          'label'        => __('Filter by "Industries"', 'lang'),
          'values'       => $this->getPostsList('industries'),
          'action_query' => function($args, $defaultArgs, $value) {
            if (!isset($args['meta_query'])) $args['meta_query'] = [];
            $args['meta_query'][] = [
              'key'     => 'industry_ids',
              'value'   => '"' . $value . '"',
              'compare' => 'LIKE',
            ];
            return $args;
          },
        ],
      ]);
    }

    private function getPostsList($postType)
    {
      $posts = get_posts([
        'post_type'      => $postType,
        'posts_per_page' => -1,
        'orderby'        => 'title',
        'order'          => 'ASC',
      ]);

      $list = [];
      foreach ($posts as $post) {
        $list[$post->ID] = $post->post_title;
      }
      return $list;
    }
  }