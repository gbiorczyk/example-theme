<?php

  namespace SiteManagement\Insights;

  class Popular
  {
    public function __construct()
    {
      add_filter('insights_popular', [$this, 'getPosts'], 10, 2);
    }

    /* ---
      Functions
    --- */

    public function getPosts($value, $count)
    {
      $args = [
        'posts_per_page' => $count,
        'orderby'        => 'meta_value_num',
        'order'          => 'DESC',
        'meta_key'       => 'views_count',
      ];

      return apply_filters('insights_posts', $value, $args);
    }
  }