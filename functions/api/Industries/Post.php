<?php

  namespace SiteManagement\Industries;
  use SiteManagement\Abstracts\PostAbstract;
  use SiteManagement\Interfaces\PostInterface;

  class Post extends PostAbstract implements PostInterface
  {
    /* ---
      Functions
    --- */

    public function getNamePrefix()
    {
      return 'industries';
    }

    public function getPostData($postId)
    {
      $data = [
        'id'    => $postId,
        'url'   => get_permalink($postId),
        'title' => get_the_title($postId),
        'icon'  => get_field('icon', $postId),
        'desc'  => ($header = get_field('header', $postId)) ? $header['desc'] ?? '' : '',
      ];
      return $data;
    }
  }