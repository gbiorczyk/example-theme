<?php

  namespace SiteManagement\Industries;
  use SiteManagement\Abstracts\ArchiveAbstract;
  use SiteManagement\Interfaces\ArchiveInterface;

  class Archive extends ArchiveAbstract implements ArchiveInterface
  {
    /* ---
      Functions
    --- */

    public function getNamePrefix()
    {
      return 'industries';
    }

    public function isArchive()
    {
      return (is_post_type_archive(['industries']) || is_tax(['']));
    }

    public function getPostsPerPage()
    {
      return -1;
    }

    public function getApiParams()
    {
      return null;
    }
  }