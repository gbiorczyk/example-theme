<?php

  namespace SiteManagement\Industries;

  class _Core
  {
    public function __construct()
    {
      new Archive();
      new Columns();
      new Post();
      new Posts();
    }
  }