<?php

  namespace SiteManagement\Industries;

  class Columns
  {
    public function __construct()
    {
      add_filter('wpf_manage-industries_columns', [$this, 'addColumns']);
    }

    /* ---
      Functions
    --- */

    public function addColumns($columns)
    {
      return array_merge($columns, [
        'priority' => [
          'label'        => 'Priority',
          'action_value' => function($objectId) {
            return get_field('priority', $objectId);
          },
          'action_sort'  => function($args, $defaultArgs, $order) {
            $args['orderby']  = 'meta_value_num';
            $args['meta_key'] = 'priority';
            return $args;
          },
        ],
      ]);
    }
  }