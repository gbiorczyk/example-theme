<?php

  namespace SiteManagement\Technologies;

  class Tabs
  {
    public function __construct()
    {
      add_filter('technologies_tabs', [$this, 'getTechnologies'], 10, 2);
    }

    /* ---
      Functions
    --- */

    public function getTechnologies($value, $tabs)
    {
      $items = apply_filters('technologies_posts', []);
      foreach ($items as $index => $item) {
        $items[$index] = array_merge($item, $this->findTechnologyInList($tabs, $item['id']));
      }
      return $items;
    }

    private function findTechnologyInList($tabs, $technologyId)
    {
      $tabIds  = [];
      $catIds = [];

      foreach ($tabs as $tabId => $tab) {
        foreach ($tab['technologies'] as $catId => $data) {
          if (isset($data['technology']) && ($data['technology'] === $technologyId)) {
            $tabIds[] = $tabId;
            $catIds[] = $technologyId;
          }
        }
      }

      return [
        '_tab_ids' => $tabIds,
        '_cat_ids' => array_unique($catIds),
      ];
    }
  }