<?php

  namespace SiteManagement\Technologies;

  class Related
  {
    public function __construct()
    {
      add_filter('technologies_related', [$this, 'getPosts'], 10, 2);
    }

    /* ---
      Functions
    --- */

    public function getPosts($value, $currentId = 0)
    {
      $args = [
        'post__not_in' => [$currentId],
      ];

      return apply_filters('technologies_posts', $value, $args);
    }
  }