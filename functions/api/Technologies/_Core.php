<?php

  namespace SiteManagement\Technologies;

  class _Core
  {
    public function __construct()
    {
      new Archive();
      new Columns();
      new Post();
      new Posts();
      new Related();
      new Tabs();
    }
  }