<?php

  namespace SiteManagement\Technologies;

  class Columns
  {
    public function __construct()
    {
      add_filter('wpf_manage-technologies_columns', [$this, 'addColumns']);
    }

    /* ---
      Functions
    --- */

    public function addColumns($columns)
    {
      return array_merge($columns, [
        'industries' => [
          'label'        => 'Industries',
          'action_value' => function($objectId) {
            return $this->getIndustries($objectId);
          },
          'action_sort'  => false,
        ],
        'priority' => [
          'label'        => 'Priority',
          'action_value' => function($objectId) {
            return get_field('priority', $objectId);
          },
          'action_sort'  => function($args, $defaultArgs, $order) {
            $args['orderby']  = 'meta_value_num';
            $args['meta_key'] = 'priority';
            return $args;
          },
        ],
      ]);
    }

    private function getIndustries($postId)
    {
      if (!$items = get_field('industries', $postId)) return '';

      $list = [];
      foreach ($items as $industryId) {
        $list[] = sprintf('<a href="%s">%s</a>', get_edit_post_link($industryId), get_the_title($industryId));
      }
      return implode(', ', $list);
    }
  }