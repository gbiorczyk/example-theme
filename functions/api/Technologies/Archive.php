<?php

  namespace SiteManagement\Technologies;
  use SiteManagement\Abstracts\ArchiveAbstract;
  use SiteManagement\Interfaces\ArchiveInterface;

  class Archive extends ArchiveAbstract implements ArchiveInterface
  {
    /* ---
      Functions
    --- */

    public function getNamePrefix()
    {
      return 'technologies';
    }

    public function isArchive()
    {
      return (is_post_type_archive(['technologies']) || is_tax(['']));
    }

    public function getPostsPerPage()
    {
      return -1;
    }

    public function getApiParams()
    {
      return null;
    }
  }