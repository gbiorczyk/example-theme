<?php

  namespace SiteManagement\Technologies;
  use SiteManagement\Abstracts\PostAbstract;
  use SiteManagement\Interfaces\PostInterface;

  class Post extends PostAbstract implements PostInterface
  {
    /* ---
      Functions
    --- */

    public function getNamePrefix()
    {
      return 'technologies';
    }

    public function getPostData($postId)
    {
      $data = [
        'id'         => $postId,
        'url'        => get_permalink($postId),
        'title'      => get_the_title($postId),
        'color'      => get_field('color', $postId),
        'icon'       => get_field('icon', $postId),
        'desc'       => get_field('desc', $postId),
        'industries' => get_field('industries', $postId),
        'image'      => ($image = get_field('image', $postId)) ? $image['sizes']['image-small'] ?? '' : '',
        'has_widget' => get_field('is_active_widget', $postId),
      ];
      return $data;
    }
  }