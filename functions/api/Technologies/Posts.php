<?php

  namespace SiteManagement\Technologies;
  use SiteManagement\Abstracts\PostsAbstract;
  use SiteManagement\Interfaces\PostsInterface;

  class Posts extends PostsAbstract implements PostsInterface
  {
    /* ---
      Functions
    --- */

    public function getNamePrefix()
    {
      return 'technologies';
    }

    public function getDefaultArgs()
    {
      return [
        'post_type' => 'technologies',
        'meta_key'  => 'priority',
        'orderby'   => 'meta_value_num',
        'order'     => 'ASC',
      ];
    }

    public function getArgsByParam($args, $key, $value)
    {
      return $args;
    }
  }