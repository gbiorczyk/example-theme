<?php

  namespace SiteManagement\Pages;

  class Archives
  {

    public function __construct()
    {
      add_filter('theme_page_templates', [$this, 'addPageTemplatesForPostTypes']);
      add_filter('display_post_states',  [$this, 'addPostState'], 10, 2);
      add_action('template_redirect',    [$this, 'setupPostForPostTypeArchive']);
      add_action('template_redirect',    [$this, 'redirectToPostTypeArchive']);
    }

    /* ---
      Functions
    --- */

    public function addPageTemplatesForPostTypes($templates)
    {
      global $wp_post_types;

      foreach ($wp_post_types as $postType) {
        if (!$postType->public || !$postType->has_archive) continue;
        $path             = '_post_type_' . $postType->name;
        $templates[$path] = sprintf('"%s" Post Type Archive', $postType->labels->name);
      }
      return $templates;
    }

    public function addPostState($states, $post)
    {
      if (($post->post_type !== 'page')
        || (!$template = get_post_meta($post->ID, '_wp_page_template', true))
        || !preg_match('/_post_type_(.*)/', $template, $matches)
        || (!$postType = get_post_type_object($matches[1]))) return $states;

      $states[$template] = sprintf('"%s" Post Type Archive', $postType->labels->name);
      return $states;
    }

    public function setupPostForPostTypeArchive()
    {
      if (!is_post_type_archive() && !is_tax()) return;

      global $post;
      $pages = get_posts([
        'posts_per_page' => -1,
        'post_type'      => 'page',
        'meta_key'       => '_wp_page_template',
        'meta_value'     => '_post_type_' . ($post->post_type ?? get_query_var('post_type')),
      ]);
      if ($pages) $post = $pages[0];
    }

    public function redirectToPostTypeArchive()
    {
      if (!is_page() || (!$template = get_page_template_slug())
        || !preg_match('/_post_type_(.*)/', $template, $matches)
        || (!$postType = get_post_type_object($matches[1]))) return;

      $url = get_post_type_archive_link($postType->name);
      wp_redirect($url, 301);
    }
  }