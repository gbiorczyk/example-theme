<?php

  namespace SiteManagement\Pages;

  class _Core
  {
    public function __construct()
    {
      new Archives();
      new Children();
      new Hierarchy();
    }
  }