<?php

  namespace SiteManagement\Pages;

  class Hierarchy
  {

    public function __construct()
    {
      add_filter('theme_page_templates', [$this, 'addParentPageTemplate']);
      add_action('template_redirect',    [$this, 'redirectToChildPage']);
    }

    /* ---
      Functions
    --- */

    public function addParentPageTemplate($templates)
    {
      $templates['_page_parent_'] = 'Page parent (auto-redirect)';
      return $templates;
    }

    public function redirectToChildPage()
    {
      if (!is_page() || (get_page_template_slug() !== '_page_parent_')
        || (!$childrenIds = $this->getChildrenPages(get_the_ID()))) return;

      $url = get_permalink($childrenIds[0]);
      wp_redirect($url, 301);
    }

    private function getChildrenPages($parentId)
    {
      $postIds = get_posts([
        'posts_per_page' => -1,
        'post_type'      => 'page',
        'post_parent'    => $parentId,
        'fields'         => 'ids',
        'order'          => 'ASC',
        'orderby'        => 'date',
      ]);
      return $postIds;
    }
  }