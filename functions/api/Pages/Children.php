<?php

  namespace SiteManagement\Pages;

  class Children
  {

    public function __construct()
    {
      add_filter('pages_children', [$this, 'getChildrenPages'], 10, 2);
    }

    /* ---
      Functions
    --- */

    public function getChildrenPages($value, $currentId)
    {
      $post = get_post($currentId);
      if (!$post->post_parent) return $value;

      return $this->getChildrenByParentId($post->post_parent, $currentId);
    }

    private function getChildrenByParentId($parentId, $currentId)
    {
      $posts = get_posts([
        'posts_per_page' => -1,
        'post_type'      => 'page',
        'post_parent'    => $parentId,
        'order'          => 'ASC',
        'orderby'        => 'date',
      ]);
      return $this->parsePages($posts, $currentId);
    }

    private function parsePages($posts, $currentId)
    {
      $items = [];
      foreach ($posts as $post) {
        $items[] = [
          'id'        => $post->ID,
          'title'     => $post->post_title,
          'url'       => get_permalink($post),
          'is_active' => ($post->ID === $currentId),
        ];
      }
      return $items;
    }
  }