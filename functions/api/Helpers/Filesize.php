<?php

  namespace SiteManagement\Helpers;

  class Filesize
  {

    public function __construct()
    {
      add_filter('helpers_filesize', [$this, 'getFileSize'], 10, 2);
    }

    /* ---
      Functions
    --- */

    public function getFileSize($value, $mediaId)
    {
      $path = get_attached_file($mediaId);
      if (!$path) return $value;
      return size_format(filesize($path), 0);
    }
  }