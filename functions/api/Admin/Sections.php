<?php

  namespace SiteManagement\Admin;

  class Sections
  {
    public function __construct()
    {
      add_filter('wpf_acf_flexible_fields', [$this, 'addFieldsToFlexible'], 10, 2);
    }

    /* ---
      Functions
    --- */

    public function addFieldsToFlexible($fields, $field)
    {
      if ($field['name'] !== 'sections') return $fields;
      return [
        [
          'label'        => 'Is disabled?',
          'name'         => 'section_is_disabled',
          'type'         => 'true_false',
          'instructions' => '',
        ],
        [
          'label'        => 'Section ID',
          'name'         => 'section_id',
          'type'         => 'text',
          'instructions' => sprintf('Specifying ID allows scrolling to given section if URL has # with that ID - e.g. %s%scontact/#form%s (use only lowercase letters and underscores).%sIt also allows section to be turned off by function if condition is not met (do not change ID of existing section).',
            '<code>',
            site_url('/'),
            '</code>',
            '<br>'
          ),
        ],
        [
          'label'        => 'Increased space before?',
          'name'         => 'section_is_space',
          'type'         => 'true_false',
          'instructions' => '',
        ],
      ];
    }
  }