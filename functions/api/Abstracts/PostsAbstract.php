<?php

  namespace SiteManagement\Abstracts;

  abstract class PostsAbstract
  {
    private $prefix, $cache = [];

    public function __construct()
    {
      $this->prefix = $this->getNamePrefix();

      add_filter($this->prefix . '_posts', [$this, 'getPosts'], 10, 3);
    }

    /* ---
      Filters
    --- */

    /**
     * Returns arrays of array of post data
     *
     * @param  string $value        default value of filter
     * @param  array  $overrideArgs list of overriding arguments for WP_Query
     * @param  array  $params       list of parameters used in REST API Endpoint
     * @return array                array of posts
     */
    public function getPosts($value, $overrideArgs = [], $params = [])
    {
      $args       = $this->getQueryArgs($params, $overrideArgs);
      $cacheIndex = json_encode($args);
      if (isset($this->cache[$cacheIndex])) return $this->cache[$cacheIndex];

      $response = $this->getPostsByQuery($args, $overrideArgs, $params);
      $this->cache[$cacheIndex] = $response;
      return $response;
    }

    /* ---
      Args for WP_Query
    --- */

    private function getQueryArgs($params, $overrideArgs)
    {
      $defaults = array_merge_recursive([
        'tax_query'      => [],
        'meta_query'     => [],
        'posts_per_page' => -1,
        'orderby'        => 'date',
        'order'          => 'DESC',
        'fields'         => 'ids',
      ], $this->getDefaultArgs($params));
      return $this->parseQueryArgs($defaults, $params, $overrideArgs);
    }

    private function parseQueryArgs($defaults, $params, $overrideArgs)
    {
      $paramsArgs = $this->getArgsByParams($params);
      $args       = array_merge($defaults, $paramsArgs, $overrideArgs);

      if (isset($params['page'])) {
        $args['posts_per_page'] = -1;
        $args['offset']         = 0;
      }
      if (isset($params['lang'])) $args['lang'] = $params['lang'];

      return $args;
    }

    private function getArgsByParams($params)
    {
      $args = [];
      foreach ($params as $key => $value) {
        if ($value === '') continue;
        $args = $this->getArgsByParam($args, $key, $value);
      }
      return $args;
    }

    /* ---
      WP_Query
    --- */

    private function getPostsByQuery($args, $overrideArgs, $params)
    {
      $posts = get_posts($args);
      $items = $this->parsePosts($posts, $overrideArgs, $params);
      return $items;
    }

    private function parsePosts($postIds, $overrideArgs, $params)
    {
      $indexes = $this->getAvailableIndexes($postIds, $overrideArgs, $params);
      $list    = [];
      foreach ($postIds as $index => $postId) {
        if (!in_array($index, $indexes)) $list[] = null;
        else $list[] = apply_filters($this->prefix . '_post', [], $postId);
      }
      return $list;
    }

    private function getAvailableIndexes($postIds, $overrideArgs, $params)
    {
      $keys = array_flip($postIds);
      if (!isset($params['page'])) return $keys;

      $indexMin = $overrideArgs['offset'] ?? 0;
      $perPage  = $overrideArgs['posts_per_page'] ?? 0;
      $indexMax = (($perPage > 0) ? $perPage : count($postIds));
      return array_slice($keys, $indexMin, $indexMax);
    }
  }