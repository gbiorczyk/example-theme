<?php

  namespace SiteManagement\Abstracts;

  abstract class ArchiveAbstract
  {
    private $prefix, $perPage, $params;

    public function __construct()
    {
      $this->prefix  = $this->getNamePrefix();
      $this->perPage = $this->getPostsPerPage();
      $this->params  = $this->getApiParams();

      add_filter($this->prefix . '_api_url',      [$this, 'getRestApiUrl']);
      add_filter($this->prefix . '_api_response', [$this, 'runEndpointManually'], 10, 2);

      add_action('pre_get_posts', [$this, 'setPostsPerPage']);
      add_action('rest_api_init', [$this, 'registerEndpoint']);
    }

    /* ---
      Filters
    --- */

    /**
     * Returns url of REST API Endpoint
     *
     * @param  string $value default value of filter
     * @return string        url to REST API Endpoint
     */
    public function getRestApiUrl($value)
    {
      if ($this->params === null) return $value;

      $path = get_rest_url(
        null,
        implode('/', [SITE_CONFIG['api']['path'], $this->prefix]) . '/'
      );
      return $path;
    }

    /**
     * Returns response of REST API Endpoint (simulates running Endpoint and taking data)
     *
     * @param  string $value  default value of filter
     * @param  array  $params list of parameters used in Endpoint (declared in `getApiParams` abstract method)
     * @return array          REST API Endpoint response
     */
    public function runEndpointManually($value, $params = [])
    {
      if ($this->params === null) return $value;

      $defaults = $this->getDefaultParams();
      $defaults = array_combine(array_keys($defaults), array_column($defaults, 'default'));
      $params   = array_merge($defaults, $params);
      return $this->getEndpointResponse($params);
    }

    /* ---
      Posts per page
    --- */

    public function setPostsPerPage($query)
    {
      if (is_admin() || !$query->is_main_query() || !$this->isArchive()) return;

      $query->set('posts_per_page', $this->perPage);
    }

    /* ---
      REST API Endpoint
    --- */

    public function registerEndpoint()
    {
      if ($this->params === null) return;

      register_rest_route(
        SITE_CONFIG['api']['path'],
        $this->prefix,
        [
          'methods'  => 'GET',
          'callback' => [$this, 'getPosts'],
          'args'     => array_merge($this->getDefaultParams(), $this->params),
        ]
      );
    }

    private function getDefaultParams()
    {
      return [
        'page' => [
          'description'       => 'Page number',
          'required'          => true,
          'default'           => 1,
          'validate_callback' => function($value, $request, $param) {
            return (preg_match('/(^[0-9]+$)/', $value) > 0);
          },
        ],
        'lang' => [
          'description'       => 'Lang slug (two letters)',
          'required'          => false,
          'default'           => function_exists('pll_default_language') ? pll_default_language() : '',
          'validate_callback' => function($value, $request, $param) {
            return (preg_match('/(^[a-z]{2}$|^$)/', $value) > 0);
          },
        ],
      ];
    }

    /* ---
      Action of Endpoint
    --- */

    public function getPosts($request)
    {
      $params = $request->get_params();
      $data   = $this->getEndpointResponse($params);
      return new \WP_REST_Response($data, 200);
    }

    private function getEndpointResponse($params)
    {
      $args = [
        'posts_per_page' => $this->perPage,
        'offset'         => max(($params['page'] - 1), 0) * $this->perPage,
      ];

      $posts = apply_filters($this->prefix . '_posts', [], $args, $params);
      return [
        'data'  => array_slice($posts, $args['offset'], ($this->perPage > -1) ? $this->perPage : null),
        'pages' => ($this->perPage > -1) ? ceil(count($posts) / $this->perPage) : 1,
      ];
    }
  }