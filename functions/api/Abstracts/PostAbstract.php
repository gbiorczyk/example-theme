<?php

  namespace SiteManagement\Abstracts;

  abstract class PostAbstract
  {
    private $prefix, $cache = [];

    public function __construct()
    {
      $this->prefix = $this->getNamePrefix();

      add_filter($this->prefix . '_post', [$this, 'getPost'], 10, 2);
    }

    /* ---
      Filters
    --- */

    /**
     * Returns data of post
     *
     * @param  string $value  default value of filter
     * @param  int    $postId post ID
     * @return array          array of post data
     */
    public function getPost($value, $postId)
    {
      $cacheIndex = $postId;
      if (isset($this->cache[$cacheIndex])) return $this->cache[$cacheIndex];

      $response = $this->getPostData($postId);
      $this->cache[$cacheIndex] = $response;
      return $response;
    }
  }