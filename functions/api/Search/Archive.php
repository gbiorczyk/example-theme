<?php

  namespace SiteManagement\Search;
  use SiteManagement\Abstracts\ArchiveAbstract;
  use SiteManagement\Interfaces\ArchiveInterface;

  class Archive extends ArchiveAbstract implements ArchiveInterface
  {
    /* ---
      Functions
    --- */

    public function getNamePrefix()
    {
      return 'search';
    }

    public function isArchive()
    {
      return (is_search());
    }

    public function getPostsPerPage()
    {
      return 6;
    }

    public function getApiParams()
    {
      return [
        'phrase' => [
          'description'       => 'Phrase for search',
          'required'          => true,
          'validate_callback' => function($value, $request, $param) {
            return true;
          },
        ],
        'category' => [
          'description'       => 'Slugs of Post Type (by default all Post Types)',
          'required'          => false,
          'validate_callback' => function($value, $request, $param) {
            if (!$value) return true;

            $list = is_array($value) ? $value : [$value];
            $list = array_filter($list, function($value) {
              return in_array($value, ['technologies', 'industries', 'insights', 'other']);
            });
            return $list ? true : false;
          },
        ],
        'sort' => [
          'description'       => 'Sort Type (date or title)',
          'required'          => false,
          'default'           => 'date',
          'validate_callback' => function($value, $request, $param) {
            return (($value === '') || in_array($value, ['date', 'title']));
          },
        ],
      ];
    }
  }