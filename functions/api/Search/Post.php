<?php

  namespace SiteManagement\Search;
  use SiteManagement\Abstracts\PostAbstract;
  use SiteManagement\Interfaces\PostInterface;

  class Post extends PostAbstract implements PostInterface
  {
    /* ---
      Functions
    --- */

    public function getNamePrefix()
    {
      return 'search';
    }

    public function getPostData($postId)
    {
      $postType = get_post_type($postId);
      $object   = get_post_type_object($postType);

      $data = [
        'id'       => $postId,
        'url'      => get_permalink($postId),
        'title'    => get_the_title($postId),
        'category' => $object->labels->singular_name ?? '',
        'desc'     => ($header = get_field('header', $postId)) ? $header['desc'] ?? '' : get_field('desc', $postId),
      ];
      return $data;
    }
  }