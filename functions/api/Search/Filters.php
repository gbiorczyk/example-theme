<?php

  namespace SiteManagement\Search;

  class Filters
  {
    public function __construct()
    {
      add_filter('search_filters', [$this, 'getFilters']);
    }

    /* ---
      Functions
    --- */

    public function getFilters($postId)
    {
      return [
        [
          'key'    => 'sort',
          'type'   => 'radio',
          'label'  => 'Sort by',
          'values' => [
            'date'  => __('Date', 'lang'),
            'title' => __('Title', 'lang'),
          ],
        ],
        [
          'key'    => 'category',
          'type'   => 'checkbox',
          'label'  => 'Category filter',
          'values' => [
            'technologies' => __('Technology', 'lang'),
            'industries'   => __('Industry', 'lang'),
            'insights'     => __('Insight', 'lang'),
            'other'        => __('Other', 'lang'),
          ],
        ],
      ];
    }
  }