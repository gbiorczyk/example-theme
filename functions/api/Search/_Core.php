<?php

  namespace SiteManagement\Search;

  class _Core
  {
    public function __construct()
    {
      new Archive();
      new Filters();
      new Post();
      new Posts();
    }
  }