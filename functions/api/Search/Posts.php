<?php

  namespace SiteManagement\Search;
  use SiteManagement\Abstracts\PostsAbstract;
  use SiteManagement\Interfaces\PostsInterface;

  class Posts extends PostsAbstract implements PostsInterface
  {
    /* ---
      Functions
    --- */

    public function getNamePrefix()
    {
      return 'search';
    }

    public function getDefaultArgs()
    {
      return [
        'post_type' => 'any',
      ];
    }

    public function getArgsByParam($args, $key, $value)
    {
      switch ($key) {
        case 'phrase':
          $args['s'] = $value;
          break;
        case 'category':
          if ($value === []) return $args;
          $args['post_type'] = $value;

          $keyOther = array_search('other', $args['post_type']);
          if ($keyOther !== false) $args['post_type'][$keyOther] = 'page';
        case 'sort':
          $args['orderby'] = ($value === 'date') ? 'date' : 'title';
          $args['order']   = ($value === 'date') ? 'DESC' : 'ASC';
          break;
      }
      return $args;
    }
  }