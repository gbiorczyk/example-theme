<?php

  namespace SiteManagement\Interfaces;

  interface PostInterface
  {
    /**
     * Returns prefix for name of filter `{$prefix}_post`
     *
     * @return string prefix of name
     */
    public function getNamePrefix();

    /**
     * Returns data for post by ID
     *
     * @param  int   $postId post ID
     * @return array         array of post data
     */
    public function getPostData($postId);
  }