<?php

  namespace SiteManagement\Interfaces;

  interface PostsInterface
  {
    /**
     * Returns prefix for name of filter `{$prefix}_posts`
     *
     * @return string prefix of name
     */
    public function getNamePrefix();

    /**
     * Returns number of posts per page for Post type archive and REST API
     *
     * @return int number of posts
     */
    public function getDefaultArgs();

    /**
     * Returns list of arguments for WP_Query in REST API Endpoint
     *
     * @param  int    $args  default list of arguments
     * @param  string $key   key of parameter
     * @param  string $value value of parameter
     * @return array         updated list of arguments
     */
    public function getArgsByParam($args, $key, $value);
  }