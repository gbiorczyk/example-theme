<?php

  namespace SiteManagement\Interfaces;

  interface ArchiveInterface
  {
    /**
     * Returns prefix for name of filters `{$prefix}_api_url` and `{$prefix}_api_response`
     * and also slug of REST API Endpoint
     *
     * @return string prefix of name
     */
    public function getNamePrefix();

    /**
     * Checks whether it is on Post Type archive or Taxonomy with these posts
     *
     * @return bool True if it is archive with posts from this group, false if not
     */
    public function isArchive();

    /**
     * Returns posts_per_page argument for WP_Query of Post type archive page
     *
     * @return int number of posts
     */
    public function getPostsPerPage();

    /**
     * Returns list of parameters for REST API Endpoint (keys are parameter names and values are arrays with options)
     *
     * @link https://developer.wordpress.org/rest-api/extending-the-rest-api/adding-custom-endpoints/#arguments
     *
     * @return array|null array of arrays of parameters or null for turn off REST API Endpoint
     */
    public function getApiParams();
  }