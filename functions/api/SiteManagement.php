<?php

  namespace SiteManagement;

  class SiteManagement
  {
    public function __construct()
    {
      define('SITE_CONFIG', require 'Config.php');

      new Admin\_Core();
      new Helpers\_Core();
      new Industries\_Core();
      new Insights\_Core();
      new Jobs\_Core();
      new Pages\_Core();
      new Search\_Core();
      new Site\_Core();
      new Technologies\_Core();
    }
  }