<?php

  namespace SiteManagement\Site;

  class _Core
  {
    public function __construct()
    {
      new Form();
      new Menu();
      new Sections();
    }
  }