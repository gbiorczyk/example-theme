<?php

  namespace SiteManagement\Site;

  class Sections
  {
    public function __construct()
    {
      add_filter('sections_is_active', [$this, 'hideSectionsByCondition'], 10, 3);
    }

    /* ---
      Functions
    --- */

    public function hideSectionsByCondition($status, $section, $sections)
    {
      if (isset($section['section_is_disabled']) && $section['section_is_disabled']) return false;

      $sectionId = $section['section_id'] ?? '';
      switch ($sectionId) {
        case 'jobs_message_has_offers':
          $response = apply_filters('jobs_api_response', []);
          return ($response && ($response['data'] !== []));
          break;
        case 'jobs_message_no_offers':
          $response = apply_filters('jobs_api_response', []);
          return (!$response || ($response['data'] === []));
          break;
      }
      return $status;
    }
  }