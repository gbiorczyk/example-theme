<?php

  namespace SiteManagement\Site;

  class Menu
  {
    public function __construct()
    {
      add_filter('parse_menu_mobile', [$this, 'parseMenuMobile']);
    }

    /* ---
      Functions
    --- */

    public function parseMenuMobile($items)
    {
      $list = $this->getMobileMenus([], $items);
      return $list;
    }

    private function getMobileMenus($menus, $items, $parent = [], $level = '', $menuIndex = 0)
    {
      $level         = trim(sprintf('%s.%d', $level, $menuIndex), '.');
      $menus[$level] = $this->getItemsFromMenu($items, $parent, $level);

      foreach ($items as $index => $item) {
        if (!$item['children']) continue;
        $menus = $this->getMobileMenus($menus, $item['children'], $item, $level, $index);
      }
      return $menus;
    }

    private function getItemsFromMenu($items, $parent, $level)
    {
      if ($parent) {
        unset($parent['children']);
        $parent['_menu'] = substr($level, 0, -2);
      }

      $list = [];
      foreach ($items as $index => $item) {
        $list[] = $this->parseItemForMenu($item, $level, $index);
      }
      return [
        '_parent' => $parent,
        '_depth'  => count(explode('.', $level)) - 1,
        '_items'  => $list,
      ];
    }

    private function parseItemForMenu($item, $level, $index)
    {
      $menuTarget = $item['children'] ? sprintf('%s.%d', $level, $index) : '';
      unset($item['children']);
      $item['_menu'] = $menuTarget;
      return $item;
    }
  }