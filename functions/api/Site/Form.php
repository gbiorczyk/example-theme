<?php

  namespace SiteManagement\Site;

  class Form
  {
    public function __construct()
    {
      add_filter('wpf_forms_field_html', [$this, 'parseFileField'], 10, 3);
    }

    /* ---
      Functions
    --- */

    public function parseFileField($html, $field, $formId)
    {
      if ($field['type'] !== 'file') return $html;

      ob_start();
      ?>
        <div class="contactForm__file">
          <div class="contactForm__fileInner">
            <div class="contactForm__fileLabel button button--border button--red"
              v-if="!form.<?= $field['name']; ?>.length"><?= __('Choose a file', 'lang'); ?></div>
            <ul v-if="form.<?= $field['name']; ?>.length" class="contactForm__fileItems">
              <li v-for="(file, index) in form.<?= $field['name']; ?>" class="contactForm__fileItem">
                <div class="contactForm__fileItemName">{{ file.name }}</div>
                <a href="#" class="contactForm__fileItemClose"
                  v-on:click="function(e) { e.preventDefault(); $emit('removeFile', '<?= $field['name']; ?>', index); }"></a>
            </ul>
            <?= substr($html, 0, strpos($html, '<div')); ?>
          </div>
          <?= substr($html, strpos($html, '<div')); ?>
        </div>
      <?php
      $output = ob_get_contents();
      ob_end_clean();
      return $output;
    }
  }