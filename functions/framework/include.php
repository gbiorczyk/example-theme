<?php

  $framework->loader->action('inline-css', [
    'public/build/css/styles.css',
  ]);

  $framework->loader->action('js', [
    'public/build/js/scripts.js',
  ]);

  $framework->loader->action('admin-css', [
    'public/build/css/admin.css',
  ]);