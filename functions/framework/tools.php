<?php

  $framework->tools->action('cleaner');

  $framework->tools->action('stats', [
    'limit_daily'   => 8,
    'limit_monthly' => 12,
    'limit_yearly'  => 10,
  ]);

  $framework->tools->action('validate-categories', [
    [
      'slug'        => 'jobs-departments',
      'post_types'  => ['jobs'],
      'min_checked' => 1,
      'max_checked' => 1,
    ],
    [
      'slug'        => 'jobs-types',
      'post_types'  => ['jobs'],
      'min_checked' => 1,
      'max_checked' => 1,
    ],
    [
      'slug'        => 'jobs-locations',
      'post_types'  => ['jobs'],
      'min_checked' => 1,
      'max_checked' => 1,
    ],
    [
      'slug'        => 'jobs-experience',
      'post_types'  => ['jobs'],
      'min_checked' => 1,
      'max_checked' => 1,
    ],
  ]);