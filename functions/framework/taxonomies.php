<?php

  $framework->taxonomy->action('register', [
    'slug'        => 'insights-clients',
    'rewrite'     => 'insights-clients',
    'posttypes'   => ['insights'],
    'is_category' => true,
    'labels'      => [
      'name' => __('Clients', 'lang'),
      'menu' => 'Clients',
    ],
    'langs'       => [],
    'args'        => [
      'public'  => false,
      'show_ui' => true,
    ],
  ]);

  $framework->taxonomy->action('register', [
    'slug'        => 'insights-locations',
    'rewrite'     => 'insights-locations',
    'posttypes'   => ['insights'],
    'is_category' => true,
    'labels'      => [
      'name' => __('Locations', 'lang'),
      'menu' => 'Locations',
    ],
    'langs'       => [],
    'args'        => [
      'public'  => false,
      'show_ui' => true,
    ],
  ]);

  $framework->taxonomy->action('register', [
    'slug'        => 'insights-solutions',
    'rewrite'     => 'insights-solutions',
    'posttypes'   => ['insights'],
    'is_category' => true,
    'labels'      => [
      'name' => __('Solutions', 'lang'),
      'menu' => 'Solutions',
    ],
    'langs'       => [],
    'args'        => [
      'public'             => false,
      'show_ui'            => true,
      'meta_box_cb'        => false,
      'show_in_quick_edit' => false,
    ],
  ]);

  $framework->taxonomy->action('register', [
    'slug'        => 'jobs-departments',
    'rewrite'     => 'jobs-departments',
    'posttypes'   => ['jobs'],
    'is_category' => true,
    'labels'      => [
      'name' => __('Departments', 'lang'),
      'menu' => 'Departments',
    ],
    'langs'       => [],
    'args'        => [
      'public'  => false,
      'show_ui' => true,
    ],
  ]);

  $framework->taxonomy->action('register', [
    'slug'        => 'jobs-types',
    'rewrite'     => 'jobs-types',
    'posttypes'   => ['jobs'],
    'is_category' => true,
    'labels'      => [
      'name' => __('Types', 'lang'),
      'menu' => 'Types',
    ],
    'langs'       => [],
    'args'        => [
      'public'  => false,
      'show_ui' => true,
    ],
  ]);

  $framework->taxonomy->action('register', [
    'slug'        => 'jobs-locations',
    'rewrite'     => 'jobs-locations',
    'posttypes'   => ['jobs'],
    'is_category' => true,
    'labels'      => [
      'name' => __('Locations', 'lang'),
      'menu' => 'Locations',
    ],
    'langs'       => [],
    'args'        => [
      'public'  => false,
      'show_ui' => true,
    ],
  ]);

  $framework->taxonomy->action('register', [
    'slug'        => 'jobs-experience',
    'rewrite'     => 'jobs-experience',
    'posttypes'   => ['jobs'],
    'is_category' => true,
    'labels'      => [
      'name' => __('Experience', 'lang'),
      'menu' => 'Experience',
    ],
    'langs'       => [],
    'args'        => [
      'public'  => false,
      'show_ui' => true,
    ],
  ]);