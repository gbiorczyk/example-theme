<?php

  $framework->settings->action('images', [
    'image-full' => [
      'width'  => 1920,
      'height' => 1080,
      'crop'   => true,
      'editor' => false,
    ],
    'image-large' => [
      'width'  => 1240,
      'height' => 930,
      'crop'   => false,
      'editor' => false,
    ],
    'image-medium' => [
      'width'  => 710,
      'height' => 710,
      'crop'   => false,
      'editor' => false,
    ],
    'image-small' => [
      'width'  => 400,
      'height' => 400,
      'crop'   => false,
      'editor' => false,
    ],
  ]);

  $framework->settings->action('nav', [
    'main_nav'   => 'Menu main',
    'footer_nav' => 'Menu footer',
  ]);

  $framework->settings->action('plugins-update', []);

  $framework->settings->action('security', [
    'wpc_path_allow' => [],
    'login_url'      => 'my-login',
  ]);

  $framework->settings->action('upload', [
    'svg' => 'image/svg+xml',
  ]);