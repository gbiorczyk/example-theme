<?php

  $framework->admin->action('menu', [
    'posts'     => false,
    'pages'     => true,
    'comments'  => false,
    'customize' => false,
    'wp_tools'  => false,
  ]);

  $framework->admin->action('tinymce', [
    'pages_editor' => false,
    'buttons_1'    => [
      'formatselect',
      'bold',
      'italic',
      'bullist',
      'numlist',
      'alignleft',
      'aligncenter',
      'alignright',
      'link',
      'removeformat',
      'pastetext',
      'undo',
      'redo',
    ],
    'buttons_2'    => [],
    'formats'      => [
      'h3' => 'Heading large',
      'h4' => 'Heading',
      'p'  => 'Paragraph',
    ],
  ]);

  // (available options from editor: Heading, Heading large, Paragraph, Bold, Italic, Bulleted and Numbered list, Link)

  $framework->admin->action('gutenberg', false);