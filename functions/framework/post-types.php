<?php

  $framework->posttype->action('register', [
    'slug'    => 'technologies',
    'rewrite' => 'technologies',
    'icon'    => 'dashicons-screenoptions',
    'labels'  => [
      'name' => __('Technologies', 'lang'),
      'menu' => 'Technologies',
    ],
    'langs'   => [
      'en' => 'technologies',
      'ar' => 'technologies',
    ],
    'args'    => [],
  ]);

  $framework->posttype->action('register', [
    'slug'    => 'industries',
    'rewrite' => 'industries',
    'icon'    => 'dashicons-networking',
    'labels'  => [
      'name' => __('Industries', 'lang'),
      'menu' => 'Industries',
    ],
    'langs'   => [
      'en' => 'industries',
      'ar' => 'industries',
    ],
    'args'    => [],
  ]);

  $framework->posttype->action('register', [
    'slug'    => 'insights',
    'rewrite' => 'insights',
    'icon'    => 'dashicons-lightbulb',
    'labels'  => [
      'name' => __('Insights', 'lang'),
      'menu' => 'Insights',
    ],
    'langs'   => [
      'en' => 'insights',
      'ar' => 'insights',
    ],
    'args'    => [],
  ]);

  $framework->posttype->action('register', [
    'slug'    => 'jobs',
    'rewrite' => 'jobs',
    'icon'    => 'dashicons-businessperson',
    'labels'  => [
      'name' => __('Jobs', 'lang'),
      'menu' => 'Jobs',
    ],
    'langs'   => [],
    'args'    => [
      'public'  => false,
      'show_ui' => true,
    ],
  ]);