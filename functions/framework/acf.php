<?php

  $source = get_template_directory() . '/public/build/css/admin.css';
  preg_match_all('/\.icon-([^:]+)/', file_exists($source) ? file_get_contents($source) : '', $icons);
  $icons = $icons ? array_unique($icons[1]) : [];

  $framework->acf->action('icons', $icons);

  $framework->acf->action('optionspage', [
    'title'       => 'Management',
    'slug'        => 'options',
    'icon'        => 'dashicons-admin-tools',
    'pages'       => [
      'header'       => 'Header',
      'footer'       => 'Footer',
      'integrations' => 'Integrations',
    ],
    'notranslate' => [
      'integrations',
    ],
  ]);